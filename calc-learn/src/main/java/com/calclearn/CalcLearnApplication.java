package com.calclearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalcLearnApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalcLearnApplication.class, args);
    }

}
