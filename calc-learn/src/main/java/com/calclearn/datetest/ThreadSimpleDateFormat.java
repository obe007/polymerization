package com.calclearn.datetest;

import org.springframework.boot.autoconfigure.template.TemplateLocation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.*;
import java.util.Calendar;
import java.util.Date;

/**
 * 多线程下 simpleDateFormat不安全 解析
 */
public class ThreadSimpleDateFormat {
    final static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static LocalDateTime now = LocalDateTime.now();

    public static void main(String[] args) {
        testAdjuster();
    }


    public static void testSimple() {
        new Thread(() -> {
            try {
                synchronized (simpleDateFormat) {
                    Date date = simpleDateFormat.parse("2020-04-14 10:53:00");
                    System.out.println("date = " + date);
                    throw new RuntimeException();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void setCalendar() {
        Calendar cal = Calendar.getInstance();
        cal.set(2008, Calendar.AUGUST, 1);
    }

    public static void dateTimeLearn1() {
        LocalDateTime dateTime = LocalDateTime.now();
    }

    /**
     * 对时间轴上的单一瞬时点建模，用于记录应用程序中的事件时间戳
     */
    public static void testInstant() {
        Instant instant = Instant.parse(LocalDateTime.parse("2019-11-11 11:11:11", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).atOffset(ZoneOffset.ofHours(+8)).toString());
        System.out.println(instant);
/*        String result = LocalDateTime.parse("2019-11-11 11:11:11", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toString();
        System.out.println("result = " + result);*/
        System.out.println("ZonedDateTime.now(ZoneId.systemDefault()) = " + ZonedDateTime.now(ZoneId.systemDefault()));

    }

    /**
     * 高精度事件间隔，处理较短时间，适合秒杀场景
     */
    public static void testDuration() {
        Duration duration = Duration.ZERO;
        System.out.println(duration.getNano());
    }

    /**
     * 初始化2018年8月8日的LocalDate对象
     */
    public static void localDateTest() {
        System.out.println(LocalDate.of(2018, 8, 8));
        System.out.println(LocalTime.of(8, 0));
        System.out.println("LocalDateTime.of(LocalDate.MIN, LocalTime.MAX) = " + LocalDateTime.of(LocalDate.MAX, LocalTime.MAX));
    }

    /**
     * 时区
     */
    public static void zoneTest() {
        var now = LocalDateTime.now();
        ZonedDateTime zonedDateTime = now.atZone(ZoneId.of("Asia/Shanghai"));
        System.out.println("Asia/Shanghai = " + zonedDateTime);
        var withZone = zonedDateTime.withZoneSameInstant(ZoneId.of(ZoneId.getAvailableZoneIds().iterator().next()));
        System.out.println("withZone = " + withZone);
    }

    public static void localDateTimeEnumMonty() {
        var enumDateTime = LocalDateTime.of(2020, Month.APRIL, 1, 1, 1, 1);
    }

    /**
     * 创建当前时间 不带时区
     * 创建当前时间 年月日
     * 创建当前时间 年月日时分秒时区
     */
    public static void testDemo2() {
        System.out.println("LocalDateTime.now() = " + LocalDateTime.now());
        System.out.println("LocalDate.now() = " + LocalDate.now());
        System.out.println("LocalDateTime.now(ZoneId.systemDefault()) = " + ZonedDateTime.now(ZoneId.of(ZoneId.getAvailableZoneIds().iterator().next())));

    }

    /**
     * 加减乘除时间
     */
    public static void plusMinus() {
        var dateTime = LocalDateTime.now();
        dateTime = dateTime.plusDays(11);
        System.out.println("dateTime = " + dateTime.format(DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss:SSS")));
    }

    /**
     * TemporalAmount学习使用
     */
    public static void testTemporalAmount() {
        LocalDateTime now = LocalDateTime.now();
//        final TemporalAmount temporalAmount;
//        final Temporal temporal;
        now.plus(Period.of(0, 0, 0));
        System.out.println("temporal = " + now);

        // 增加某种单位的数量，例如 1,ChronoUnit.CENTURIES 增加一个世纪的时间
        now = now.plus(1000, ChronoUnit.CENTURIES);
        System.out.println("now = " + now);
    }

    /**
     * 日期修改
     */
    public static void testWith() {
        now.withDayOfMonth(12);
        now.with(ChronoField.DAY_OF_YEAR, 1);
        now.with(TemporalAdjusters.lastDayOfYear());

        now.with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
        System.out.println("now = " + now);
    }

    /**
     * 调用自定义调节器
     */
    public static void testAdjuster() {
        PayDayAdjuster payDayAdjuster = new PayDayAdjuster();
        now = LocalDateTime.of(2020, 8, 15, 0, 0, 0);
        System.out.println(payDayAdjuster.adjustInto(LocalDateTime.now()));
    }
}
