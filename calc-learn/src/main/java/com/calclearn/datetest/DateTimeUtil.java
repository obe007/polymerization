package com.calclearn.datetest;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {
    public static LocalDate plusDays(LocalDate date, long daysToAdd) {
        int year = date.getYear();
        int month = date.getMonthValue();
        int day = date.getDayOfMonth();
        long dom = day + daysToAdd;
        // 今年剩余天数
        int hasTotalDays = lengthOfMonth(year, day) - day;  // 这个月还剩余多少天
        for (int i = month + 1; i <= 12; i++) {
            hasTotalDays +=  lengthOfMonth(year, i);
        }
        if (dom < 28) {
            return LocalDate.of(year, month, (int) dom);
        } else if (dom < (28 + 31)) {   // 不超过2个月
            int monthLen = lengthOfMonth(year, month);
            if (dom < lengthOfMonth(year, month)) {
                return LocalDate.of(year, month + 1, (int) dom - month);
            } else {
                return LocalDate.of(year, month + 1, (int) dom -lengthOfMonth(year, month));
            }
        } else if ((day + hasTotalDays) < 365) {
            int monthsToAdd = 1;
            hasTotalDays -= day;
            for (int i = month + 1; i <= 12; i++) {
                if (hasTotalDays - lengthOfMonth(year, i) > 0) {
                    hasTotalDays -= lengthOfMonth(year, i);
                    ++monthsToAdd;
                } else {
                    return LocalDate.of(year, month + (monthsToAdd - 1) , hasTotalDays);
                }
            }
        }
        System.out.println("date = " + date);
        return null;
    }

    public static int lengthOfMonth(int year, int month) {
        return switch(month) {
            case 1, 3, 5, 7, 8, 10, 12 -> 31;   // 31天
            case 2 -> isLeapYear(year, month) ? 29 : 28;
            case 4, 6, 9, 11 -> 30;
            default -> 0;
        };
    }

    private static boolean isLeapYear(int year, int month) {
        boolean isLearYear = (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
        System.out.println("是否闰年 = " + (isLearYear ? "是" : "不是"));
        return isLearYear;
    }

    public static void main(String[] args) {

        LocalDate date = plusDays(LocalDate.now(), 500L);
//        date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).;
        System.out.println("date = " + date);
//        System.out.println(lengthOfMonth(2000, 2));

        LocalDate jdkDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth()).plusDays(500);
        System.out.println("jdkDate = " + jdkDate);

        long now = Instant.now().getEpochSecond();
        long addDays = 500 * 24 * 60 * 60;
        LocalDate instantDate = LocalDate.ofInstant(Instant.ofEpochSecond(addDays + now), ZoneId.of("Asia/Shanghai"));
        System.out.println("instantDate = " + instantDate);

    }
}
