package com.calclearn.datetest;

import org.springframework.beans.factory.annotation.Configurable;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalUnit;

/**
 * 自定义时间调节器
 */
@Configurable
public class PayDayAdjuster implements TemporalAdjuster {
    @Override
    public Temporal adjustInto(Temporal temporal) {
        LocalDateTime dateTime = LocalDateTime.from(temporal);
        dateTime = dateTime.withDayOfMonth(15);
        if (DayOfWeek.SATURDAY == dateTime.getDayOfWeek() || DayOfWeek.SUNDAY.equals(dateTime.getDayOfWeek())) {
            return dateTime.with(ChronoField.DAY_OF_WEEK, 5);
        }
        return dateTime;
    }
}
