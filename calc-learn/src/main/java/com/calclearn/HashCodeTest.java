package com.calclearn;

/**
 * Created by WuTianyu on 2020/6/2 9:28.
 */
public class HashCodeTest {
    public static void main(String[] args) {
        int code1 = "aaa".hashCode();
        int code2 = Math.abs("六一儿童节2020".hashCode());
        System.out.println("code1 = " + code1);
        System.out.println(code2);
    }
}
