package com.calclearn;

import cn.hutool.crypto.digest.DigestUtil;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

/**
 * Created by WuTianyu on 2020/4/24 14:36.
 */
public class MD5Util {
    public static String getMD5String(String s){
        return DigestUtils.md5DigestAsHex(s.getBytes(StandardCharsets.UTF_16));
    }

    public static void main(String[] args) {
        byte[] sha256Str = DigestUtil.sha256("net user", "UTF-8");
        System.out.println("sha256Str = " + new String(sha256Str));
        System.out.println(MD5Util.getMD5String("was"));
    }
}
