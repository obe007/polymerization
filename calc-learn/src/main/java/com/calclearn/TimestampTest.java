package com.calclearn;

import java.security.Timestamp;
import java.time.Instant;

/**
 * Created by WuTianyu on 2020/4/22 15:29.
 */
public class TimestampTest {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        long timestamp2 = Instant.now().getEpochSecond();
        System.out.println("timestamp2 = " + timestamp2);
        System.out.println(System.currentTimeMillis() - start);

         start = System.currentTimeMillis();
        long timestamp1 = System.currentTimeMillis() / 1000;
        System.out.println("timestamp1 = " + timestamp1);
        System.out.println(System.currentTimeMillis() - start);
    }
}
