package com.calclearn.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * Created by WuTianyu on 2020/4/10 13:05.
 */
public class Demo {
    private String aaa;

    // student类
    class Student {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    public static void main(String[] args) throws NoSuchFieldException {
/*        Object o = new Object();
        Student s = (Student) o;
        s.setName("aaa");
        System.out.println("s = " + s);*/
        getVariable();
    }

    public static void getVariable() throws NoSuchFieldException {
//        try {
            /*Demo demo = new Demo();
            Constructor constructor = null;
            System.out.println("demo.aaa = " + demo.aaa);
            constructor = demo.getClass().getConstructor();
            Object obj = constructor.newInstance();
            Field field = obj.getClass().getField("aaa");
            field.set(obj, "");
            System.out.println("demo.aaa = " + obj);*/

        Demo demo = new Demo();
        Field[] fields = demo.getClass().getFields();
        // 设置运行访问private
        Arrays.stream(fields).forEach(f -> f.setAccessible(true));

        Field field = demo.getClass().getField("aaa");
        field.setAccessible(true);
        Method[] methods = demo.getClass().getDeclaredMethods();

        System.out.println("field = " + field);


        System.out.println("methods = " + methods);
/*        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }*/
    }
}
