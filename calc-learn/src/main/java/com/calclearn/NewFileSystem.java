package com.calclearn;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Editor;
import cn.hutool.core.lang.Filter;
import cn.hutool.core.util.ArrayUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by WuTianyu on 2020/07/21 10:44
 * 新函数测试 FileSystems.newFileSystem
 */
public class NewFileSystem {
    public static final String nodejs_zipUri = "D:\\Program Files\\nodejs.zip";

    public static void main(String[] args) {
        try {
            File nodejsZip = new File(nodejs_zipUri);
            System.out.println("//================================== FileSystems =============================//");
            FileSystem fileSystem = FileSystems.newFileSystem(nodejsZip.toPath());
            List<Path> filePath = new ArrayList<>();
            Iterable dirsIterable = fileSystem.getRootDirectories();
            Set<String> pathSet = new HashSet<>();
            List<String> dirs = new ArrayList<>();
            for (Iterator dirsIter = dirsIterable.iterator(); dirsIter.hasNext();) {
                Path nowDir = (Path)dirsIter.next();
                dirs.addAll( Files.list(nowDir).map(path -> path.toString()).collect(Collectors.toList()) );
                pathSet.addAll(dirs);
                filePath.add( nowDir );
            }

            Set<String> resultSet = getDeepDirs(fileSystem, pathSet, ArrayUtil.toArray(dirs, String.class));
            List<String> resultList = resultSet.stream().sorted().collect(Collectors.toList());
            resultList.forEach( path -> {
                System.out.println(path);
            });

        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    public static Set<String> getDeepDirs(FileSystem fileSystem /* 文件系统 */, Set<String> pathSet /* 所有Path */, String... dirPathArr /* 要深入的路径 */) {
        List<String> childDirs = new ArrayList<>();
        pathSet.addAll(ListUtil.toList(dirPathArr));
        for (String dirPath : dirPathArr) {
            Path nowDirPath = fileSystem.getPath(dirPath);
            // 当前文件夹的deep
            try {
                List<String> paths = Files.list(nowDirPath).filter(path -> Files.isDirectory(path)).map(path -> path.toString()).collect(Collectors.toList());
                if (paths.size() == 0) {
                    continue;
                }
                childDirs.addAll(paths);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (childDirs.isEmpty()) {
            return pathSet;
        }
        return getDeepDirs(fileSystem, pathSet, ArrayUtil.toArray(childDirs, String.class));
    }
    /*public static final String getDeep() {

    }*/

}
