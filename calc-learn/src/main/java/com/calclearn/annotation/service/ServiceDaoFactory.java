package com.calclearn.annotation.service;

import com.calclearn.annotation.TestAnnotation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by WuTianyu on 2020/4/10 14:23.
 */
public class ServiceDaoFactory {
    private static final ServiceDaoFactory factory = new ServiceDaoFactory();

    public static ServiceDaoFactory getInstance() {
        return factory;
    }

    // 判断用户是否有权限
    public <T> T createDao(String classname, Class<T> clazz) {
        System.out.println("添加分类进来了！！！");

        try {
            // 得到该类的类型
            final T type = (T) Class.forName(classname).getDeclaredConstructor().newInstance();
            // 返回一个动态代理对象
            return (T) Proxy.newProxyInstance(ServiceDaoFactory.class.getClassLoader(), type.getClass().getInterfaces(), (proxy, method, args) -> {
                // 判断用户调用的是什么方式
                String methodName = method.getName();
                System.out.println("methodName = " + methodName);

                // 得到用户调用的真实方法
                Method userMethod = type.getClass().getMethod(methodName, method.getParameterTypes());
                // 查看方法上有没有注解
                TestAnnotation testAnnotation = userMethod.getAnnotation(TestAnnotation.class);
                // 为空直接调用该方法
                if (testAnnotation == null) {
                    System.out.println("testAnnotation = 此方法没有");
                    return method.invoke(type, args);
                }

                // 如果注解不为空，得到注解的权限
                System.out.println("有此注解");
                return null;
            });
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
