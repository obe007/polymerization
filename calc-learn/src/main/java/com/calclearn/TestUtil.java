package com.calclearn;

import cn.hutool.core.lang.Editor;
import cn.hutool.core.util.ArrayUtil;
import com.calclearn.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;

public class TestUtil {
    @Autowired
    TestService testService;

    public void test() {
        testService.testShow();
    }

    public static void main(String[] args) {
        Integer[] a = {1,2,3,4,5,6};
        Integer[] filter = ArrayUtil.filter(a, new Editor<Integer>(){
            @Override
            public Integer edit(Integer t) {
                return (t % 2 == 0) ? t : null;
            }});
        for (Integer integer : filter) {
            System.out.println(integer);
        }
    }
}
