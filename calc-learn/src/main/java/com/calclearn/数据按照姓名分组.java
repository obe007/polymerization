package com.calclearn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by WuTianyu on 2020/4/27 9:28.
 */
public class 数据按照姓名分组 {

    public static void main(String[] args) {
        // 创建一个数据库记录对应的对象
        IllegalInfo illegalInfo = new IllegalInfo();
        // 添加记录
        IllegalInfo i1 = illegalInfo.builder().id("a1").softwareName("金蝶软件").email("zs@qq.com").empName("张三").build();
        IllegalInfo i2 = illegalInfo.builder().id("a1").softwareName("QQ通讯").email("zs@qq.com").empName("张三").build();
        IllegalInfo i3 = illegalInfo.builder().id("a2").softwareName("TIM通讯").email("ls@qq.com").empName("李四").build();
        IllegalInfo i4 = illegalInfo.builder().id("a2").softwareName("微信").email("ls@qq.com").empName("李四").build();
        // 集合，把数据库记录放进去
        List<IllegalInfo> illegalInfos = new ArrayList<>();
        illegalInfos.addAll(List.of(i1, i2, i3, i4));

        // 数据分组
        Map<String, List<IllegalInfo>> map = new ConcurrentHashMap<>();
        for (IllegalInfo info: illegalInfos) {
            List<IllegalInfo> list = map.get(info.getEmail());
            // 之前插入过
            if (list != null) {
                list.add(info);
                continue;
            } else {
                List newList = new ArrayList();
                newList.add(info);
                map.put(info.getEmail(), newList);
            }
        }

        // 加强循环的 使用
        for (Map.Entry<String, List<IllegalInfo>> entry : map.entrySet()) {
            // 邮箱
            String email = entry.getKey();
            // 使用的非法软件
            String softwareName = "";
            List<IllegalInfo> infos = map.get(entry.getKey());
            for (IllegalInfo info: infos) {
                softwareName += " 和 " + info.getSoftwareName();
            }
            System.out.println("邮箱 = " + email + " 非法软件 = " + softwareName);
        }
    }
}
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
class IllegalInfo {
    private String id;
    private String softwareName;
    private String email;
    private String empName;
}
