package com.calclearn;

/**
 * Created by WuTianyu on 2020/4/24 14:23.
 */
public class SignUtil {
    public static void main(String[] args) {
        System.out.println(getSignWithNgs("0016", "2020-04-02 00:00:00"));
    }

    public static String getSignWithNgs(String outstationNo, String startTime) {
        final String appId = "9672c7dc141348ecbd2e6d76f8af178f";
        final String appSecret = "ec6bf79d1b4bb02adcf051ffbe7aa75fc6613b14";
        String body = "{\n" +
                "    \"outstationNo\":\"" + outstationNo + "\",\n" +
                "    \"startTime\":\"" + startTime + "\"\n" +
                "}";
        String body2 = """
                """;
        String sb = appSecret +
                "appId=" + appId +
                "&body=" + body+
                "&timestamp=" + (/*System.currentTimeMillis() / 1000*/ 1586393642) +
                appSecret;
        String sign = MD5Util.getMD5String(sb).toUpperCase();
        return sign;
    }
}
