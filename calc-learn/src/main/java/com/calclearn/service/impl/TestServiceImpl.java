package com.calclearn.service.impl;

import com.calclearn.annotation.TestAnnotation;
import com.calclearn.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

    @Override
    @TestAnnotation
    public void testShow() {

    }
}
