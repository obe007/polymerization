package com.calclearn;

/**
 * Created by WuTianyu on 2020/5/29 12:54.
 * 一个函数调用另一个 return的函数
 */
public class CallThatFun {
    public static void main(String[] args) {
        System.out.println(1);
        test();
        System.out.println(3);

        String newString = """
                123asd
                asdasd
                111111111111
                """;
    }

    public static void test() {
        System.out.println(2);
        return;
    }
}
