package com.transaction;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

@SpringBootApplication
@Slf4j
public class SpringTransactionApplication implements CommandLineRunner {
    @Autowired
    private TransactionTemplate transactionTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private FooService fooService;

    public static void main(String[] args) {
        SpringApplication.run(SpringTransactionApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        transactionMode();
        runFooService();
    }

    private void transactionMode() {
        log.info("COUNT BEFORE TRANSACTION: {}", getCount());
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                for (int i = 0; i <= 999; i++) {
                    jdbcTemplate.execute("insert into foo(`bar`) values ('wty')");
                    log.info("COUNT BEFORE TRANSACTION: {}", getCount());
                }
                log.info("COUNT BEFORE TRANSACTION: {}", getCount());
//                transactionStatus.setRollbackOnly();
                transactionStatus.flush();
            }
        });

        log.info("COUNT BEFORE TRANSACTION: {}", getCount());
    }

    private long getCount() {
        return (long) jdbcTemplate.queryForList("select count(*) as cnt from foo").get(0).get("cnt");
    }

    private void runFooService() {
        fooService.insertRecord();
        log.info("aaa1 {}", jdbcTemplate.queryForList("select count(*) from foo where bar = 'test001'", Long.class));

        try {
            fooService.insertThenRollback();
        } catch (Exception e) {
            log.info("bbb2 {}", jdbcTemplate.queryForList("select count(*) from foo where bar = 'test002'", Long.class));
        }

        fooService.invokeInsertThenRollback();
        log.info("aaa3 {}", jdbcTemplate.queryForList("select count(*) from foo where bar = 'test001'", Long.class));

    }

}
