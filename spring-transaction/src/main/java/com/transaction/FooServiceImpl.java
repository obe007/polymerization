package com.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.RollbackException;

@Component
public class FooServiceImpl implements FooService {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    FooService fooService;

    @Override
    @Transactional
    public void insertRecord() {
        jdbcTemplate.execute("insert into foo (bar) values ('test001')");
    }

    @Override
    @Transactional(rollbackFor = RollbackException.class)
    public void insertThenRollback() throws RollbackException{
        jdbcTemplate.execute("insert into foo (bar) values ('test002')");
        throw new RollbackException();
    }

    @Override
    public void invokeInsertThenRollback() {
        fooService.insertRecord();
    }
}
