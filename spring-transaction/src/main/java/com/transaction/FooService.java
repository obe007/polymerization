package com.transaction;

public interface FooService {
    void insertRecord();

    void insertThenRollback();

    void invokeInsertThenRollback();
}
