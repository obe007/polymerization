package com.common;

public class Const {

    /** 使用的方式为UUID */
    public static final String UUID = "uuid2";

    public static final String emptyStr = "";

    /** 门店导出配送上报Excel path TODO Linux下路径 */
    public static final String PRODUCT_REPORT_PRE  = "/www/wwwfiles/product-report/";
    public static final String VEG_FRUIT_PRE  = "/www/wwwfiles/veg-fruit/";
    /** 门店导出配送上报Excel path TODO Windows下路径 */
//    public static final String PRODUCT_REPORT_PRE  = "D:/data/excel/product-report";
//    public static final String VEG_FRUIT_PRE  = "D:/data/excel/veg-fruit";

    public static final String CURRENT_USER = "currentUser";

    public static final String VALIDATE_CODE = "validateCode";

    public static final String USERNAME = "username";
    public static final String EMAIL = "email";


    /**
     * 全局变量
     */
    public static Long ONLINE_USER_NUMBER = 0L;   //在线用户数量

    public interface Role {
        int ROLE_CUSTOMER = 0; //普通用户
        int ROLE_ADMIN = 1; //管理员
    }

}
