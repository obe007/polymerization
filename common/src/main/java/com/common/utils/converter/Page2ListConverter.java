package com.common.utils.converter;

import org.springframework.data.domain.Page;

import java.util.List;

public class Page2ListConverter<T> {

    public static<T> List<T> convert(Page<T> page) {
        return page.getContent();
    }
}
