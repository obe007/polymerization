package com.common.utils;

import com.common.enums.ResponseCode;
import com.common.pojo.vo.ResultVO;

import java.io.Serializable;

public class ResultVOUtil implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建success 的几种可能
     * @param <T>
     * @return
     */
    public static <T> ResultVO success() {
        return new ResultVO( ResponseCode.SUCCESS.getCode() );
    }

    public static <T> ResultVO successMessage() {
        return new ResultVO( ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMsg() );
    }

    public static <T> ResultVO successMessage(String msg) {
        return new ResultVO( ResponseCode.SUCCESS.getCode(), msg );
    }

    public static <T> ResultVO success(T data) {
        return new ResultVO( ResponseCode.SUCCESS.getCode(), ResponseCode.SUCCESS.getMsg(), data );
    }

    public static <T> ResultVO success(String msg, T data) {
        return new ResultVO( ResponseCode.SUCCESS.getCode(), msg, data );
    }

    /**
     * 创建ERROR
     * @param <T>
     * @return
     */
    public static <T> ResultVO error() {
        return new ResultVO(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getMsg());
    }

    public static <T> ResultVO error(Integer code, String msg) {
        return new ResultVO(code, msg);
    }



}
