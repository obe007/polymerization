package com.common.enums;

public enum ResponseCode {

    SUCCESS(0, "成功"),
    ERROR(1, "失败"),
    NEED_LOGIN(10, "需要登录"),
    ILLEGAL_ARGUMENT(2, "非法参数");

    private final int code;
    private final String msg;

    ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

}
