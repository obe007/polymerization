package com.rocketmq.demo.manager.rocketmq.producer;

import com.rocketmq.demo.RocketmqDemoApplicationTests;
import com.rocketmq.demo.common.constant.rocketmq.RocketConst;
import com.rocketmq.demo.manager.rocketmq.producer.impl.TransactionListener2Impl;
import com.rocketmq.demo.manager.rocketmq.producer.impl.TransactionListenerImpl;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.common.message.Message;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class SyncProducerTest extends RocketmqDemoApplicationTests {
    @Resource
    private SyncProducer syncProducer;
    @Resource
    public TransactionProducer transactionProducer;

    @Test
    void TestSendMsg() {
        String msgBody = "1这是我的消息哟哟";
        Message message1 = new Message(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG, msgBody.getBytes());
        Message message2 = new Message(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG, "2这是我的消息哟哟".getBytes());
        List<Message> messages = new ArrayList<>();
        messages.add(message1);
        messages.add(message2);
        syncProducer.sendMessage(messages);
    }


    @Test
    void TestSendTransactionMain() {
        try {
            for (int i = 0; i < 10; i++) {
                new Thread(this::testSendTransactionMsg1).start();
                new Thread(this::testSendTransactionMsg2).start();
            }
            Thread.sleep(Integer.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Test
    void testSendTransactionMsg1() {
        System.out.println("方法1开始执行了 当前线程: " + Thread.currentThread().getName() );
        Message message2 = new Message(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG, "事务消息1111".getBytes());

        transactionProducer.setTransactionListener(new TransactionListenerImpl());

        TransactionSendResult transactionSendResult = transactionProducer.sendMessage(message2);
/*        String result = switch (transactionSendResult.getSendStatus()) {
            case SEND_OK -> "成功返回结果了";
            case FLUSH_DISK_TIMEOUT -> "刷盘超时";
            case FLUSH_SLAVE_TIMEOUT -> "从节点刷盘超时";
            case SLAVE_NOT_AVAILABLE -> "从节点不可用";
            default -> "未知";
        };*/
        System.out.println("方法1执行完毕");
     /*   try {

            Thread.sleep(Integer.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
    }
    @Test
    void testSendTransactionMsg2() {
        System.out.println("方法2开始执行了 当前线程: " + Thread.currentThread().getName() );
        Message message2 = new Message(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG, "事务消息2222".getBytes());
        transactionProducer.setTransactionListener(new TransactionListener2Impl());
        transactionProducer.sendMessage(message2);
        System.out.println("方法2执行完毕");
    }


}
class sendMsg2 implements Runnable {
    @Resource
    public TransactionProducer transactionProducer;

    @Override
    public void run() {
        System.out.println("方法2开始执行了 当前线程: " + Thread.currentThread().getName() );
        Message message2 = new Message(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG, "1.这是事务消息哟哟2222".getBytes());
        transactionProducer.setTransactionListener(new TransactionListener2Impl());
        transactionProducer.sendMessage(message2);
        System.out.println("方法2执行完毕");    }
}