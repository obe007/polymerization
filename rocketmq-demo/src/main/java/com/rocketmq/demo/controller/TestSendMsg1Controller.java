package com.rocketmq.demo.controller;

import com.rocketmq.demo.common.constant.rocketmq.RocketConst;
import com.rocketmq.demo.manager.rocketmq.producer.TransactionProducer;
import com.rocketmq.demo.manager.rocketmq.producer.impl.TransactionListenerImpl;
import org.apache.rocketmq.common.message.Message;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by WuTianyu on 2020/07/15 13:17
 */
@RequestMapping("")
@RestController
public class TestSendMsg1Controller {
    @Resource
    private TransactionProducer transactionProducer;

    @GetMapping("send1")
    public String sendMsg() {
        Message message1 = new Message(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG, "事务消息1111".getBytes());
        transactionProducer.setTransactionListener(new TransactionListenerImpl());
        transactionProducer.sendMessage(message1);
        return "事务消息1111";
    }
}
