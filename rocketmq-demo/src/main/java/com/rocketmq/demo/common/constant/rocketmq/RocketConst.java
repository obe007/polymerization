package com.rocketmq.demo.common.constant.rocketmq;

/**
 * Created by WuTianyu on 2020/5/6 10:56.
 */
public interface RocketConst {
    /**
     * topic 主题
     */
    interface Topic {
        String TOPIC_TEST = "TOPIC_TEST";

        String CALLBACK_ORDER = "CALLBACK_ORDER_TOPIC";
    }
    /**
     * Tag 标签
     */
    interface Tag {
        String TAG_A = "TAG_A";
        String TAG_B = "TAG_B";
        String CALLBACK_PAY_TAG = "callback_pay_tag";
    }

    /**
     * ConsumerGroup 消费者组
     */
    interface ConsumerGroup {
        String PRAISE_CONSUMER = "producer-test-group";

        String ConsumerGroupName1 = "consumer-test-group1";

        String ConsumerGroupName2 = "consumer-test-group2";

        String OUTSTATION_NO_0001 = "0001";

        String OUTSTATION_NO_3001 = "3001";

        String ORDER_CONSUMER = "order_consumer_group";
    }

    /**
     * nameSrv地址池
     */
    interface NameSrvAddress {
        String NAME_SRV_ADDR_MULTI_MASTER = "172.20.0.223:9876;10.10.22.91:9876";    // ;10.10.22.91:9876;
    }

    /**
     * ProducerGroup 生产者组
     */
    interface ProducerGroup {
        String PRAISE_CONSUMER = ConsumerGroup.PRAISE_CONSUMER;

        String TRANSACTION_GROUP = "TRANSACTION_GROUP";

        String PAY_A = "tx_pay_producer_group_name";
        String CALLBACK_PAY_GROUP_NAME = "callback_pay_group_name";
    }

    String brokerName = "broker-a";

    int  MAX_RECONSUME_TIMES = 3;
}
