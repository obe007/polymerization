package com.rocketmq.demo.manager.rocketmq.producer.impl;

/**
 * Created by WuTianyu on 2020/07/15 15:29
 */

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.stereotype.Component;

/**
 * 想办法业务解耦
 */
@Component
@Slf4j
@ToString
public class TransactionListenerImpl implements TransactionListener {
    public TransactionListenerImpl() {

    }

    /**
     * When send transactional prepare(half) message succeed, this method will be invoked to execute local transaction.
     *
     * @param msg Half(prepare) message
     * @param arg Custom business parameter
     * @return Transaction state
     */
    @Override
    public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
        log.info("============================= 执行本地事务1111 ===============================");
        if (msg == null || msg.getBody().length == 0) {
            return LocalTransactionState.UNKNOW;
        }
        try {
            log.info("【新消息】{}", new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET));
            return LocalTransactionState.COMMIT_MESSAGE;
        } catch (Exception e) {
            return LocalTransactionState.UNKNOW;
        }
    }

    /**
     * When no response to prepare(half) message. broker will send check message to check the transaction status, and this
     * method will be invoked to get local transaction status.
     *
     * @param msg Check message
     * @return Transaction state
     */
    @Override
    public LocalTransactionState checkLocalTransaction(MessageExt msg) {
        log.info("============================= 回查本地事务11 ===============================");
        log.info("成功后才能发消息11");
        return LocalTransactionState.COMMIT_MESSAGE;
    }
}
