package com.rocketmq.demo.manager.rocketmq;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ReflectUtil;
import com.rocketmq.demo.manager.rocketmq.producer.impl.TransactionListenerImpl;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by WuTianyu on 2020/07/15 15:27
 */
public class Test {
    public static <T> Method[] setTransactionListenerImpl(Class<T> clazz) {
        return ReflectUtil.getPublicMethods(clazz);
    }

    public static void main(String[] args) {
//        ReflectUtil.setFieldValue(Field);
        List<String> records = FileUtil.readLines("F:\\Users\\Administrator\\Desktop\\邻里购NGS\\test1.txt", RemotingHelper.DEFAULT_CHARSET);
        Long msg1Count = records.stream().filter(a -> a.contains("【新消息】msg = 事务消息1111")).count();
        Long msg2Count = records.stream().filter(a -> a.contains("【新消息】msg = 事务消息2222")).count();
        System.out.println("msg1Count = " + msg1Count + ", msg2Count = " + msg2Count);
    }
}
