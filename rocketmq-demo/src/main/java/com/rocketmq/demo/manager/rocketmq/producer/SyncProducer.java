package com.rocketmq.demo.manager.rocketmq.producer;

import com.rocketmq.demo.common.constant.rocketmq.RocketConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by WuTianyu on 2020/07/14 14:38
 */
@Component
@Slf4j
public class SyncProducer {
    /** 同步生产者 */
    private DefaultMQProducer producer;
    /** 生产者组 */
    private String PRODUCER_GROUP = RocketConst.ProducerGroup.PRAISE_CONSUMER;
    /** name server address */
    private String NAMESRV_ADDR = RocketConst.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER;
    /** 默认重试次数 */
    private int RETRY_TIMES_WHEN_SEND_FAILED = 3;

    private SyncProducer() {
        this.producer = new TransactionMQProducer(PRODUCER_GROUP);
        this.producer.setNamesrvAddr(NAMESRV_ADDR);
        // retry num of times with error send
        this.producer.setRetryTimesWhenSendFailed(RETRY_TIMES_WHEN_SEND_FAILED);
        this.start();
    }

    private void start() {
        try {
            this.producer.start();
            log.info("【同步生产者】【已启动】");
        } catch (MQClientException e) {
            log.error("【同步生产者】【启动失败】errorMsg = {}", e.getErrorMessage());
        }
    }

    public void shutdown() {
        this.producer.shutdown();
    }

    //========================================== sendMessage ================================================

    /**
     * @param topic topic
     * @param content message body
     * @return SendResult
     */
    public SendResult sendMessage(String topic, String content) {
        return sendMessage(new Message(topic, content.getBytes()));
    }

    /**
     * @param topic topic
     * @param tags tags
     * @param content message body
     * @return SendResult
     */
    public SendResult sendMessage(String topic, String tags, String content) {
        return sendMessage(new Message(topic, tags, content.getBytes()));
    }

    /**
     * sendMessageInTransaction
     * @param msg 消息
     * @return TransactionSendResult 或 null
     */
    public SendResult sendMessage(Message msg) {
        try {
            return this.producer.send(msg);
        } catch (MQClientException e) {
            log.error("【发送消息】【发送失败】MQClientException, errorMsg = {}", e.getErrorMessage());
        } catch (InterruptedException e) {
            log.error("【发送消息】【发送失败】InterruptedException, errorMsg = {}", e.getMessage());
        } catch (RemotingException e) {
            log.error("【发送消息】【发送失败】RemotingException, errorMsg = {}", e.getMessage());
        } catch (MQBrokerException e) {
            log.error("【发送消息】【发送失败】MQBrokerException, errorMsg = {}", e.getMessage());
        }
        return null;
    }

    /**
     * sendMessageInTransaction
     * @param msgs 消息
     * @return TransactionSendResult 或 null
     */
    public SendResult sendMessage(Collection<Message> msgs) {
        try {
            return this.producer.send(msgs);
        } catch (MQClientException e) {
            log.error("【发送消息】【发送失败】MQClientException, errorMsg = {}", e.getErrorMessage());
        } catch (InterruptedException e) {
            log.error("【发送消息】【发送失败】InterruptedException, errorMsg = {}", e.getMessage());
        } catch (RemotingException e) {
            log.error("【发送消息】【发送失败】RemotingException, errorMsg = {}", e.getMessage());
        } catch (MQBrokerException e) {
            log.error("【发送消息】【发送失败】MQBrokerException, errorMsg = {}", e.getMessage());
        }
        return null;
    }

//========================================== 参数化设置, 需要发送消息前先配置 ================================================

    /**
     * 设置 重试次数
     * @param retryTimesWhenSendFailed retry num of times
     */
    public void setRetryTimesWhenSendFailed(int retryTimesWhenSendFailed) {
        this.producer.setRetryTimesWhenSendFailed(retryTimesWhenSendFailed);
    }
}
