package com.rocketmq.demo.manager.rocketmq.producer;

import com.rocketmq.demo.common.constant.rocketmq.RocketConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.lang.reflect.Field;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by WuTianyu on 2020/07/24 14:38
 */
@Component
//@Scope("prototype")
@Slf4j
public class TransactionProducer /*implements InitializingBean */{
    /** 事务生产者 */
    private TransactionMQProducer producer;
    /**  回查本地事务 -> 线程池 */
    private ExecutorService executorService;
    /** 异步回调函数 -> 反射赋值 */
    /*@Resource
    private TransactionListener transactionListenerImpl;*/
    /** 生产者组 */
    private String PRODUCER_GROUP = RocketConst.ProducerGroup.TRANSACTION_GROUP;
    /** name server address */
    private String NAMESRV_ADDR = RocketConst.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER;
    /** retry num of times */
    private int RETRY_TIMES_WHEN_SEND_FAILED = 3;

    /*================================= Reflection related ===========================================*/

    /*================================= executorService ===========================================*/
    /** 核心线程池数量 */
    public static int corePoolSize = 2;
    /** 核心线程池数量 */
    public static int maximumPoolSize = 10;
    /** 等待时间 */
    public static long keepAliveTime = 100;
    /** capacity */
    public static int ARRAY_BLOCKING_QUEUE_CAPACITY = 2000;

    private TransactionProducer() {
        this.producer = new TransactionMQProducer(PRODUCER_GROUP);
        this.producer.setNamesrvAddr(NAMESRV_ADDR);
        this.producer.setRetryTimesWhenSendFailed(this.RETRY_TIMES_WHEN_SEND_FAILED);
        // 设置线程池
        this.executorService = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<>(ARRAY_BLOCKING_QUEUE_CAPACITY), r -> {
            Thread thread = new Thread(r);
            thread.setName(PRODUCER_GROUP + "-check-thread");
            return thread;
        });
        this.producer.setExecutorService(this.executorService);
        this.start();
    }

    /**
     * 初始化bean时 执行
     */
//    @Override
//    public void afterPropertiesSet() {
//        /*this.producer.setTransactionListener(transactionListenerImpl);*/
//        this.start();
//    }

    private void start() {
        try {
            this.producer.start();
            log.info("【事务生产者】【已启动】");
        } catch (MQClientException e) {
            log.error("【事务生产者】【启动失败】errorMsg = {}", e.getErrorMessage());
        }
    }

    public void shutdown() {
        this.producer.shutdown();
    }


    //========================================== sendMessage ================================================

    /**
     * @param topic topic
     * @param content message body
     * @return SendResult
     */
    public TransactionSendResult sendMessage(String topic, String content) {
        return sendMessage(new Message(topic, content.getBytes()));
    }

    /**
     * @param topic topic
     * @param tags tags
     * @param content message body
     * @return SendResult
     */
    public TransactionSendResult sendMessage(String topic, String tags, String content) {
        return sendMessage(new Message(topic, tags, content.getBytes()));
    }

    /**
     * sendMessageInTransaction
     * @param msg 消息
     * @return TransactionSendResult 或 null
     */
    public TransactionSendResult sendMessage(Message msg) {
        return sendMessage(msg, null);
    }

    /**
     * 发送消息
     * @param msg 消息
     * @param arg 参数
     * @return TransactionSendResult
     */
    public TransactionSendResult sendMessage(Message msg, Object arg) {
        try {
            return this.producer.sendMessageInTransaction(msg, arg);
        } catch (MQClientException e) {
            log.error("【事务消息】【发送失败】errorMsg = {}", e.getErrorMessage());
        }
        return null;
    }

    //========================================== 设置动态事务监听 ================================================

    public void setTransactionListener(Object obj) {
        log.info("【回调函数】【更新监听类】class = {}", obj);
        // 回调监听类: 执行本地事务 & 回查本地事务 -> RocketMQ事务生产者回调监听对象的名字
        setFieldValue(producer, "transactionListener", obj);
    }

    /**
     * 设置字段值
     *
     * @param obj   对象，如果是static字段，此参数为null
     * @param fieldName 字段
     * @param value 值，值类型必须与字段类型匹配，不会自动转换对象类型
     */
    public static void setFieldValue(Object obj, String fieldName, Object value) {
        Assert.notNull(obj, "【设置事务监听类】【ERROR】监听类 = null");

        // get Field -> by field name
        final Field field = getField(obj.getClass(), fieldName);
        // field access
        if (!field.canAccess(obj)) {
            field.setAccessible(true);
        }
        try {
            field.set(obj instanceof Class ? null : obj, value);
        } catch (IllegalAccessException e) {
            log.error("【设置监听类】【ERROR】errorMsg = {}", e.getMessage());
        }
    }

    /**
     * 查找指定类中的所有字段（包括非public字段），也包括父类和Object类的字段， 字段不存在则返回<code>null</code>
     *
     * @param beanClass 被查找字段的类,不能为null
     * @param name      字段名
     * @return 字段
     */
    public static Field getField(Class<?> beanClass, String name) {
        final Field[] fields = beanClass.getDeclaredFields();
        if (fields != null && fields.length != 0) {
            for (Field field: fields) {
                if ((name.equals( field.getName() ))) {
                    return field;
                }
            }
        }
        log.error("【ERROR】【getFiled is no one】");
        return null;
    }

    //========================================== 参数化设置, 需要发送消息前先配置 ================================================

    /**
     * 设置 重试次数
     * @param retryTimesWhenSendFailed retry num of times
     */
    public void setRetryTimesWhenSendFailed(int retryTimesWhenSendFailed) {
        this.producer.setRetryTimesWhenSendFailed(retryTimesWhenSendFailed);
    }
}