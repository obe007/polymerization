package com.rocketmq.demo.manager.rocketmq.consumer;

import cn.hutool.core.io.FileUtil;
import com.rocketmq.demo.common.constant.rocketmq.RocketConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by WuTianyu on 2020/07/15 10:35
 */
@Slf4j
public class PushConsumer {
    public static final String consumerFilePath = "F:\\Users\\Administrator\\Desktop\\邻里购NGS\\test1.txt";
    public static List<String> msgList = Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args) {
        try {
            DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketConst.ConsumerGroup.ORDER_CONSUMER);
            log.info("【订单状态修改消费者】【启动】【OrderStatusUpdateConsumer】");
            consumer.setNamesrvAddr(RocketConst.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
            consumer.setConsumeThreadMin(10);
            consumer.setConsumeThreadMax(50);
            consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
            consumer.subscribe(RocketConst.Topic.CALLBACK_ORDER, RocketConst.Tag.CALLBACK_PAY_TAG);
            consumer.registerMessageListener(new MessageListenerConcurrently4Pay());
            consumer.start();


        } catch (MQClientException e) {
            log.error("【订单消费者】【异常】errorMsg = {}", e.getErrorMessage());
        }
}
static class MessageListenerConcurrently4Pay implements MessageListenerConcurrently {
        /**
         * It is not recommend to throw exception,rather than returning ConsumeConcurrentlyStatus.RECONSUME_LATER if
         * consumption failure
         *
         * @param msgs    msgs.size() >= 1<br> DefaultMQPushConsumer.consumeMessageBatchMaxSize=1,you can modify here
         * @param context
         * @return The consume status
         */
        @Override
        public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
//            log.info("【订单状态修改消费者】【触发】");
            if (msgs.isEmpty()) {
                log.info("【订单消费者】【无数据】");
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
            if (msgs.size() != 1) {
                log.error("失败");
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
            try {
                MessageExt msg = msgs.get(0);
                String msgBody = new StringBuilder(new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET)).append("\r\n").toString();
                log.info("【新消息】msg = {}", msgBody);
                msgList.add(msgBody);
                if (msgList.size() == (712 + 987)) {
                    writeFile(msgList);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            } catch (Exception e) {
                log.error("【消费失败】errorMsg = {}", e.getMessage());
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        }
    }

    public static void writeFile(Collection content) {
        FileUtil.writeUtf8Lines(content, consumerFilePath);
    }
}