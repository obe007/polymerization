package com.druiddemo;

import com.alibaba.druid.filter.FilterChain;
import com.alibaba.druid.filter.FilterEventAdapter;
import com.alibaba.druid.proxy.jdbc.ConnectionProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Slf4j
@Component  //声明为组件之后才会把当前类注册为bean 被使用
public class ConnectLogFilter extends FilterEventAdapter {


    @Override
    public void connection_connectBefore(FilterChain chain, Properties info) {
        log.info("BEFORE CONNECTION!! 连接成功之前");
    }

    public void connection_connectAfter(ConnectionProxy connection) {
        log.info("BEFORE CONNECTION!! 连接成功之后");
    }
}
