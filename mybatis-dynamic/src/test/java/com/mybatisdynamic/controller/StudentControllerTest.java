package com.mybatisdynamic.controller;

import cn.hutool.core.util.StrUtil;
import com.mybatisdynamic.mapper.mysql.*;
import com.mybatisdynamic.mapper.sqlserver.PeiHuoDao;
import com.mybatisdynamic.mapper.sqlserver.StoreCommodityDao;
import com.mybatisdynamic.mapper.sqlserver.VegetableMainDao;
import com.mybatisdynamic.pojo.entity.mysql.*;
import com.mybatisdynamic.pojo.entity.sqlserver.PeiHuo;
import com.mybatisdynamic.pojo.entity.sqlserver.StoreCommodity;
import com.mybatisdynamic.pojo.entity.sqlserver.VegetableMain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StudentControllerTest {

    @Autowired
    PeiHuoDao peiHuoDao;
    @Autowired
    ProductReportDao productReportDao;

    @Test
    public void list() {
    }

    @Test
    public void ms2mysql() {
        List<PeiHuo> peiHuos = peiHuoDao.selectAll();
        List<ProductReport> productReports = new LinkedList<>();
        peiHuos.parallelStream().forEach(peiHuo -> {
            ProductReport report = new ProductReport();
            report.setFilingDate( peiHuo.getDq() );
            report.setStoreNo( peiHuo.getShpno() );
            report.setSixBitCode( peiHuo.getcCode() );
            report.setProductName( peiHuo.getShpname() );
            report.setScale( peiHuo.getGuige() );
            report.setStock( new BigDecimal(peiHuo.getJskc()));
            report.setLastMonthSale( new BigDecimal(peiHuo.getSyxs()) );
            report.setPromotionType( peiHuo.getCxfs() );
            report.setDeliveryType( peiHuo.getPsfs() );
            report.setSettlementType( peiHuo.getJs() );
            report.setRetailType( peiHuo.getSfcl() );
            report.setOriginalPurchasePrice( peiHuo.getYjj() );
            report.setPromotionPurchasePrice( peiHuo.getCxjj() );
            report.setPromotionSalePrice( peiHuo.getCxsj() );
            report.setSuggestedReplenishment( new BigDecimal(peiHuo.getJyphl()) );
            report.setRealReplenishment( new BigDecimal(peiHuo.getMdyhl()) );
            report.setMinReplenishment(new BigDecimal(peiHuo.getZxqdl()));
            report.setDeleteStatus( 0 );
            int i = productReportDao.insertSelective( report );
            System.out.println("report = " + report + ", 是否成功=" + i);
        });

    }

    @Autowired
    StoreCommodityDao storeCommodityDao;
    @Autowired
    StoreInfoDao storeInfoDao;
    @Test
    public void ms2mysql_storeCommody() {
        List<StoreInfo> storeInfos = new LinkedList<>();
//        List<StoreCommodity> storeCommodities = storeCommodityDao.selectAll();
        List<StoreCommodity> storeCommodities = storeCommodityDao.selectAll();
        final Long count[] = {0L};
        storeCommodities.parallelStream()
                .sorted(
                        Comparator.comparing(storeCommodity -> Integer.parseInt(storeCommodity.getStore()))
                )
                .forEach(storeCommodity -> {
            StoreInfo storeInfo = new StoreInfo();
            storeInfo.setStoreNo(storeCommodity.getStore());
            storeInfo.setName(storeCommodity.getStoreName());
            storeInfo.setOrderCycle(storeCommodity.getOrdCycle());
            if (StrUtil.equals("y",storeCommodity.getIsauto())) {
                storeInfo.setAutoReplenish( 1 );
            } else if (StrUtil.equals("n",storeCommodity.getIsauto())) {
                storeInfo.setAutoReplenish( 0 );
            }
            storeInfo.setIp(storeCommodity.getIp());
            storeInfo.setLocationCode(storeCommodity.getDistNo().toString());
            storeInfo.setOldStoreNo(storeCommodity.getOldNo());
            storeInfoDao.insertSelective(storeInfo);
            System.out.println("count = " + (count[0]+=1L));
        });
    }

    @Autowired
    VegFruitDao vegFruitDao;
    @Test
    public void vegFruitAll() {
        VegFruit vegFruit = new VegFruit();
        vegFruit.setName("123123123");
//        List<VegFruit> vegFruits = vegFruit.selectAll();

        List<VegFruit> vegFruits = new ArrayList<>();
//        vegFruits = vegFruitDao.selectList(null);
//        vegFruits = vegFruitDao.selectAll();
        vegFruitDao.insert(vegFruit);
        System.out.println(" = " + vegFruits);
    }

//    @Autowired
//    VegetableMainDao vegetableMainDao;
//    @Autowired
//    VegFruitCategoryDao vegFruitCategoryDao;
//    @Test
//    public void vegetableMain2Mysql() {
//        final Long count[] = {0L};
//        VegFruitCategoryExample example = new VegFruitCategoryExample();
//        example.createCriteria();
//        final List<VegFruitCategory> vegFruitCategoryList = vegFruitCategoryDao.selectByExample(example);
//        List<VegetableMain> vegetableMains = new ArrayList<>();
//        vegetableMains = vegetableMainDao.selectAll();
//
//        vegetableMains.parallelStream().forEach(vegetableMain -> {
//            VegFruit vegFruit = new VegFruit();
//            String categoryName = StrUtil.subAfter(vegetableMain.getCataName(), '-', true);
//            for (VegFruitCategory vegFruitCategory: vegFruitCategoryList) {
//                if (vegFruitCategory.getName().equals(categoryName)) {
//                    vegFruit.setVegFruitCategoryId(vegFruitCategory.getId());
//                }
//            }
//            vegFruit.setSixBitCode( vegetableMain.getcCode() );
//            vegFruit.setName( vegetableMain.getcName() );
//            vegFruit.setCategoryName( vegetableMain.getCataName() );
//            vegFruit.setSaleUnit( vegetableMain.getSellUnit() );
//            vegFruit.setBoxfulScale( vegetableMain.getxScale() );
//            vegFruit.setOrderScale( vegetableMain.getScale() );
//            vegFruit.setSuggestedReplenishment( new BigDecimal(vegetableMain.getOrderSlMin()) );
//            vegFruit.setPurchasePrice( vegetableMain.getImportPrice() );
//            vegFruit.setSalePrice( vegetableMain.getSellPrice() );
//            int resultValue = vegFruitDao.insertSelective(vegFruit);
//            System.out.println("count = " + ( resultValue>=1?count[0]+=1L:"失败啦" ));
//        });
//    }

    @Autowired
    GreenqtyDao greenqtyDao;
    @Autowired
    VegFruitReplenishmentDao vegFruitReplenishmentDao;
    @Test
    public void vegFruitReplenishmentTest() {
        final AtomicLong count[] = {new AtomicLong(0L)};
        List<Greenqty> greenqtyList = greenqtyDao.selectAll();

        greenqtyList.parallelStream().forEach(greenqty -> {
            VegFruitReplenishment vegFruitReplenishment = new VegFruitReplenishment();
            vegFruitReplenishment.setStoreNo( greenqty.getStoreNo() );
            vegFruitReplenishment.setSixBitCode( greenqty.getSixBitCode() );
            vegFruitReplenishment.setReplenishment( new BigDecimal(greenqty.getQty()) );
            int resultValue = vegFruitReplenishmentDao.insertSelective(vegFruitReplenishment);
            System.out.println("count = " + ( resultValue>=1?count[0].getAndAdd(1L):"失败啦" ));
        });
    }
}
