package com.mybatisdynamic.service;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.mybatisdynamic.mapper.mysql.StudentMapper;
import com.mybatisdynamic.pojo.entity.sqlserver.Commodity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CommodityServiceTest {

    @Autowired
    CommodityService commodityService;

    @Autowired
    StudentMapper studentMapper;

    @Test
    public void selectAll() {
        System.out.println(commodityService.selectAll());
    }

    @Test
    public void selectStudent() {
        System.out.println(studentMapper.selectAll());
    }


    /***
     * Commodity的数据写入硬盘
     */

    @Test
    public void writeLog() {
        final String filePath = "E:\\data\\log\\commodityJson.log";
        List<Commodity> commodities = commodityService.selectAll();
        String commodityJson = JSON.toJSONString( commodities );

        File file = FileUtil.writeUtf8String(commodityJson, new File(filePath));
        if (file != null) {
            System.out.println("file = " + "成功");
        } else {
            System.out.println("file = " + "失败");
        }
    }
}