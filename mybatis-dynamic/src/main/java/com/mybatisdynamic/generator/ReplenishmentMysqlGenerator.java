package com.mybatisdynamic.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// 演示例子，执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
public class ReplenishmentMysqlGenerator {

    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
//        String projectPath = System.getProperty("user.dir");
        String projectPath = "G:/replenishment";
        gc.setOutputDir(projectPath + "/management/src/main/java");

        gc.setAuthor("Jax");    //生成文件主人 时间等信息
        gc.setOpen(false);  //完成后打开文件夹
        gc.setSwagger2(true); //实体属性 Swagger2 注解
        gc.setFileOverride(true);   //覆盖同名文件
        gc.setActiveRecord(true);  //ActiveRecord 模式    像JPA一样强大
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap  baseResultMap
        gc.setBaseColumnList(true);// XML columLis  生成 XML文件里resultMap
        gc.setDateType(DateType.TIME_PACK);   // 时间类型对应策略

        gc.setEntityName("%s");
        gc.setMapperName("%sDao");
        gc.setXmlName("%sDao");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setIdType(IdType.AUTO);  //  id自增模式
        mpg.setGlobalConfig(gc);


        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://10.10.22.90/auto_replenishment?serverTimezone=Asia/Shanghai&characterEncoding=utf-8&useSSL=false");
        // dsc.setSchemaName("public"); //mysql不需要
        dsc.setDbType(DbType.MARIADB);
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("kaixuan123");

        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("");

        pc.setEntity("com.ngs.management.pojo.mysql");
        pc.setMapper("com.ngs.management.dao");
        pc.setXml("delete");
        pc.setController("com.ngs.management.controller");

        pc.setService("com.ngs.management.service");
        pc.setServiceImpl("com.ngs.management.service.impl");

        mpg.setPackageInfo(pc);



        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";

        // 如果模板引擎是 velocity
//         String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/management/src/main/resources/mapper/" + tableInfo.getEntityName() + "Dao" + StringPool.DOT_XML;
            }
        });
        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录");
                return false;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
//        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
//         templateConfig.setEntity("templates/entity2.java");
//         templateConfig.setService(null);
//         templateConfig.setController(null);
//
//        templateConfig.setXml(null);    //路径
//        mpg.setTemplate(templateConfig);

        // 策略配置 数据库表配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setSkipView(false);    //跳过视图
        strategy.setNaming(NamingStrategy.underline_to_camel);  // 驼峰式
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);    // 驼峰式
        strategy.setEntityLombokModel(true);    // 【实体】是否为lombok模型（默认 false）
        strategy.setRestControllerStyle(true);  // 生成RestController

//        strategy.setEntityBuilderModel(true);   // 是否为构建者模型（默认 false）
//        strategy.setEntityLombokModel(true);    // 是否为lombok模型

        strategy.setControllerMappingHyphenStyle(true); // 驼峰转连字符

        strategy.setEntityTableFieldAnnotationEnable(false); // 是否生成实体时，生成字段注解


        // 公共父类
//        strategy.setSuperControllerClass("com.mybatisdynamic.controller");
        // 写于父类中的公共字段
//        strategy.setSuperEntityColumns("id");
        strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

}
