package com.mybatisdynamic.pojo.entity.sqlserver;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * greenqty_sum
 * @author 
 */
public class GreenqtySum implements Serializable {
    private String storeNo;

    private String cCode;

    private Integer qty;

    private Integer status;

    private LocalDateTime orderDate;

    private static final long serialVersionUID = 1L;

    public String getStoreNo() {
        return storeNo;
    }

    public void setStoreNo(String storeNo) {
        this.storeNo = storeNo;
    }

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }
}