package com.mybatisdynamic.pojo.entity.mysql;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 蔬果分类;蔬果要货(产品分类)
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("veg_fruit_category")
@ApiModel(value="VegFruitCategory对象", description="蔬果分类;蔬果要货(产品分类)")
public class VegFruitCategory extends Model<VegFruitCategory> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID;序号")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "上级分类的id：0表示一级分类")
    private Long parentId;

    @ApiModelProperty(value = "类目名字")
    private String name;

    @ApiModelProperty(value = "分类级别：0->1级；1->2级；2->3级")
    private Boolean level;

    @ApiModelProperty(value = "蔬果数量")
    private Integer vegFruitCount;

    @ApiModelProperty(value = "(计量或计数用的)单位")
    private Integer vegFruitUnit;

    @ApiModelProperty(value = "是否显示在导航栏：0->不显示；1->显示")
    private Integer navStatus;

    @ApiModelProperty(value = "显示状态：0->不显示；1->显示")
    private Integer showStatus;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "关键词")
    private String keywords;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
