package com.mybatisdynamic.pojo.entity.mysql;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 蔬果要货产品表(蔬果要货功能中的 蔬果产品表)
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("veg_fruit")
@ApiModel(value="VegFruit对象", description="蔬果要货产品表(蔬果要货功能中的 蔬果产品表)")
public class VegFruit extends Model<VegFruit> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID;序号")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "商品编码(六位码)")
    private String sixBitCode;

    @ApiModelProperty(value = "品名(商品名称)")
    private String name;

    @ApiModelProperty(value = "展示图片")
    private String pic;

    @ApiModelProperty(value = "蔬果分类id")
    private Long vegFruitCategoryId;

    @ApiModelProperty(value = "小类名(蔬果完整分类名)")
    private String categoryName;

    @ApiModelProperty(value = "零售单位(销售单位)")
    private String saleUnit;

    @ApiModelProperty(value = "箱规格(一箱的量)")
    private String boxfulScale;

    @ApiModelProperty(value = "订货规格")
    private BigDecimal orderScale;

    @ApiModelProperty(value = "建议点货数量(推荐补货量)")
    private BigDecimal suggestedReplenishment;

    @ApiModelProperty(value = "进价(进货价)")
    private BigDecimal purchasePrice;

    @ApiModelProperty(value = "售价(销售价)")
    private BigDecimal salePrice;

    @ApiModelProperty(value = "备注信息")
    private String note;

    @ApiModelProperty(value = "删除状态：0->未删除；1->已删除")
    private Integer deleteStatus;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
