package com.mybatisdynamic.pojo.entity.mysql;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ProductReportExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ProductReportExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFilingDateIsNull() {
            addCriterion("filing_date is null");
            return (Criteria) this;
        }

        public Criteria andFilingDateIsNotNull() {
            addCriterion("filing_date is not null");
            return (Criteria) this;
        }

        public Criteria andFilingDateEqualTo(String value) {
            addCriterion("filing_date =", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateNotEqualTo(String value) {
            addCriterion("filing_date <>", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateGreaterThan(String value) {
            addCriterion("filing_date >", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateGreaterThanOrEqualTo(String value) {
            addCriterion("filing_date >=", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateLessThan(String value) {
            addCriterion("filing_date <", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateLessThanOrEqualTo(String value) {
            addCriterion("filing_date <=", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateLike(String value) {
            addCriterion("filing_date like", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateNotLike(String value) {
            addCriterion("filing_date not like", value, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateIn(List<String> values) {
            addCriterion("filing_date in", values, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateNotIn(List<String> values) {
            addCriterion("filing_date not in", values, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateBetween(String value1, String value2) {
            addCriterion("filing_date between", value1, value2, "filingDate");
            return (Criteria) this;
        }

        public Criteria andFilingDateNotBetween(String value1, String value2) {
            addCriterion("filing_date not between", value1, value2, "filingDate");
            return (Criteria) this;
        }

        public Criteria andStoreNoIsNull() {
            addCriterion("store_no is null");
            return (Criteria) this;
        }

        public Criteria andStoreNoIsNotNull() {
            addCriterion("store_no is not null");
            return (Criteria) this;
        }

        public Criteria andStoreNoEqualTo(String value) {
            addCriterion("store_no =", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoNotEqualTo(String value) {
            addCriterion("store_no <>", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoGreaterThan(String value) {
            addCriterion("store_no >", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoGreaterThanOrEqualTo(String value) {
            addCriterion("store_no >=", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoLessThan(String value) {
            addCriterion("store_no <", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoLessThanOrEqualTo(String value) {
            addCriterion("store_no <=", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoLike(String value) {
            addCriterion("store_no like", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoNotLike(String value) {
            addCriterion("store_no not like", value, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoIn(List<String> values) {
            addCriterion("store_no in", values, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoNotIn(List<String> values) {
            addCriterion("store_no not in", values, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoBetween(String value1, String value2) {
            addCriterion("store_no between", value1, value2, "storeNo");
            return (Criteria) this;
        }

        public Criteria andStoreNoNotBetween(String value1, String value2) {
            addCriterion("store_no not between", value1, value2, "storeNo");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeIsNull() {
            addCriterion("six_bit_code is null");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeIsNotNull() {
            addCriterion("six_bit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeEqualTo(String value) {
            addCriterion("six_bit_code =", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotEqualTo(String value) {
            addCriterion("six_bit_code <>", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeGreaterThan(String value) {
            addCriterion("six_bit_code >", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeGreaterThanOrEqualTo(String value) {
            addCriterion("six_bit_code >=", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeLessThan(String value) {
            addCriterion("six_bit_code <", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeLessThanOrEqualTo(String value) {
            addCriterion("six_bit_code <=", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeLike(String value) {
            addCriterion("six_bit_code like", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotLike(String value) {
            addCriterion("six_bit_code not like", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeIn(List<String> values) {
            addCriterion("six_bit_code in", values, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotIn(List<String> values) {
            addCriterion("six_bit_code not in", values, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeBetween(String value1, String value2) {
            addCriterion("six_bit_code between", value1, value2, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotBetween(String value1, String value2) {
            addCriterion("six_bit_code not between", value1, value2, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNull() {
            addCriterion("product_name is null");
            return (Criteria) this;
        }

        public Criteria andProductNameIsNotNull() {
            addCriterion("product_name is not null");
            return (Criteria) this;
        }

        public Criteria andProductNameEqualTo(String value) {
            addCriterion("product_name =", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotEqualTo(String value) {
            addCriterion("product_name <>", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThan(String value) {
            addCriterion("product_name >", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameGreaterThanOrEqualTo(String value) {
            addCriterion("product_name >=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThan(String value) {
            addCriterion("product_name <", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLessThanOrEqualTo(String value) {
            addCriterion("product_name <=", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameLike(String value) {
            addCriterion("product_name like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotLike(String value) {
            addCriterion("product_name not like", value, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameIn(List<String> values) {
            addCriterion("product_name in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotIn(List<String> values) {
            addCriterion("product_name not in", values, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameBetween(String value1, String value2) {
            addCriterion("product_name between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andProductNameNotBetween(String value1, String value2) {
            addCriterion("product_name not between", value1, value2, "productName");
            return (Criteria) this;
        }

        public Criteria andPicIsNull() {
            addCriterion("pic is null");
            return (Criteria) this;
        }

        public Criteria andPicIsNotNull() {
            addCriterion("pic is not null");
            return (Criteria) this;
        }

        public Criteria andPicEqualTo(String value) {
            addCriterion("pic =", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotEqualTo(String value) {
            addCriterion("pic <>", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThan(String value) {
            addCriterion("pic >", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThanOrEqualTo(String value) {
            addCriterion("pic >=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThan(String value) {
            addCriterion("pic <", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThanOrEqualTo(String value) {
            addCriterion("pic <=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLike(String value) {
            addCriterion("pic like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotLike(String value) {
            addCriterion("pic not like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicIn(List<String> values) {
            addCriterion("pic in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotIn(List<String> values) {
            addCriterion("pic not in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicBetween(String value1, String value2) {
            addCriterion("pic between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotBetween(String value1, String value2) {
            addCriterion("pic not between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andScaleIsNull() {
            addCriterion("`scale` is null");
            return (Criteria) this;
        }

        public Criteria andScaleIsNotNull() {
            addCriterion("`scale` is not null");
            return (Criteria) this;
        }

        public Criteria andScaleEqualTo(String value) {
            addCriterion("`scale` =", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotEqualTo(String value) {
            addCriterion("`scale` <>", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleGreaterThan(String value) {
            addCriterion("`scale` >", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleGreaterThanOrEqualTo(String value) {
            addCriterion("`scale` >=", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleLessThan(String value) {
            addCriterion("`scale` <", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleLessThanOrEqualTo(String value) {
            addCriterion("`scale` <=", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleLike(String value) {
            addCriterion("`scale` like", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotLike(String value) {
            addCriterion("`scale` not like", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleIn(List<String> values) {
            addCriterion("`scale` in", values, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotIn(List<String> values) {
            addCriterion("`scale` not in", values, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleBetween(String value1, String value2) {
            addCriterion("`scale` between", value1, value2, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotBetween(String value1, String value2) {
            addCriterion("`scale` not between", value1, value2, "scale");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(BigDecimal value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(BigDecimal value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(BigDecimal value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(BigDecimal value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<BigDecimal> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<BigDecimal> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andMinStockIsNull() {
            addCriterion("min_stock is null");
            return (Criteria) this;
        }

        public Criteria andMinStockIsNotNull() {
            addCriterion("min_stock is not null");
            return (Criteria) this;
        }

        public Criteria andMinStockEqualTo(BigDecimal value) {
            addCriterion("min_stock =", value, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockNotEqualTo(BigDecimal value) {
            addCriterion("min_stock <>", value, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockGreaterThan(BigDecimal value) {
            addCriterion("min_stock >", value, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("min_stock >=", value, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockLessThan(BigDecimal value) {
            addCriterion("min_stock <", value, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("min_stock <=", value, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockIn(List<BigDecimal> values) {
            addCriterion("min_stock in", values, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockNotIn(List<BigDecimal> values) {
            addCriterion("min_stock not in", values, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_stock between", value1, value2, "minStock");
            return (Criteria) this;
        }

        public Criteria andMinStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_stock not between", value1, value2, "minStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockIsNull() {
            addCriterion("max_stock is null");
            return (Criteria) this;
        }

        public Criteria andMaxStockIsNotNull() {
            addCriterion("max_stock is not null");
            return (Criteria) this;
        }

        public Criteria andMaxStockEqualTo(BigDecimal value) {
            addCriterion("max_stock =", value, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockNotEqualTo(BigDecimal value) {
            addCriterion("max_stock <>", value, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockGreaterThan(BigDecimal value) {
            addCriterion("max_stock >", value, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("max_stock >=", value, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockLessThan(BigDecimal value) {
            addCriterion("max_stock <", value, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockLessThanOrEqualTo(BigDecimal value) {
            addCriterion("max_stock <=", value, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockIn(List<BigDecimal> values) {
            addCriterion("max_stock in", values, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockNotIn(List<BigDecimal> values) {
            addCriterion("max_stock not in", values, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("max_stock between", value1, value2, "maxStock");
            return (Criteria) this;
        }

        public Criteria andMaxStockNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("max_stock not between", value1, value2, "maxStock");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleIsNull() {
            addCriterion("last_month_sale is null");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleIsNotNull() {
            addCriterion("last_month_sale is not null");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleEqualTo(BigDecimal value) {
            addCriterion("last_month_sale =", value, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleNotEqualTo(BigDecimal value) {
            addCriterion("last_month_sale <>", value, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleGreaterThan(BigDecimal value) {
            addCriterion("last_month_sale >", value, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("last_month_sale >=", value, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleLessThan(BigDecimal value) {
            addCriterion("last_month_sale <", value, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("last_month_sale <=", value, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleIn(List<BigDecimal> values) {
            addCriterion("last_month_sale in", values, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleNotIn(List<BigDecimal> values) {
            addCriterion("last_month_sale not in", values, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("last_month_sale between", value1, value2, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andLastMonthSaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("last_month_sale not between", value1, value2, "lastMonthSale");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeIsNull() {
            addCriterion("promotion_type is null");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeIsNotNull() {
            addCriterion("promotion_type is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeEqualTo(String value) {
            addCriterion("promotion_type =", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotEqualTo(String value) {
            addCriterion("promotion_type <>", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeGreaterThan(String value) {
            addCriterion("promotion_type >", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeGreaterThanOrEqualTo(String value) {
            addCriterion("promotion_type >=", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeLessThan(String value) {
            addCriterion("promotion_type <", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeLessThanOrEqualTo(String value) {
            addCriterion("promotion_type <=", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeLike(String value) {
            addCriterion("promotion_type like", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotLike(String value) {
            addCriterion("promotion_type not like", value, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeIn(List<String> values) {
            addCriterion("promotion_type in", values, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotIn(List<String> values) {
            addCriterion("promotion_type not in", values, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeBetween(String value1, String value2) {
            addCriterion("promotion_type between", value1, value2, "promotionType");
            return (Criteria) this;
        }

        public Criteria andPromotionTypeNotBetween(String value1, String value2) {
            addCriterion("promotion_type not between", value1, value2, "promotionType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeIsNull() {
            addCriterion("delivery_type is null");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeIsNotNull() {
            addCriterion("delivery_type is not null");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeEqualTo(String value) {
            addCriterion("delivery_type =", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeNotEqualTo(String value) {
            addCriterion("delivery_type <>", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeGreaterThan(String value) {
            addCriterion("delivery_type >", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeGreaterThanOrEqualTo(String value) {
            addCriterion("delivery_type >=", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeLessThan(String value) {
            addCriterion("delivery_type <", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeLessThanOrEqualTo(String value) {
            addCriterion("delivery_type <=", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeLike(String value) {
            addCriterion("delivery_type like", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeNotLike(String value) {
            addCriterion("delivery_type not like", value, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeIn(List<String> values) {
            addCriterion("delivery_type in", values, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeNotIn(List<String> values) {
            addCriterion("delivery_type not in", values, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeBetween(String value1, String value2) {
            addCriterion("delivery_type between", value1, value2, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andDeliveryTypeNotBetween(String value1, String value2) {
            addCriterion("delivery_type not between", value1, value2, "deliveryType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeIsNull() {
            addCriterion("settlement_type is null");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeIsNotNull() {
            addCriterion("settlement_type is not null");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeEqualTo(String value) {
            addCriterion("settlement_type =", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeNotEqualTo(String value) {
            addCriterion("settlement_type <>", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeGreaterThan(String value) {
            addCriterion("settlement_type >", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeGreaterThanOrEqualTo(String value) {
            addCriterion("settlement_type >=", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeLessThan(String value) {
            addCriterion("settlement_type <", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeLessThanOrEqualTo(String value) {
            addCriterion("settlement_type <=", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeLike(String value) {
            addCriterion("settlement_type like", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeNotLike(String value) {
            addCriterion("settlement_type not like", value, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeIn(List<String> values) {
            addCriterion("settlement_type in", values, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeNotIn(List<String> values) {
            addCriterion("settlement_type not in", values, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeBetween(String value1, String value2) {
            addCriterion("settlement_type between", value1, value2, "settlementType");
            return (Criteria) this;
        }

        public Criteria andSettlementTypeNotBetween(String value1, String value2) {
            addCriterion("settlement_type not between", value1, value2, "settlementType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeIsNull() {
            addCriterion("retail_type is null");
            return (Criteria) this;
        }

        public Criteria andRetailTypeIsNotNull() {
            addCriterion("retail_type is not null");
            return (Criteria) this;
        }

        public Criteria andRetailTypeEqualTo(String value) {
            addCriterion("retail_type =", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeNotEqualTo(String value) {
            addCriterion("retail_type <>", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeGreaterThan(String value) {
            addCriterion("retail_type >", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeGreaterThanOrEqualTo(String value) {
            addCriterion("retail_type >=", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeLessThan(String value) {
            addCriterion("retail_type <", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeLessThanOrEqualTo(String value) {
            addCriterion("retail_type <=", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeLike(String value) {
            addCriterion("retail_type like", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeNotLike(String value) {
            addCriterion("retail_type not like", value, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeIn(List<String> values) {
            addCriterion("retail_type in", values, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeNotIn(List<String> values) {
            addCriterion("retail_type not in", values, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeBetween(String value1, String value2) {
            addCriterion("retail_type between", value1, value2, "retailType");
            return (Criteria) this;
        }

        public Criteria andRetailTypeNotBetween(String value1, String value2) {
            addCriterion("retail_type not between", value1, value2, "retailType");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceIsNull() {
            addCriterion("original_purchase_price is null");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceIsNotNull() {
            addCriterion("original_purchase_price is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceEqualTo(BigDecimal value) {
            addCriterion("original_purchase_price =", value, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceNotEqualTo(BigDecimal value) {
            addCriterion("original_purchase_price <>", value, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceGreaterThan(BigDecimal value) {
            addCriterion("original_purchase_price >", value, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("original_purchase_price >=", value, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceLessThan(BigDecimal value) {
            addCriterion("original_purchase_price <", value, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("original_purchase_price <=", value, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceIn(List<BigDecimal> values) {
            addCriterion("original_purchase_price in", values, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceNotIn(List<BigDecimal> values) {
            addCriterion("original_purchase_price not in", values, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_purchase_price between", value1, value2, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andOriginalPurchasePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("original_purchase_price not between", value1, value2, "originalPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceIsNull() {
            addCriterion("promotion_purchase_price is null");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceIsNotNull() {
            addCriterion("promotion_purchase_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceEqualTo(BigDecimal value) {
            addCriterion("promotion_purchase_price =", value, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceNotEqualTo(BigDecimal value) {
            addCriterion("promotion_purchase_price <>", value, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceGreaterThan(BigDecimal value) {
            addCriterion("promotion_purchase_price >", value, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_purchase_price >=", value, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceLessThan(BigDecimal value) {
            addCriterion("promotion_purchase_price <", value, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_purchase_price <=", value, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceIn(List<BigDecimal> values) {
            addCriterion("promotion_purchase_price in", values, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceNotIn(List<BigDecimal> values) {
            addCriterion("promotion_purchase_price not in", values, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_purchase_price between", value1, value2, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionPurchasePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_purchase_price not between", value1, value2, "promotionPurchasePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceIsNull() {
            addCriterion("promotion_sale_price is null");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceIsNotNull() {
            addCriterion("promotion_sale_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceEqualTo(BigDecimal value) {
            addCriterion("promotion_sale_price =", value, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceNotEqualTo(BigDecimal value) {
            addCriterion("promotion_sale_price <>", value, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceGreaterThan(BigDecimal value) {
            addCriterion("promotion_sale_price >", value, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_sale_price >=", value, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceLessThan(BigDecimal value) {
            addCriterion("promotion_sale_price <", value, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("promotion_sale_price <=", value, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceIn(List<BigDecimal> values) {
            addCriterion("promotion_sale_price in", values, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceNotIn(List<BigDecimal> values) {
            addCriterion("promotion_sale_price not in", values, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_sale_price between", value1, value2, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andPromotionSalePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("promotion_sale_price not between", value1, value2, "promotionSalePrice");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentIsNull() {
            addCriterion("min_replenishment is null");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentIsNotNull() {
            addCriterion("min_replenishment is not null");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentEqualTo(BigDecimal value) {
            addCriterion("min_replenishment =", value, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentNotEqualTo(BigDecimal value) {
            addCriterion("min_replenishment <>", value, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentGreaterThan(BigDecimal value) {
            addCriterion("min_replenishment >", value, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("min_replenishment >=", value, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentLessThan(BigDecimal value) {
            addCriterion("min_replenishment <", value, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentLessThanOrEqualTo(BigDecimal value) {
            addCriterion("min_replenishment <=", value, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentIn(List<BigDecimal> values) {
            addCriterion("min_replenishment in", values, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentNotIn(List<BigDecimal> values) {
            addCriterion("min_replenishment not in", values, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_replenishment between", value1, value2, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andMinReplenishmentNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("min_replenishment not between", value1, value2, "minReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentIsNull() {
            addCriterion("suggested_replenishment is null");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentIsNotNull() {
            addCriterion("suggested_replenishment is not null");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment =", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentNotEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment <>", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentGreaterThan(BigDecimal value) {
            addCriterion("suggested_replenishment >", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment >=", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentLessThan(BigDecimal value) {
            addCriterion("suggested_replenishment <", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentLessThanOrEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment <=", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentIn(List<BigDecimal> values) {
            addCriterion("suggested_replenishment in", values, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentNotIn(List<BigDecimal> values) {
            addCriterion("suggested_replenishment not in", values, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("suggested_replenishment between", value1, value2, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("suggested_replenishment not between", value1, value2, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentIsNull() {
            addCriterion("real_replenishment is null");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentIsNotNull() {
            addCriterion("real_replenishment is not null");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentEqualTo(BigDecimal value) {
            addCriterion("real_replenishment =", value, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentNotEqualTo(BigDecimal value) {
            addCriterion("real_replenishment <>", value, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentGreaterThan(BigDecimal value) {
            addCriterion("real_replenishment >", value, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("real_replenishment >=", value, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentLessThan(BigDecimal value) {
            addCriterion("real_replenishment <", value, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentLessThanOrEqualTo(BigDecimal value) {
            addCriterion("real_replenishment <=", value, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentIn(List<BigDecimal> values) {
            addCriterion("real_replenishment in", values, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentNotIn(List<BigDecimal> values) {
            addCriterion("real_replenishment not in", values, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("real_replenishment between", value1, value2, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andRealReplenishmentNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("real_replenishment not between", value1, value2, "realReplenishment");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNull() {
            addCriterion("delete_status is null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNotNull() {
            addCriterion("delete_status is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusEqualTo(Integer value) {
            addCriterion("delete_status =", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotEqualTo(Integer value) {
            addCriterion("delete_status <>", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThan(Integer value) {
            addCriterion("delete_status >", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("delete_status >=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThan(Integer value) {
            addCriterion("delete_status <", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThanOrEqualTo(Integer value) {
            addCriterion("delete_status <=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIn(List<Integer> values) {
            addCriterion("delete_status in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotIn(List<Integer> values) {
            addCriterion("delete_status not in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusBetween(Integer value1, Integer value2) {
            addCriterion("delete_status between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("delete_status not between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(LocalDateTime value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(LocalDateTime value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(LocalDateTime value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(LocalDateTime value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<LocalDateTime> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<LocalDateTime> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(LocalDateTime value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(LocalDateTime value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(LocalDateTime value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(LocalDateTime value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<LocalDateTime> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<LocalDateTime> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}