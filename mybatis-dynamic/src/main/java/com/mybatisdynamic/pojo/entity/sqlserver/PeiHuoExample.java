package com.mybatisdynamic.pojo.entity.sqlserver;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PeiHuoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PeiHuoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andDqIsNull() {
            addCriterion("dq is null");
            return (Criteria) this;
        }

        public Criteria andDqIsNotNull() {
            addCriterion("dq is not null");
            return (Criteria) this;
        }

        public Criteria andDqEqualTo(String value) {
            addCriterion("dq =", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqNotEqualTo(String value) {
            addCriterion("dq <>", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqGreaterThan(String value) {
            addCriterion("dq >", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqGreaterThanOrEqualTo(String value) {
            addCriterion("dq >=", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqLessThan(String value) {
            addCriterion("dq <", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqLessThanOrEqualTo(String value) {
            addCriterion("dq <=", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqLike(String value) {
            addCriterion("dq like", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqNotLike(String value) {
            addCriterion("dq not like", value, "dq");
            return (Criteria) this;
        }

        public Criteria andDqIn(List<String> values) {
            addCriterion("dq in", values, "dq");
            return (Criteria) this;
        }

        public Criteria andDqNotIn(List<String> values) {
            addCriterion("dq not in", values, "dq");
            return (Criteria) this;
        }

        public Criteria andDqBetween(String value1, String value2) {
            addCriterion("dq between", value1, value2, "dq");
            return (Criteria) this;
        }

        public Criteria andDqNotBetween(String value1, String value2) {
            addCriterion("dq not between", value1, value2, "dq");
            return (Criteria) this;
        }

        public Criteria andShpnoIsNull() {
            addCriterion("shpno is null");
            return (Criteria) this;
        }

        public Criteria andShpnoIsNotNull() {
            addCriterion("shpno is not null");
            return (Criteria) this;
        }

        public Criteria andShpnoEqualTo(String value) {
            addCriterion("shpno =", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoNotEqualTo(String value) {
            addCriterion("shpno <>", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoGreaterThan(String value) {
            addCriterion("shpno >", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoGreaterThanOrEqualTo(String value) {
            addCriterion("shpno >=", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoLessThan(String value) {
            addCriterion("shpno <", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoLessThanOrEqualTo(String value) {
            addCriterion("shpno <=", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoLike(String value) {
            addCriterion("shpno like", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoNotLike(String value) {
            addCriterion("shpno not like", value, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoIn(List<String> values) {
            addCriterion("shpno in", values, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoNotIn(List<String> values) {
            addCriterion("shpno not in", values, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoBetween(String value1, String value2) {
            addCriterion("shpno between", value1, value2, "shpno");
            return (Criteria) this;
        }

        public Criteria andShpnoNotBetween(String value1, String value2) {
            addCriterion("shpno not between", value1, value2, "shpno");
            return (Criteria) this;
        }

        public Criteria andCCodeIsNull() {
            addCriterion("c_code is null");
            return (Criteria) this;
        }

        public Criteria andCCodeIsNotNull() {
            addCriterion("c_code is not null");
            return (Criteria) this;
        }

        public Criteria andCCodeEqualTo(String value) {
            addCriterion("c_code =", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotEqualTo(String value) {
            addCriterion("c_code <>", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeGreaterThan(String value) {
            addCriterion("c_code >", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeGreaterThanOrEqualTo(String value) {
            addCriterion("c_code >=", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeLessThan(String value) {
            addCriterion("c_code <", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeLessThanOrEqualTo(String value) {
            addCriterion("c_code <=", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeLike(String value) {
            addCriterion("c_code like", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotLike(String value) {
            addCriterion("c_code not like", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeIn(List<String> values) {
            addCriterion("c_code in", values, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotIn(List<String> values) {
            addCriterion("c_code not in", values, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeBetween(String value1, String value2) {
            addCriterion("c_code between", value1, value2, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotBetween(String value1, String value2) {
            addCriterion("c_code not between", value1, value2, "cCode");
            return (Criteria) this;
        }

        public Criteria andShpnameIsNull() {
            addCriterion("shpname is null");
            return (Criteria) this;
        }

        public Criteria andShpnameIsNotNull() {
            addCriterion("shpname is not null");
            return (Criteria) this;
        }

        public Criteria andShpnameEqualTo(String value) {
            addCriterion("shpname =", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameNotEqualTo(String value) {
            addCriterion("shpname <>", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameGreaterThan(String value) {
            addCriterion("shpname >", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameGreaterThanOrEqualTo(String value) {
            addCriterion("shpname >=", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameLessThan(String value) {
            addCriterion("shpname <", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameLessThanOrEqualTo(String value) {
            addCriterion("shpname <=", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameLike(String value) {
            addCriterion("shpname like", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameNotLike(String value) {
            addCriterion("shpname not like", value, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameIn(List<String> values) {
            addCriterion("shpname in", values, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameNotIn(List<String> values) {
            addCriterion("shpname not in", values, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameBetween(String value1, String value2) {
            addCriterion("shpname between", value1, value2, "shpname");
            return (Criteria) this;
        }

        public Criteria andShpnameNotBetween(String value1, String value2) {
            addCriterion("shpname not between", value1, value2, "shpname");
            return (Criteria) this;
        }

        public Criteria andGuigeIsNull() {
            addCriterion("guige is null");
            return (Criteria) this;
        }

        public Criteria andGuigeIsNotNull() {
            addCriterion("guige is not null");
            return (Criteria) this;
        }

        public Criteria andGuigeEqualTo(String value) {
            addCriterion("guige =", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeNotEqualTo(String value) {
            addCriterion("guige <>", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeGreaterThan(String value) {
            addCriterion("guige >", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeGreaterThanOrEqualTo(String value) {
            addCriterion("guige >=", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeLessThan(String value) {
            addCriterion("guige <", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeLessThanOrEqualTo(String value) {
            addCriterion("guige <=", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeLike(String value) {
            addCriterion("guige like", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeNotLike(String value) {
            addCriterion("guige not like", value, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeIn(List<String> values) {
            addCriterion("guige in", values, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeNotIn(List<String> values) {
            addCriterion("guige not in", values, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeBetween(String value1, String value2) {
            addCriterion("guige between", value1, value2, "guige");
            return (Criteria) this;
        }

        public Criteria andGuigeNotBetween(String value1, String value2) {
            addCriterion("guige not between", value1, value2, "guige");
            return (Criteria) this;
        }

        public Criteria andJskcIsNull() {
            addCriterion("jskc is null");
            return (Criteria) this;
        }

        public Criteria andJskcIsNotNull() {
            addCriterion("jskc is not null");
            return (Criteria) this;
        }

        public Criteria andJskcEqualTo(String value) {
            addCriterion("jskc =", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcNotEqualTo(String value) {
            addCriterion("jskc <>", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcGreaterThan(String value) {
            addCriterion("jskc >", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcGreaterThanOrEqualTo(String value) {
            addCriterion("jskc >=", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcLessThan(String value) {
            addCriterion("jskc <", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcLessThanOrEqualTo(String value) {
            addCriterion("jskc <=", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcLike(String value) {
            addCriterion("jskc like", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcNotLike(String value) {
            addCriterion("jskc not like", value, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcIn(List<String> values) {
            addCriterion("jskc in", values, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcNotIn(List<String> values) {
            addCriterion("jskc not in", values, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcBetween(String value1, String value2) {
            addCriterion("jskc between", value1, value2, "jskc");
            return (Criteria) this;
        }

        public Criteria andJskcNotBetween(String value1, String value2) {
            addCriterion("jskc not between", value1, value2, "jskc");
            return (Criteria) this;
        }

        public Criteria andSyxsIsNull() {
            addCriterion("syxs is null");
            return (Criteria) this;
        }

        public Criteria andSyxsIsNotNull() {
            addCriterion("syxs is not null");
            return (Criteria) this;
        }

        public Criteria andSyxsEqualTo(String value) {
            addCriterion("syxs =", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsNotEqualTo(String value) {
            addCriterion("syxs <>", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsGreaterThan(String value) {
            addCriterion("syxs >", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsGreaterThanOrEqualTo(String value) {
            addCriterion("syxs >=", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsLessThan(String value) {
            addCriterion("syxs <", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsLessThanOrEqualTo(String value) {
            addCriterion("syxs <=", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsLike(String value) {
            addCriterion("syxs like", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsNotLike(String value) {
            addCriterion("syxs not like", value, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsIn(List<String> values) {
            addCriterion("syxs in", values, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsNotIn(List<String> values) {
            addCriterion("syxs not in", values, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsBetween(String value1, String value2) {
            addCriterion("syxs between", value1, value2, "syxs");
            return (Criteria) this;
        }

        public Criteria andSyxsNotBetween(String value1, String value2) {
            addCriterion("syxs not between", value1, value2, "syxs");
            return (Criteria) this;
        }

        public Criteria andCxfsIsNull() {
            addCriterion("cxfs is null");
            return (Criteria) this;
        }

        public Criteria andCxfsIsNotNull() {
            addCriterion("cxfs is not null");
            return (Criteria) this;
        }

        public Criteria andCxfsEqualTo(String value) {
            addCriterion("cxfs =", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsNotEqualTo(String value) {
            addCriterion("cxfs <>", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsGreaterThan(String value) {
            addCriterion("cxfs >", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsGreaterThanOrEqualTo(String value) {
            addCriterion("cxfs >=", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsLessThan(String value) {
            addCriterion("cxfs <", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsLessThanOrEqualTo(String value) {
            addCriterion("cxfs <=", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsLike(String value) {
            addCriterion("cxfs like", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsNotLike(String value) {
            addCriterion("cxfs not like", value, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsIn(List<String> values) {
            addCriterion("cxfs in", values, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsNotIn(List<String> values) {
            addCriterion("cxfs not in", values, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsBetween(String value1, String value2) {
            addCriterion("cxfs between", value1, value2, "cxfs");
            return (Criteria) this;
        }

        public Criteria andCxfsNotBetween(String value1, String value2) {
            addCriterion("cxfs not between", value1, value2, "cxfs");
            return (Criteria) this;
        }

        public Criteria andPsfsIsNull() {
            addCriterion("psfs is null");
            return (Criteria) this;
        }

        public Criteria andPsfsIsNotNull() {
            addCriterion("psfs is not null");
            return (Criteria) this;
        }

        public Criteria andPsfsEqualTo(String value) {
            addCriterion("psfs =", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsNotEqualTo(String value) {
            addCriterion("psfs <>", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsGreaterThan(String value) {
            addCriterion("psfs >", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsGreaterThanOrEqualTo(String value) {
            addCriterion("psfs >=", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsLessThan(String value) {
            addCriterion("psfs <", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsLessThanOrEqualTo(String value) {
            addCriterion("psfs <=", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsLike(String value) {
            addCriterion("psfs like", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsNotLike(String value) {
            addCriterion("psfs not like", value, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsIn(List<String> values) {
            addCriterion("psfs in", values, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsNotIn(List<String> values) {
            addCriterion("psfs not in", values, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsBetween(String value1, String value2) {
            addCriterion("psfs between", value1, value2, "psfs");
            return (Criteria) this;
        }

        public Criteria andPsfsNotBetween(String value1, String value2) {
            addCriterion("psfs not between", value1, value2, "psfs");
            return (Criteria) this;
        }

        public Criteria andJsIsNull() {
            addCriterion("js is null");
            return (Criteria) this;
        }

        public Criteria andJsIsNotNull() {
            addCriterion("js is not null");
            return (Criteria) this;
        }

        public Criteria andJsEqualTo(String value) {
            addCriterion("js =", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsNotEqualTo(String value) {
            addCriterion("js <>", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsGreaterThan(String value) {
            addCriterion("js >", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsGreaterThanOrEqualTo(String value) {
            addCriterion("js >=", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsLessThan(String value) {
            addCriterion("js <", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsLessThanOrEqualTo(String value) {
            addCriterion("js <=", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsLike(String value) {
            addCriterion("js like", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsNotLike(String value) {
            addCriterion("js not like", value, "js");
            return (Criteria) this;
        }

        public Criteria andJsIn(List<String> values) {
            addCriterion("js in", values, "js");
            return (Criteria) this;
        }

        public Criteria andJsNotIn(List<String> values) {
            addCriterion("js not in", values, "js");
            return (Criteria) this;
        }

        public Criteria andJsBetween(String value1, String value2) {
            addCriterion("js between", value1, value2, "js");
            return (Criteria) this;
        }

        public Criteria andJsNotBetween(String value1, String value2) {
            addCriterion("js not between", value1, value2, "js");
            return (Criteria) this;
        }

        public Criteria andSfclIsNull() {
            addCriterion("sfcl is null");
            return (Criteria) this;
        }

        public Criteria andSfclIsNotNull() {
            addCriterion("sfcl is not null");
            return (Criteria) this;
        }

        public Criteria andSfclEqualTo(String value) {
            addCriterion("sfcl =", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclNotEqualTo(String value) {
            addCriterion("sfcl <>", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclGreaterThan(String value) {
            addCriterion("sfcl >", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclGreaterThanOrEqualTo(String value) {
            addCriterion("sfcl >=", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclLessThan(String value) {
            addCriterion("sfcl <", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclLessThanOrEqualTo(String value) {
            addCriterion("sfcl <=", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclLike(String value) {
            addCriterion("sfcl like", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclNotLike(String value) {
            addCriterion("sfcl not like", value, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclIn(List<String> values) {
            addCriterion("sfcl in", values, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclNotIn(List<String> values) {
            addCriterion("sfcl not in", values, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclBetween(String value1, String value2) {
            addCriterion("sfcl between", value1, value2, "sfcl");
            return (Criteria) this;
        }

        public Criteria andSfclNotBetween(String value1, String value2) {
            addCriterion("sfcl not between", value1, value2, "sfcl");
            return (Criteria) this;
        }

        public Criteria andYjjIsNull() {
            addCriterion("yjj is null");
            return (Criteria) this;
        }

        public Criteria andYjjIsNotNull() {
            addCriterion("yjj is not null");
            return (Criteria) this;
        }

        public Criteria andYjjEqualTo(BigDecimal value) {
            addCriterion("yjj =", value, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjNotEqualTo(BigDecimal value) {
            addCriterion("yjj <>", value, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjGreaterThan(BigDecimal value) {
            addCriterion("yjj >", value, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("yjj >=", value, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjLessThan(BigDecimal value) {
            addCriterion("yjj <", value, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjLessThanOrEqualTo(BigDecimal value) {
            addCriterion("yjj <=", value, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjIn(List<BigDecimal> values) {
            addCriterion("yjj in", values, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjNotIn(List<BigDecimal> values) {
            addCriterion("yjj not in", values, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yjj between", value1, value2, "yjj");
            return (Criteria) this;
        }

        public Criteria andYjjNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("yjj not between", value1, value2, "yjj");
            return (Criteria) this;
        }

        public Criteria andCxjjIsNull() {
            addCriterion("cxjj is null");
            return (Criteria) this;
        }

        public Criteria andCxjjIsNotNull() {
            addCriterion("cxjj is not null");
            return (Criteria) this;
        }

        public Criteria andCxjjEqualTo(BigDecimal value) {
            addCriterion("cxjj =", value, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjNotEqualTo(BigDecimal value) {
            addCriterion("cxjj <>", value, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjGreaterThan(BigDecimal value) {
            addCriterion("cxjj >", value, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cxjj >=", value, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjLessThan(BigDecimal value) {
            addCriterion("cxjj <", value, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cxjj <=", value, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjIn(List<BigDecimal> values) {
            addCriterion("cxjj in", values, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjNotIn(List<BigDecimal> values) {
            addCriterion("cxjj not in", values, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cxjj between", value1, value2, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxjjNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cxjj not between", value1, value2, "cxjj");
            return (Criteria) this;
        }

        public Criteria andCxsjIsNull() {
            addCriterion("cxsj is null");
            return (Criteria) this;
        }

        public Criteria andCxsjIsNotNull() {
            addCriterion("cxsj is not null");
            return (Criteria) this;
        }

        public Criteria andCxsjEqualTo(BigDecimal value) {
            addCriterion("cxsj =", value, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjNotEqualTo(BigDecimal value) {
            addCriterion("cxsj <>", value, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjGreaterThan(BigDecimal value) {
            addCriterion("cxsj >", value, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("cxsj >=", value, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjLessThan(BigDecimal value) {
            addCriterion("cxsj <", value, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjLessThanOrEqualTo(BigDecimal value) {
            addCriterion("cxsj <=", value, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjIn(List<BigDecimal> values) {
            addCriterion("cxsj in", values, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjNotIn(List<BigDecimal> values) {
            addCriterion("cxsj not in", values, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cxsj between", value1, value2, "cxsj");
            return (Criteria) this;
        }

        public Criteria andCxsjNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("cxsj not between", value1, value2, "cxsj");
            return (Criteria) this;
        }

        public Criteria andJyphlIsNull() {
            addCriterion("jyphl is null");
            return (Criteria) this;
        }

        public Criteria andJyphlIsNotNull() {
            addCriterion("jyphl is not null");
            return (Criteria) this;
        }

        public Criteria andJyphlEqualTo(Integer value) {
            addCriterion("jyphl =", value, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlNotEqualTo(Integer value) {
            addCriterion("jyphl <>", value, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlGreaterThan(Integer value) {
            addCriterion("jyphl >", value, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlGreaterThanOrEqualTo(Integer value) {
            addCriterion("jyphl >=", value, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlLessThan(Integer value) {
            addCriterion("jyphl <", value, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlLessThanOrEqualTo(Integer value) {
            addCriterion("jyphl <=", value, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlIn(List<Integer> values) {
            addCriterion("jyphl in", values, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlNotIn(List<Integer> values) {
            addCriterion("jyphl not in", values, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlBetween(Integer value1, Integer value2) {
            addCriterion("jyphl between", value1, value2, "jyphl");
            return (Criteria) this;
        }

        public Criteria andJyphlNotBetween(Integer value1, Integer value2) {
            addCriterion("jyphl not between", value1, value2, "jyphl");
            return (Criteria) this;
        }

        public Criteria andMdyhlIsNull() {
            addCriterion("mdyhl is null");
            return (Criteria) this;
        }

        public Criteria andMdyhlIsNotNull() {
            addCriterion("mdyhl is not null");
            return (Criteria) this;
        }

        public Criteria andMdyhlEqualTo(Integer value) {
            addCriterion("mdyhl =", value, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlNotEqualTo(Integer value) {
            addCriterion("mdyhl <>", value, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlGreaterThan(Integer value) {
            addCriterion("mdyhl >", value, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlGreaterThanOrEqualTo(Integer value) {
            addCriterion("mdyhl >=", value, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlLessThan(Integer value) {
            addCriterion("mdyhl <", value, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlLessThanOrEqualTo(Integer value) {
            addCriterion("mdyhl <=", value, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlIn(List<Integer> values) {
            addCriterion("mdyhl in", values, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlNotIn(List<Integer> values) {
            addCriterion("mdyhl not in", values, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlBetween(Integer value1, Integer value2) {
            addCriterion("mdyhl between", value1, value2, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andMdyhlNotBetween(Integer value1, Integer value2) {
            addCriterion("mdyhl not between", value1, value2, "mdyhl");
            return (Criteria) this;
        }

        public Criteria andZxqdlIsNull() {
            addCriterion("zxqdl is null");
            return (Criteria) this;
        }

        public Criteria andZxqdlIsNotNull() {
            addCriterion("zxqdl is not null");
            return (Criteria) this;
        }

        public Criteria andZxqdlEqualTo(Integer value) {
            addCriterion("zxqdl =", value, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlNotEqualTo(Integer value) {
            addCriterion("zxqdl <>", value, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlGreaterThan(Integer value) {
            addCriterion("zxqdl >", value, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlGreaterThanOrEqualTo(Integer value) {
            addCriterion("zxqdl >=", value, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlLessThan(Integer value) {
            addCriterion("zxqdl <", value, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlLessThanOrEqualTo(Integer value) {
            addCriterion("zxqdl <=", value, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlIn(List<Integer> values) {
            addCriterion("zxqdl in", values, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlNotIn(List<Integer> values) {
            addCriterion("zxqdl not in", values, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlBetween(Integer value1, Integer value2) {
            addCriterion("zxqdl between", value1, value2, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andZxqdlNotBetween(Integer value1, Integer value2) {
            addCriterion("zxqdl not between", value1, value2, "zxqdl");
            return (Criteria) this;
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}