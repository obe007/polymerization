package com.mybatisdynamic.pojo.entity.sqlserver;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * VegetableMain
 * @author 
 */
public class VegetableMain implements Serializable {
    private String cCode;

    private String cName;

    private String cataName;

    private String sellUnit;

    private String xScale;

    private Integer orderSlMin;

    private String cgz;

    private BigDecimal j;

    private BigDecimal c;

    private BigDecimal f;

    private BigDecimal scale;

    private BigDecimal importPrice;

    private BigDecimal sellPrice;

    private static final long serialVersionUID = 1L;

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getCataName() {
        return cataName;
    }

    public void setCataName(String cataName) {
        this.cataName = cataName;
    }

    public String getSellUnit() {
        return sellUnit;
    }

    public void setSellUnit(String sellUnit) {
        this.sellUnit = sellUnit;
    }

    public String getxScale() {
        return xScale;
    }

    public void setxScale(String xScale) {
        this.xScale = xScale;
    }

    public Integer getOrderSlMin() {
        return orderSlMin;
    }

    public void setOrderSlMin(Integer orderSlMin) {
        this.orderSlMin = orderSlMin;
    }

    public String getCgz() {
        return cgz;
    }

    public void setCgz(String cgz) {
        this.cgz = cgz;
    }

    public BigDecimal getJ() {
        return j;
    }

    public void setJ(BigDecimal j) {
        this.j = j;
    }

    public BigDecimal getC() {
        return c;
    }

    public void setC(BigDecimal c) {
        this.c = c;
    }

    public BigDecimal getF() {
        return f;
    }

    public void setF(BigDecimal f) {
        this.f = f;
    }

    public BigDecimal getScale() {
        return scale;
    }

    public void setScale(BigDecimal scale) {
        this.scale = scale;
    }

    public BigDecimal getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(BigDecimal importPrice) {
        this.importPrice = importPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }
}