package com.mybatisdynamic.pojo.entity.sqlserver;

import java.io.Serializable;
import java.util.Date;

/**
 * store_commodity
 * @author 
 */
public class StoreCommodity implements Serializable {
    private String store;

    private String storeName;

    private Integer distNo;

    private Integer distNo2;

    private Integer distNo3;

    private String ordCycle;

    private String isauto;

    private String oldNo;

    private String ip;

    private Date chgTime;

    private static final long serialVersionUID = 1L;

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Integer getDistNo() {
        return distNo;
    }

    public void setDistNo(Integer distNo) {
        this.distNo = distNo;
    }

    public Integer getDistNo2() {
        return distNo2;
    }

    public void setDistNo2(Integer distNo2) {
        this.distNo2 = distNo2;
    }

    public Integer getDistNo3() {
        return distNo3;
    }

    public void setDistNo3(Integer distNo3) {
        this.distNo3 = distNo3;
    }

    public String getOrdCycle() {
        return ordCycle;
    }

    public void setOrdCycle(String ordCycle) {
        this.ordCycle = ordCycle;
    }

    public String getIsauto() {
        return isauto;
    }

    public void setIsauto(String isauto) {
        this.isauto = isauto;
    }

    public String getOldNo() {
        return oldNo;
    }

    public void setOldNo(String oldNo) {
        this.oldNo = oldNo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getChgTime() {
        return chgTime;
    }

    public void setChgTime(Date chgTime) {
        this.chgTime = chgTime;
    }
}