package com.mybatisdynamic.pojo.entity.sqlserver;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class StoreCommodityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public StoreCommodityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andStoreIsNull() {
            addCriterion("store is null");
            return (Criteria) this;
        }

        public Criteria andStoreIsNotNull() {
            addCriterion("store is not null");
            return (Criteria) this;
        }

        public Criteria andStoreEqualTo(String value) {
            addCriterion("store =", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotEqualTo(String value) {
            addCriterion("store <>", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreGreaterThan(String value) {
            addCriterion("store >", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreGreaterThanOrEqualTo(String value) {
            addCriterion("store >=", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreLessThan(String value) {
            addCriterion("store <", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreLessThanOrEqualTo(String value) {
            addCriterion("store <=", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreLike(String value) {
            addCriterion("store like", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotLike(String value) {
            addCriterion("store not like", value, "store");
            return (Criteria) this;
        }

        public Criteria andStoreIn(List<String> values) {
            addCriterion("store in", values, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotIn(List<String> values) {
            addCriterion("store not in", values, "store");
            return (Criteria) this;
        }

        public Criteria andStoreBetween(String value1, String value2) {
            addCriterion("store between", value1, value2, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNotBetween(String value1, String value2) {
            addCriterion("store not between", value1, value2, "store");
            return (Criteria) this;
        }

        public Criteria andStoreNameIsNull() {
            addCriterion("store_name is null");
            return (Criteria) this;
        }

        public Criteria andStoreNameIsNotNull() {
            addCriterion("store_name is not null");
            return (Criteria) this;
        }

        public Criteria andStoreNameEqualTo(String value) {
            addCriterion("store_name =", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotEqualTo(String value) {
            addCriterion("store_name <>", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameGreaterThan(String value) {
            addCriterion("store_name >", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameGreaterThanOrEqualTo(String value) {
            addCriterion("store_name >=", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameLessThan(String value) {
            addCriterion("store_name <", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameLessThanOrEqualTo(String value) {
            addCriterion("store_name <=", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameLike(String value) {
            addCriterion("store_name like", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotLike(String value) {
            addCriterion("store_name not like", value, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameIn(List<String> values) {
            addCriterion("store_name in", values, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotIn(List<String> values) {
            addCriterion("store_name not in", values, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameBetween(String value1, String value2) {
            addCriterion("store_name between", value1, value2, "storeName");
            return (Criteria) this;
        }

        public Criteria andStoreNameNotBetween(String value1, String value2) {
            addCriterion("store_name not between", value1, value2, "storeName");
            return (Criteria) this;
        }

        public Criteria andDistNoIsNull() {
            addCriterion("dist_no is null");
            return (Criteria) this;
        }

        public Criteria andDistNoIsNotNull() {
            addCriterion("dist_no is not null");
            return (Criteria) this;
        }

        public Criteria andDistNoEqualTo(Integer value) {
            addCriterion("dist_no =", value, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoNotEqualTo(Integer value) {
            addCriterion("dist_no <>", value, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoGreaterThan(Integer value) {
            addCriterion("dist_no >", value, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("dist_no >=", value, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoLessThan(Integer value) {
            addCriterion("dist_no <", value, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoLessThanOrEqualTo(Integer value) {
            addCriterion("dist_no <=", value, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoIn(List<Integer> values) {
            addCriterion("dist_no in", values, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoNotIn(List<Integer> values) {
            addCriterion("dist_no not in", values, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoBetween(Integer value1, Integer value2) {
            addCriterion("dist_no between", value1, value2, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNoNotBetween(Integer value1, Integer value2) {
            addCriterion("dist_no not between", value1, value2, "distNo");
            return (Criteria) this;
        }

        public Criteria andDistNo2IsNull() {
            addCriterion("dist_no2 is null");
            return (Criteria) this;
        }

        public Criteria andDistNo2IsNotNull() {
            addCriterion("dist_no2 is not null");
            return (Criteria) this;
        }

        public Criteria andDistNo2EqualTo(Integer value) {
            addCriterion("dist_no2 =", value, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2NotEqualTo(Integer value) {
            addCriterion("dist_no2 <>", value, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2GreaterThan(Integer value) {
            addCriterion("dist_no2 >", value, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2GreaterThanOrEqualTo(Integer value) {
            addCriterion("dist_no2 >=", value, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2LessThan(Integer value) {
            addCriterion("dist_no2 <", value, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2LessThanOrEqualTo(Integer value) {
            addCriterion("dist_no2 <=", value, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2In(List<Integer> values) {
            addCriterion("dist_no2 in", values, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2NotIn(List<Integer> values) {
            addCriterion("dist_no2 not in", values, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2Between(Integer value1, Integer value2) {
            addCriterion("dist_no2 between", value1, value2, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo2NotBetween(Integer value1, Integer value2) {
            addCriterion("dist_no2 not between", value1, value2, "distNo2");
            return (Criteria) this;
        }

        public Criteria andDistNo3IsNull() {
            addCriterion("dist_no3 is null");
            return (Criteria) this;
        }

        public Criteria andDistNo3IsNotNull() {
            addCriterion("dist_no3 is not null");
            return (Criteria) this;
        }

        public Criteria andDistNo3EqualTo(Integer value) {
            addCriterion("dist_no3 =", value, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3NotEqualTo(Integer value) {
            addCriterion("dist_no3 <>", value, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3GreaterThan(Integer value) {
            addCriterion("dist_no3 >", value, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3GreaterThanOrEqualTo(Integer value) {
            addCriterion("dist_no3 >=", value, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3LessThan(Integer value) {
            addCriterion("dist_no3 <", value, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3LessThanOrEqualTo(Integer value) {
            addCriterion("dist_no3 <=", value, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3In(List<Integer> values) {
            addCriterion("dist_no3 in", values, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3NotIn(List<Integer> values) {
            addCriterion("dist_no3 not in", values, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3Between(Integer value1, Integer value2) {
            addCriterion("dist_no3 between", value1, value2, "distNo3");
            return (Criteria) this;
        }

        public Criteria andDistNo3NotBetween(Integer value1, Integer value2) {
            addCriterion("dist_no3 not between", value1, value2, "distNo3");
            return (Criteria) this;
        }

        public Criteria andOrdCycleIsNull() {
            addCriterion("ord_cycle is null");
            return (Criteria) this;
        }

        public Criteria andOrdCycleIsNotNull() {
            addCriterion("ord_cycle is not null");
            return (Criteria) this;
        }

        public Criteria andOrdCycleEqualTo(String value) {
            addCriterion("ord_cycle =", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleNotEqualTo(String value) {
            addCriterion("ord_cycle <>", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleGreaterThan(String value) {
            addCriterion("ord_cycle >", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleGreaterThanOrEqualTo(String value) {
            addCriterion("ord_cycle >=", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleLessThan(String value) {
            addCriterion("ord_cycle <", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleLessThanOrEqualTo(String value) {
            addCriterion("ord_cycle <=", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleLike(String value) {
            addCriterion("ord_cycle like", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleNotLike(String value) {
            addCriterion("ord_cycle not like", value, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleIn(List<String> values) {
            addCriterion("ord_cycle in", values, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleNotIn(List<String> values) {
            addCriterion("ord_cycle not in", values, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleBetween(String value1, String value2) {
            addCriterion("ord_cycle between", value1, value2, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andOrdCycleNotBetween(String value1, String value2) {
            addCriterion("ord_cycle not between", value1, value2, "ordCycle");
            return (Criteria) this;
        }

        public Criteria andIsautoIsNull() {
            addCriterion("isauto is null");
            return (Criteria) this;
        }

        public Criteria andIsautoIsNotNull() {
            addCriterion("isauto is not null");
            return (Criteria) this;
        }

        public Criteria andIsautoEqualTo(String value) {
            addCriterion("isauto =", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoNotEqualTo(String value) {
            addCriterion("isauto <>", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoGreaterThan(String value) {
            addCriterion("isauto >", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoGreaterThanOrEqualTo(String value) {
            addCriterion("isauto >=", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoLessThan(String value) {
            addCriterion("isauto <", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoLessThanOrEqualTo(String value) {
            addCriterion("isauto <=", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoLike(String value) {
            addCriterion("isauto like", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoNotLike(String value) {
            addCriterion("isauto not like", value, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoIn(List<String> values) {
            addCriterion("isauto in", values, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoNotIn(List<String> values) {
            addCriterion("isauto not in", values, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoBetween(String value1, String value2) {
            addCriterion("isauto between", value1, value2, "isauto");
            return (Criteria) this;
        }

        public Criteria andIsautoNotBetween(String value1, String value2) {
            addCriterion("isauto not between", value1, value2, "isauto");
            return (Criteria) this;
        }

        public Criteria andOldNoIsNull() {
            addCriterion("old_no is null");
            return (Criteria) this;
        }

        public Criteria andOldNoIsNotNull() {
            addCriterion("old_no is not null");
            return (Criteria) this;
        }

        public Criteria andOldNoEqualTo(String value) {
            addCriterion("old_no =", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoNotEqualTo(String value) {
            addCriterion("old_no <>", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoGreaterThan(String value) {
            addCriterion("old_no >", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoGreaterThanOrEqualTo(String value) {
            addCriterion("old_no >=", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoLessThan(String value) {
            addCriterion("old_no <", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoLessThanOrEqualTo(String value) {
            addCriterion("old_no <=", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoLike(String value) {
            addCriterion("old_no like", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoNotLike(String value) {
            addCriterion("old_no not like", value, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoIn(List<String> values) {
            addCriterion("old_no in", values, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoNotIn(List<String> values) {
            addCriterion("old_no not in", values, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoBetween(String value1, String value2) {
            addCriterion("old_no between", value1, value2, "oldNo");
            return (Criteria) this;
        }

        public Criteria andOldNoNotBetween(String value1, String value2) {
            addCriterion("old_no not between", value1, value2, "oldNo");
            return (Criteria) this;
        }

        public Criteria andIpIsNull() {
            addCriterion("ip is null");
            return (Criteria) this;
        }

        public Criteria andIpIsNotNull() {
            addCriterion("ip is not null");
            return (Criteria) this;
        }

        public Criteria andIpEqualTo(String value) {
            addCriterion("ip =", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotEqualTo(String value) {
            addCriterion("ip <>", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpGreaterThan(String value) {
            addCriterion("ip >", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpGreaterThanOrEqualTo(String value) {
            addCriterion("ip >=", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLessThan(String value) {
            addCriterion("ip <", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLessThanOrEqualTo(String value) {
            addCriterion("ip <=", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLike(String value) {
            addCriterion("ip like", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotLike(String value) {
            addCriterion("ip not like", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpIn(List<String> values) {
            addCriterion("ip in", values, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotIn(List<String> values) {
            addCriterion("ip not in", values, "ip");
            return (Criteria) this;
        }

        public Criteria andIpBetween(String value1, String value2) {
            addCriterion("ip between", value1, value2, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotBetween(String value1, String value2) {
            addCriterion("ip not between", value1, value2, "ip");
            return (Criteria) this;
        }

        public Criteria andChgTimeIsNull() {
            addCriterion("chg_time is null");
            return (Criteria) this;
        }

        public Criteria andChgTimeIsNotNull() {
            addCriterion("chg_time is not null");
            return (Criteria) this;
        }

        public Criteria andChgTimeEqualTo(LocalDateTime value) {
            addCriterion("chg_time =", value, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeNotEqualTo(LocalDateTime value) {
            addCriterion("chg_time <>", value, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeGreaterThan(LocalDateTime value) {
            addCriterion("chg_time >", value, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("chg_time >=", value, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeLessThan(LocalDateTime value) {
            addCriterion("chg_time <", value, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("chg_time <=", value, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeIn(List<LocalDateTime> values) {
            addCriterion("chg_time in", values, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeNotIn(List<LocalDateTime> values) {
            addCriterion("chg_time not in", values, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("chg_time between", value1, value2, "chgTime");
            return (Criteria) this;
        }

        public Criteria andChgTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("chg_time not between", value1, value2, "chgTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}