package com.mybatisdynamic.pojo.entity.sqlserver;

import java.math.BigDecimal;
import java.util.Date;

public class Commodity {

    /** 货号 */
    private String cCode;

    /** 货名 */
    private String cName;

    /** 货名 */
    private String ordCycle;

    /**  */
    private String scale;

    private BigDecimal scaleQty;

    private String isauto;

    /** 量词 */
    private String unit;

    /** IP地址 */
    private String ip;

    private Date chgTime;

    public Commodity() {
    }

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getOrdCycle() {
        return ordCycle;
    }

    public void setOrdCycle(String ordCycle) {
        this.ordCycle = ordCycle;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public BigDecimal getScaleQty() {
        return scaleQty;
    }

    public void setScaleQty(BigDecimal scaleQty) {
        this.scaleQty = scaleQty;
    }

    public String getIsauto() {
        return isauto;
    }

    public void setIsauto(String isauto) {
        this.isauto = isauto;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getChgTime() {
        return chgTime;
    }

    public void setChgTime(Date chgTime) {
        this.chgTime = chgTime;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Commodity commodity = (Commodity) o;
//        return Objects.equals(cCode, commodity.cCode) &&
//                Objects.equals(cName, commodity.cName) &&
//                Objects.equals(ordCycle, commodity.ordCycle) &&
//                Objects.equals(scale, commodity.scale) &&
//                Objects.equals(scaleQty, commodity.scaleQty) &&
//                Objects.equals(isauto, commodity.isauto) &&
//                Objects.equals(unit, commodity.unit) &&
//                Objects.equals(ip, commodity.ip) &&
//                Objects.equals(chgTime, commodity.chgTime);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(cCode, cName, ordCycle, scale, scaleQty, isauto, unit, ip, chgTime);
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Commodity)) return false;

        Commodity commodity = (Commodity) o;

        if (getcCode() != null ? !getcCode().equals(commodity.getcCode()) : commodity.getcCode() != null) return false;
        if (getcName() != null ? !getcName().equals(commodity.getcName()) : commodity.getcName() != null) return false;
        if (getOrdCycle() != null ? !getOrdCycle().equals(commodity.getOrdCycle()) : commodity.getOrdCycle() != null)
            return false;
        if (getScale() != null ? !getScale().equals(commodity.getScale()) : commodity.getScale() != null) return false;
        if (getScaleQty() != null ? !getScaleQty().equals(commodity.getScaleQty()) : commodity.getScaleQty() != null)
            return false;
        if (getIsauto() != null ? !getIsauto().equals(commodity.getIsauto()) : commodity.getIsauto() != null)
            return false;
        if (getUnit() != null ? !getUnit().equals(commodity.getUnit()) : commodity.getUnit() != null) return false;
        if (getIp() != null ? !getIp().equals(commodity.getIp()) : commodity.getIp() != null) return false;
        return getChgTime() != null ? getChgTime().equals(commodity.getChgTime()) : commodity.getChgTime() == null;
    }

    @Override
    public int hashCode() {
        int result = getcCode() != null ? getcCode().hashCode() : 0;
        result = 31 * result + (getcName() != null ? getcName().hashCode() : 0);
        result = 31 * result + (getOrdCycle() != null ? getOrdCycle().hashCode() : 0);
        result = 31 * result + (getScale() != null ? getScale().hashCode() : 0);
        result = 31 * result + (getScaleQty() != null ? getScaleQty().hashCode() : 0);
        result = 31 * result + (getIsauto() != null ? getIsauto().hashCode() : 0);
        result = 31 * result + (getUnit() != null ? getUnit().hashCode() : 0);
        result = 31 * result + (getIp() != null ? getIp().hashCode() : 0);
        result = 31 * result + (getChgTime() != null ? getChgTime().hashCode() : 0);
        return result;
    }
}
