package com.mybatisdynamic.pojo.entity.mysql;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 蔬果->门店要货数量->是否开通此商品关系表(蔬果六位码和门店号对应,要货数量,无记录=未开通)
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("veg_fruit_replenishment")
@ApiModel(value="VegFruitReplenishment对象", description="蔬果->门店要货数量->是否开通此商品关系表(蔬果六位码和门店号对应,要货数量,无记录=未开通)")
public class VegFruitReplenishment extends Model<VegFruitReplenishment> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID;序号")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "门店号")
    private String storeNo;

    @ApiModelProperty(value = "六位码(商品编码)")
    private String sixBitCode;

    @ApiModelProperty(value = "要货数量(补货量)")
    private BigDecimal replenishment;

    @ApiModelProperty(value = "删除状态：0->未删除；1->已删除")
    private Integer deleteStatus;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
