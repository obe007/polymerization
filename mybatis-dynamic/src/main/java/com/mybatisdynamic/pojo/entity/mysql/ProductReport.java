package com.mybatisdynamic.pojo.entity.mysql;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * product_report
 * @author 
 */
public class ProductReport implements Serializable {
    /**
     * 主键ID;序号
     */
    private Long id;

    /**
     * 档期
     */
    private String filingDate;

    /**
     * 门店No
     */
    private String storeNo;

    /**
     * 六位码
     */
    private String sixBitCode;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 展示图片
     */
    private String pic;

    /**
     * 规格(商品配送规格)
     */
    private String scale;

    /**
     * 即时库存
     */
    private BigDecimal stock;

    /**
     * 最低库存
     */
    private BigDecimal minStock;

    /**
     * 最高库存
     */
    private BigDecimal maxStock;

    /**
     * 上月销量
     */
    private BigDecimal lastMonthSale;

    /**
     * 促销方式(促销类型)
     */
    private String promotionType;

    /**
     * 配送方式
     */
    private String deliveryType;

    /**
     * 结算方式
     */
    private String settlementType;

    /**
     * 是否拆零(是否零售)
     */
    private String retailType;

    /**
     * 原进价(原进货价)
     */
    private BigDecimal originalPurchasePrice;

    /**
     * 促销进货价
     */
    private BigDecimal promotionPurchasePrice;

    /**
     * 促销销售价
     */
    private BigDecimal promotionSalePrice;

    /**
     * 最小起订量(最小补货量)
     */
    private BigDecimal minReplenishment;

    /**
     * 推荐补货量
     */
    private BigDecimal suggestedReplenishment;

    /**
     * 门店要货(实际补货量)
     */
    private BigDecimal realReplenishment;

    /**
     * 删除状态：0->未删除；1->已删除
     */
    private Integer deleteStatus;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(String filingDate) {
        this.filingDate = filingDate;
    }

    public String getStoreNo() {
        return storeNo;
    }

    public void setStoreNo(String storeNo) {
        this.storeNo = storeNo;
    }

    public String getSixBitCode() {
        return sixBitCode;
    }

    public void setSixBitCode(String sixBitCode) {
        this.sixBitCode = sixBitCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    public BigDecimal getMinStock() {
        return minStock;
    }

    public void setMinStock(BigDecimal minStock) {
        this.minStock = minStock;
    }

    public BigDecimal getMaxStock() {
        return maxStock;
    }

    public void setMaxStock(BigDecimal maxStock) {
        this.maxStock = maxStock;
    }

    public BigDecimal getLastMonthSale() {
        return lastMonthSale;
    }

    public void setLastMonthSale(BigDecimal lastMonthSale) {
        this.lastMonthSale = lastMonthSale;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getRetailType() {
        return retailType;
    }

    public void setRetailType(String retailType) {
        this.retailType = retailType;
    }

    public BigDecimal getOriginalPurchasePrice() {
        return originalPurchasePrice;
    }

    public void setOriginalPurchasePrice(BigDecimal originalPurchasePrice) {
        this.originalPurchasePrice = originalPurchasePrice;
    }

    public BigDecimal getPromotionPurchasePrice() {
        return promotionPurchasePrice;
    }

    public void setPromotionPurchasePrice(BigDecimal promotionPurchasePrice) {
        this.promotionPurchasePrice = promotionPurchasePrice;
    }

    public BigDecimal getPromotionSalePrice() {
        return promotionSalePrice;
    }

    public void setPromotionSalePrice(BigDecimal promotionSalePrice) {
        this.promotionSalePrice = promotionSalePrice;
    }

    public BigDecimal getMinReplenishment() {
        return minReplenishment;
    }

    public void setMinReplenishment(BigDecimal minReplenishment) {
        this.minReplenishment = minReplenishment;
    }

    public BigDecimal getSuggestedReplenishment() {
        return suggestedReplenishment;
    }

    public void setSuggestedReplenishment(BigDecimal suggestedReplenishment) {
        this.suggestedReplenishment = suggestedReplenishment;
    }

    public BigDecimal getRealReplenishment() {
        return realReplenishment;
    }

    public void setRealReplenishment(BigDecimal realReplenishment) {
        this.realReplenishment = realReplenishment;
    }

    public Integer getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Integer deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}