package com.mybatisdynamic.pojo.entity.sqlserver;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class VegetableMainExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VegetableMainExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCCodeIsNull() {
            addCriterion("c_code is null");
            return (Criteria) this;
        }

        public Criteria andCCodeIsNotNull() {
            addCriterion("c_code is not null");
            return (Criteria) this;
        }

        public Criteria andCCodeEqualTo(String value) {
            addCriterion("c_code =", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotEqualTo(String value) {
            addCriterion("c_code <>", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeGreaterThan(String value) {
            addCriterion("c_code >", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeGreaterThanOrEqualTo(String value) {
            addCriterion("c_code >=", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeLessThan(String value) {
            addCriterion("c_code <", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeLessThanOrEqualTo(String value) {
            addCriterion("c_code <=", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeLike(String value) {
            addCriterion("c_code like", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotLike(String value) {
            addCriterion("c_code not like", value, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeIn(List<String> values) {
            addCriterion("c_code in", values, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotIn(List<String> values) {
            addCriterion("c_code not in", values, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeBetween(String value1, String value2) {
            addCriterion("c_code between", value1, value2, "cCode");
            return (Criteria) this;
        }

        public Criteria andCCodeNotBetween(String value1, String value2) {
            addCriterion("c_code not between", value1, value2, "cCode");
            return (Criteria) this;
        }

        public Criteria andCNameIsNull() {
            addCriterion("c_name is null");
            return (Criteria) this;
        }

        public Criteria andCNameIsNotNull() {
            addCriterion("c_name is not null");
            return (Criteria) this;
        }

        public Criteria andCNameEqualTo(String value) {
            addCriterion("c_name =", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotEqualTo(String value) {
            addCriterion("c_name <>", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameGreaterThan(String value) {
            addCriterion("c_name >", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameGreaterThanOrEqualTo(String value) {
            addCriterion("c_name >=", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameLessThan(String value) {
            addCriterion("c_name <", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameLessThanOrEqualTo(String value) {
            addCriterion("c_name <=", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameLike(String value) {
            addCriterion("c_name like", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotLike(String value) {
            addCriterion("c_name not like", value, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameIn(List<String> values) {
            addCriterion("c_name in", values, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotIn(List<String> values) {
            addCriterion("c_name not in", values, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameBetween(String value1, String value2) {
            addCriterion("c_name between", value1, value2, "cName");
            return (Criteria) this;
        }

        public Criteria andCNameNotBetween(String value1, String value2) {
            addCriterion("c_name not between", value1, value2, "cName");
            return (Criteria) this;
        }

        public Criteria andCataNameIsNull() {
            addCriterion("cata_name is null");
            return (Criteria) this;
        }

        public Criteria andCataNameIsNotNull() {
            addCriterion("cata_name is not null");
            return (Criteria) this;
        }

        public Criteria andCataNameEqualTo(String value) {
            addCriterion("cata_name =", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameNotEqualTo(String value) {
            addCriterion("cata_name <>", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameGreaterThan(String value) {
            addCriterion("cata_name >", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameGreaterThanOrEqualTo(String value) {
            addCriterion("cata_name >=", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameLessThan(String value) {
            addCriterion("cata_name <", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameLessThanOrEqualTo(String value) {
            addCriterion("cata_name <=", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameLike(String value) {
            addCriterion("cata_name like", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameNotLike(String value) {
            addCriterion("cata_name not like", value, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameIn(List<String> values) {
            addCriterion("cata_name in", values, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameNotIn(List<String> values) {
            addCriterion("cata_name not in", values, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameBetween(String value1, String value2) {
            addCriterion("cata_name between", value1, value2, "cataName");
            return (Criteria) this;
        }

        public Criteria andCataNameNotBetween(String value1, String value2) {
            addCriterion("cata_name not between", value1, value2, "cataName");
            return (Criteria) this;
        }

        public Criteria andSellUnitIsNull() {
            addCriterion("sell_unit is null");
            return (Criteria) this;
        }

        public Criteria andSellUnitIsNotNull() {
            addCriterion("sell_unit is not null");
            return (Criteria) this;
        }

        public Criteria andSellUnitEqualTo(String value) {
            addCriterion("sell_unit =", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitNotEqualTo(String value) {
            addCriterion("sell_unit <>", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitGreaterThan(String value) {
            addCriterion("sell_unit >", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitGreaterThanOrEqualTo(String value) {
            addCriterion("sell_unit >=", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitLessThan(String value) {
            addCriterion("sell_unit <", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitLessThanOrEqualTo(String value) {
            addCriterion("sell_unit <=", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitLike(String value) {
            addCriterion("sell_unit like", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitNotLike(String value) {
            addCriterion("sell_unit not like", value, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitIn(List<String> values) {
            addCriterion("sell_unit in", values, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitNotIn(List<String> values) {
            addCriterion("sell_unit not in", values, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitBetween(String value1, String value2) {
            addCriterion("sell_unit between", value1, value2, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andSellUnitNotBetween(String value1, String value2) {
            addCriterion("sell_unit not between", value1, value2, "sellUnit");
            return (Criteria) this;
        }

        public Criteria andXScaleIsNull() {
            addCriterion("x_scale is null");
            return (Criteria) this;
        }

        public Criteria andXScaleIsNotNull() {
            addCriterion("x_scale is not null");
            return (Criteria) this;
        }

        public Criteria andXScaleEqualTo(String value) {
            addCriterion("x_scale =", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleNotEqualTo(String value) {
            addCriterion("x_scale <>", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleGreaterThan(String value) {
            addCriterion("x_scale >", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleGreaterThanOrEqualTo(String value) {
            addCriterion("x_scale >=", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleLessThan(String value) {
            addCriterion("x_scale <", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleLessThanOrEqualTo(String value) {
            addCriterion("x_scale <=", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleLike(String value) {
            addCriterion("x_scale like", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleNotLike(String value) {
            addCriterion("x_scale not like", value, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleIn(List<String> values) {
            addCriterion("x_scale in", values, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleNotIn(List<String> values) {
            addCriterion("x_scale not in", values, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleBetween(String value1, String value2) {
            addCriterion("x_scale between", value1, value2, "xScale");
            return (Criteria) this;
        }

        public Criteria andXScaleNotBetween(String value1, String value2) {
            addCriterion("x_scale not between", value1, value2, "xScale");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinIsNull() {
            addCriterion("order_sl_min is null");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinIsNotNull() {
            addCriterion("order_sl_min is not null");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinEqualTo(Integer value) {
            addCriterion("order_sl_min =", value, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinNotEqualTo(Integer value) {
            addCriterion("order_sl_min <>", value, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinGreaterThan(Integer value) {
            addCriterion("order_sl_min >", value, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_sl_min >=", value, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinLessThan(Integer value) {
            addCriterion("order_sl_min <", value, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinLessThanOrEqualTo(Integer value) {
            addCriterion("order_sl_min <=", value, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinIn(List<Integer> values) {
            addCriterion("order_sl_min in", values, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinNotIn(List<Integer> values) {
            addCriterion("order_sl_min not in", values, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinBetween(Integer value1, Integer value2) {
            addCriterion("order_sl_min between", value1, value2, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andOrderSlMinNotBetween(Integer value1, Integer value2) {
            addCriterion("order_sl_min not between", value1, value2, "orderSlMin");
            return (Criteria) this;
        }

        public Criteria andCgzIsNull() {
            addCriterion("cgz is null");
            return (Criteria) this;
        }

        public Criteria andCgzIsNotNull() {
            addCriterion("cgz is not null");
            return (Criteria) this;
        }

        public Criteria andCgzEqualTo(String value) {
            addCriterion("cgz =", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzNotEqualTo(String value) {
            addCriterion("cgz <>", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzGreaterThan(String value) {
            addCriterion("cgz >", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzGreaterThanOrEqualTo(String value) {
            addCriterion("cgz >=", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzLessThan(String value) {
            addCriterion("cgz <", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzLessThanOrEqualTo(String value) {
            addCriterion("cgz <=", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzLike(String value) {
            addCriterion("cgz like", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzNotLike(String value) {
            addCriterion("cgz not like", value, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzIn(List<String> values) {
            addCriterion("cgz in", values, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzNotIn(List<String> values) {
            addCriterion("cgz not in", values, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzBetween(String value1, String value2) {
            addCriterion("cgz between", value1, value2, "cgz");
            return (Criteria) this;
        }

        public Criteria andCgzNotBetween(String value1, String value2) {
            addCriterion("cgz not between", value1, value2, "cgz");
            return (Criteria) this;
        }

        public Criteria andJIsNull() {
            addCriterion("j is null");
            return (Criteria) this;
        }

        public Criteria andJIsNotNull() {
            addCriterion("j is not null");
            return (Criteria) this;
        }

        public Criteria andJEqualTo(BigDecimal value) {
            addCriterion("j =", value, "j");
            return (Criteria) this;
        }

        public Criteria andJNotEqualTo(BigDecimal value) {
            addCriterion("j <>", value, "j");
            return (Criteria) this;
        }

        public Criteria andJGreaterThan(BigDecimal value) {
            addCriterion("j >", value, "j");
            return (Criteria) this;
        }

        public Criteria andJGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("j >=", value, "j");
            return (Criteria) this;
        }

        public Criteria andJLessThan(BigDecimal value) {
            addCriterion("j <", value, "j");
            return (Criteria) this;
        }

        public Criteria andJLessThanOrEqualTo(BigDecimal value) {
            addCriterion("j <=", value, "j");
            return (Criteria) this;
        }

        public Criteria andJIn(List<BigDecimal> values) {
            addCriterion("j in", values, "j");
            return (Criteria) this;
        }

        public Criteria andJNotIn(List<BigDecimal> values) {
            addCriterion("j not in", values, "j");
            return (Criteria) this;
        }

        public Criteria andJBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("j between", value1, value2, "j");
            return (Criteria) this;
        }

        public Criteria andJNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("j not between", value1, value2, "j");
            return (Criteria) this;
        }

        public Criteria andCIsNull() {
            addCriterion("\"c\" is null");
            return (Criteria) this;
        }

        public Criteria andCIsNotNull() {
            addCriterion("\"c\" is not null");
            return (Criteria) this;
        }

        public Criteria andCEqualTo(BigDecimal value) {
            addCriterion("\"c\" =", value, "c");
            return (Criteria) this;
        }

        public Criteria andCNotEqualTo(BigDecimal value) {
            addCriterion("\"c\" <>", value, "c");
            return (Criteria) this;
        }

        public Criteria andCGreaterThan(BigDecimal value) {
            addCriterion("\"c\" >", value, "c");
            return (Criteria) this;
        }

        public Criteria andCGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("\"c\" >=", value, "c");
            return (Criteria) this;
        }

        public Criteria andCLessThan(BigDecimal value) {
            addCriterion("\"c\" <", value, "c");
            return (Criteria) this;
        }

        public Criteria andCLessThanOrEqualTo(BigDecimal value) {
            addCriterion("\"c\" <=", value, "c");
            return (Criteria) this;
        }

        public Criteria andCIn(List<BigDecimal> values) {
            addCriterion("\"c\" in", values, "c");
            return (Criteria) this;
        }

        public Criteria andCNotIn(List<BigDecimal> values) {
            addCriterion("\"c\" not in", values, "c");
            return (Criteria) this;
        }

        public Criteria andCBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("\"c\" between", value1, value2, "c");
            return (Criteria) this;
        }

        public Criteria andCNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("\"c\" not between", value1, value2, "c");
            return (Criteria) this;
        }

        public Criteria andFIsNull() {
            addCriterion("f is null");
            return (Criteria) this;
        }

        public Criteria andFIsNotNull() {
            addCriterion("f is not null");
            return (Criteria) this;
        }

        public Criteria andFEqualTo(BigDecimal value) {
            addCriterion("f =", value, "f");
            return (Criteria) this;
        }

        public Criteria andFNotEqualTo(BigDecimal value) {
            addCriterion("f <>", value, "f");
            return (Criteria) this;
        }

        public Criteria andFGreaterThan(BigDecimal value) {
            addCriterion("f >", value, "f");
            return (Criteria) this;
        }

        public Criteria andFGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("f >=", value, "f");
            return (Criteria) this;
        }

        public Criteria andFLessThan(BigDecimal value) {
            addCriterion("f <", value, "f");
            return (Criteria) this;
        }

        public Criteria andFLessThanOrEqualTo(BigDecimal value) {
            addCriterion("f <=", value, "f");
            return (Criteria) this;
        }

        public Criteria andFIn(List<BigDecimal> values) {
            addCriterion("f in", values, "f");
            return (Criteria) this;
        }

        public Criteria andFNotIn(List<BigDecimal> values) {
            addCriterion("f not in", values, "f");
            return (Criteria) this;
        }

        public Criteria andFBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("f between", value1, value2, "f");
            return (Criteria) this;
        }

        public Criteria andFNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("f not between", value1, value2, "f");
            return (Criteria) this;
        }

        public Criteria andScaleIsNull() {
            addCriterion("\"scale\" is null");
            return (Criteria) this;
        }

        public Criteria andScaleIsNotNull() {
            addCriterion("\"scale\" is not null");
            return (Criteria) this;
        }

        public Criteria andScaleEqualTo(BigDecimal value) {
            addCriterion("\"scale\" =", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotEqualTo(BigDecimal value) {
            addCriterion("\"scale\" <>", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleGreaterThan(BigDecimal value) {
            addCriterion("\"scale\" >", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("\"scale\" >=", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleLessThan(BigDecimal value) {
            addCriterion("\"scale\" <", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("\"scale\" <=", value, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleIn(List<BigDecimal> values) {
            addCriterion("\"scale\" in", values, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotIn(List<BigDecimal> values) {
            addCriterion("\"scale\" not in", values, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("\"scale\" between", value1, value2, "scale");
            return (Criteria) this;
        }

        public Criteria andScaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("\"scale\" not between", value1, value2, "scale");
            return (Criteria) this;
        }

        public Criteria andImportPriceIsNull() {
            addCriterion("import_price is null");
            return (Criteria) this;
        }

        public Criteria andImportPriceIsNotNull() {
            addCriterion("import_price is not null");
            return (Criteria) this;
        }

        public Criteria andImportPriceEqualTo(BigDecimal value) {
            addCriterion("import_price =", value, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceNotEqualTo(BigDecimal value) {
            addCriterion("import_price <>", value, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceGreaterThan(BigDecimal value) {
            addCriterion("import_price >", value, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("import_price >=", value, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceLessThan(BigDecimal value) {
            addCriterion("import_price <", value, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("import_price <=", value, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceIn(List<BigDecimal> values) {
            addCriterion("import_price in", values, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceNotIn(List<BigDecimal> values) {
            addCriterion("import_price not in", values, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("import_price between", value1, value2, "importPrice");
            return (Criteria) this;
        }

        public Criteria andImportPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("import_price not between", value1, value2, "importPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceIsNull() {
            addCriterion("sell_price is null");
            return (Criteria) this;
        }

        public Criteria andSellPriceIsNotNull() {
            addCriterion("sell_price is not null");
            return (Criteria) this;
        }

        public Criteria andSellPriceEqualTo(BigDecimal value) {
            addCriterion("sell_price =", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceNotEqualTo(BigDecimal value) {
            addCriterion("sell_price <>", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceGreaterThan(BigDecimal value) {
            addCriterion("sell_price >", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("sell_price >=", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceLessThan(BigDecimal value) {
            addCriterion("sell_price <", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("sell_price <=", value, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceIn(List<BigDecimal> values) {
            addCriterion("sell_price in", values, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceNotIn(List<BigDecimal> values) {
            addCriterion("sell_price not in", values, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sell_price between", value1, value2, "sellPrice");
            return (Criteria) this;
        }

        public Criteria andSellPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sell_price not between", value1, value2, "sellPrice");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}