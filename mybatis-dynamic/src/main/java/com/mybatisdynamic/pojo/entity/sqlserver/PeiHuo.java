package com.mybatisdynamic.pojo.entity.sqlserver;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * PeiHuo
 * @author 
 */
public class PeiHuo implements Serializable {
    private String dq;

    private String shpno;

    private String cCode;

    private String shpname;

    private String guige;

    private String jskc;

    private String syxs;

    private String cxfs;

    private String psfs;

    private String js;

    private String sfcl;

    private BigDecimal yjj;

    private BigDecimal cxjj;

    private BigDecimal cxsj;

    private Integer jyphl;

    private Integer mdyhl;

    private Integer zxqdl;

    private Integer id;

    private static final long serialVersionUID = 1L;

    public String getDq() {
        return dq;
    }

    public void setDq(String dq) {
        this.dq = dq;
    }

    public String getShpno() {
        return shpno;
    }

    public void setShpno(String shpno) {
        this.shpno = shpno;
    }

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public String getShpname() {
        return shpname;
    }

    public void setShpname(String shpname) {
        this.shpname = shpname;
    }

    public String getGuige() {
        return guige;
    }

    public void setGuige(String guige) {
        this.guige = guige;
    }

    public String getJskc() {
        return jskc;
    }

    public void setJskc(String jskc) {
        this.jskc = jskc;
    }

    public String getSyxs() {
        return syxs;
    }

    public void setSyxs(String syxs) {
        this.syxs = syxs;
    }

    public String getCxfs() {
        return cxfs;
    }

    public void setCxfs(String cxfs) {
        this.cxfs = cxfs;
    }

    public String getPsfs() {
        return psfs;
    }

    public void setPsfs(String psfs) {
        this.psfs = psfs;
    }

    public String getJs() {
        return js;
    }

    public void setJs(String js) {
        this.js = js;
    }

    public String getSfcl() {
        return sfcl;
    }

    public void setSfcl(String sfcl) {
        this.sfcl = sfcl;
    }

    public BigDecimal getYjj() {
        return yjj;
    }

    public void setYjj(BigDecimal yjj) {
        this.yjj = yjj;
    }

    public BigDecimal getCxjj() {
        return cxjj;
    }

    public void setCxjj(BigDecimal cxjj) {
        this.cxjj = cxjj;
    }

    public BigDecimal getCxsj() {
        return cxsj;
    }

    public void setCxsj(BigDecimal cxsj) {
        this.cxsj = cxsj;
    }

    public Integer getJyphl() {
        return jyphl;
    }

    public void setJyphl(Integer jyphl) {
        this.jyphl = jyphl;
    }

    public Integer getMdyhl() {
        return mdyhl;
    }

    public void setMdyhl(Integer mdyhl) {
        this.mdyhl = mdyhl;
    }

    public Integer getZxqdl() {
        return zxqdl;
    }

    public void setZxqdl(Integer zxqdl) {
        this.zxqdl = zxqdl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}