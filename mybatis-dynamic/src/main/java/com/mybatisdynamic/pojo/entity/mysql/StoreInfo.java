package com.mybatisdynamic.pojo.entity.mysql;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * store_info
 * @author 
 */
public class StoreInfo implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 门店号
     */
    private String storeNo;

    /**
     * 门店名称
     */
    private String name;

    /**
     * 订货周期(要货周期)
     */
    private String orderCycle;

    /**
     * 自动补货状态: 0->不自动补货; 1->自动补货
     */
    private Integer autoReplenish;

    /**
     * 门店ip地址
     */
    private String ip;

    /**
     * 门店位置编号(郊区)
     */
    private String locationCode;

    /**
     * 门店区域ID
     */
    private Long zoneId;

    /**
     * 旧的编号
     */
    private String oldStoreNo;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStoreNo() {
        return storeNo;
    }

    public void setStoreNo(String storeNo) {
        this.storeNo = storeNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderCycle() {
        return orderCycle;
    }

    public void setOrderCycle(String orderCycle) {
        this.orderCycle = orderCycle;
    }

    public Integer getAutoReplenish() {
        return autoReplenish;
    }

    public void setAutoReplenish(Integer autoReplenish) {
        this.autoReplenish = autoReplenish;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public String getOldStoreNo() {
        return oldStoreNo;
    }

    public void setOldStoreNo(String oldStoreNo) {
        this.oldStoreNo = oldStoreNo;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}