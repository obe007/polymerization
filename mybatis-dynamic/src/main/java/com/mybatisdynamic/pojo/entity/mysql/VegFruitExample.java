package com.mybatisdynamic.pojo.entity.mysql;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class VegFruitExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public VegFruitExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeIsNull() {
            addCriterion("six_bit_code is null");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeIsNotNull() {
            addCriterion("six_bit_code is not null");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeEqualTo(String value) {
            addCriterion("six_bit_code =", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotEqualTo(String value) {
            addCriterion("six_bit_code <>", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeGreaterThan(String value) {
            addCriterion("six_bit_code >", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeGreaterThanOrEqualTo(String value) {
            addCriterion("six_bit_code >=", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeLessThan(String value) {
            addCriterion("six_bit_code <", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeLessThanOrEqualTo(String value) {
            addCriterion("six_bit_code <=", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeLike(String value) {
            addCriterion("six_bit_code like", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotLike(String value) {
            addCriterion("six_bit_code not like", value, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeIn(List<String> values) {
            addCriterion("six_bit_code in", values, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotIn(List<String> values) {
            addCriterion("six_bit_code not in", values, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeBetween(String value1, String value2) {
            addCriterion("six_bit_code between", value1, value2, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andSixBitCodeNotBetween(String value1, String value2) {
            addCriterion("six_bit_code not between", value1, value2, "sixBitCode");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("`name` is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("`name` is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("`name` =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("`name` <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("`name` >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("`name` >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("`name` <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("`name` <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("`name` like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("`name` not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("`name` in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("`name` not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("`name` between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("`name` not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPicIsNull() {
            addCriterion("pic is null");
            return (Criteria) this;
        }

        public Criteria andPicIsNotNull() {
            addCriterion("pic is not null");
            return (Criteria) this;
        }

        public Criteria andPicEqualTo(String value) {
            addCriterion("pic =", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotEqualTo(String value) {
            addCriterion("pic <>", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThan(String value) {
            addCriterion("pic >", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThanOrEqualTo(String value) {
            addCriterion("pic >=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThan(String value) {
            addCriterion("pic <", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThanOrEqualTo(String value) {
            addCriterion("pic <=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLike(String value) {
            addCriterion("pic like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotLike(String value) {
            addCriterion("pic not like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicIn(List<String> values) {
            addCriterion("pic in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotIn(List<String> values) {
            addCriterion("pic not in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicBetween(String value1, String value2) {
            addCriterion("pic between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotBetween(String value1, String value2) {
            addCriterion("pic not between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdIsNull() {
            addCriterion("veg_fruit_category_id is null");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdIsNotNull() {
            addCriterion("veg_fruit_category_id is not null");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdEqualTo(Long value) {
            addCriterion("veg_fruit_category_id =", value, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdNotEqualTo(Long value) {
            addCriterion("veg_fruit_category_id <>", value, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdGreaterThan(Long value) {
            addCriterion("veg_fruit_category_id >", value, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdGreaterThanOrEqualTo(Long value) {
            addCriterion("veg_fruit_category_id >=", value, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdLessThan(Long value) {
            addCriterion("veg_fruit_category_id <", value, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdLessThanOrEqualTo(Long value) {
            addCriterion("veg_fruit_category_id <=", value, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdIn(List<Long> values) {
            addCriterion("veg_fruit_category_id in", values, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdNotIn(List<Long> values) {
            addCriterion("veg_fruit_category_id not in", values, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdBetween(Long value1, Long value2) {
            addCriterion("veg_fruit_category_id between", value1, value2, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andVegFruitCategoryIdNotBetween(Long value1, Long value2) {
            addCriterion("veg_fruit_category_id not between", value1, value2, "vegFruitCategoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryNameIsNull() {
            addCriterion("category_name is null");
            return (Criteria) this;
        }

        public Criteria andCategoryNameIsNotNull() {
            addCriterion("category_name is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryNameEqualTo(String value) {
            addCriterion("category_name =", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameNotEqualTo(String value) {
            addCriterion("category_name <>", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameGreaterThan(String value) {
            addCriterion("category_name >", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameGreaterThanOrEqualTo(String value) {
            addCriterion("category_name >=", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameLessThan(String value) {
            addCriterion("category_name <", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameLessThanOrEqualTo(String value) {
            addCriterion("category_name <=", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameLike(String value) {
            addCriterion("category_name like", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameNotLike(String value) {
            addCriterion("category_name not like", value, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameIn(List<String> values) {
            addCriterion("category_name in", values, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameNotIn(List<String> values) {
            addCriterion("category_name not in", values, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameBetween(String value1, String value2) {
            addCriterion("category_name between", value1, value2, "categoryName");
            return (Criteria) this;
        }

        public Criteria andCategoryNameNotBetween(String value1, String value2) {
            addCriterion("category_name not between", value1, value2, "categoryName");
            return (Criteria) this;
        }

        public Criteria andSaleUnitIsNull() {
            addCriterion("sale_unit is null");
            return (Criteria) this;
        }

        public Criteria andSaleUnitIsNotNull() {
            addCriterion("sale_unit is not null");
            return (Criteria) this;
        }

        public Criteria andSaleUnitEqualTo(String value) {
            addCriterion("sale_unit =", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitNotEqualTo(String value) {
            addCriterion("sale_unit <>", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitGreaterThan(String value) {
            addCriterion("sale_unit >", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitGreaterThanOrEqualTo(String value) {
            addCriterion("sale_unit >=", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitLessThan(String value) {
            addCriterion("sale_unit <", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitLessThanOrEqualTo(String value) {
            addCriterion("sale_unit <=", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitLike(String value) {
            addCriterion("sale_unit like", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitNotLike(String value) {
            addCriterion("sale_unit not like", value, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitIn(List<String> values) {
            addCriterion("sale_unit in", values, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitNotIn(List<String> values) {
            addCriterion("sale_unit not in", values, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitBetween(String value1, String value2) {
            addCriterion("sale_unit between", value1, value2, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andSaleUnitNotBetween(String value1, String value2) {
            addCriterion("sale_unit not between", value1, value2, "saleUnit");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleIsNull() {
            addCriterion("boxful_scale is null");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleIsNotNull() {
            addCriterion("boxful_scale is not null");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleEqualTo(String value) {
            addCriterion("boxful_scale =", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleNotEqualTo(String value) {
            addCriterion("boxful_scale <>", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleGreaterThan(String value) {
            addCriterion("boxful_scale >", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleGreaterThanOrEqualTo(String value) {
            addCriterion("boxful_scale >=", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleLessThan(String value) {
            addCriterion("boxful_scale <", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleLessThanOrEqualTo(String value) {
            addCriterion("boxful_scale <=", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleLike(String value) {
            addCriterion("boxful_scale like", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleNotLike(String value) {
            addCriterion("boxful_scale not like", value, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleIn(List<String> values) {
            addCriterion("boxful_scale in", values, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleNotIn(List<String> values) {
            addCriterion("boxful_scale not in", values, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleBetween(String value1, String value2) {
            addCriterion("boxful_scale between", value1, value2, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andBoxfulScaleNotBetween(String value1, String value2) {
            addCriterion("boxful_scale not between", value1, value2, "boxfulScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleIsNull() {
            addCriterion("order_scale is null");
            return (Criteria) this;
        }

        public Criteria andOrderScaleIsNotNull() {
            addCriterion("order_scale is not null");
            return (Criteria) this;
        }

        public Criteria andOrderScaleEqualTo(BigDecimal value) {
            addCriterion("order_scale =", value, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleNotEqualTo(BigDecimal value) {
            addCriterion("order_scale <>", value, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleGreaterThan(BigDecimal value) {
            addCriterion("order_scale >", value, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("order_scale >=", value, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleLessThan(BigDecimal value) {
            addCriterion("order_scale <", value, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("order_scale <=", value, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleIn(List<BigDecimal> values) {
            addCriterion("order_scale in", values, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleNotIn(List<BigDecimal> values) {
            addCriterion("order_scale not in", values, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("order_scale between", value1, value2, "orderScale");
            return (Criteria) this;
        }

        public Criteria andOrderScaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("order_scale not between", value1, value2, "orderScale");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentIsNull() {
            addCriterion("suggested_replenishment is null");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentIsNotNull() {
            addCriterion("suggested_replenishment is not null");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment =", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentNotEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment <>", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentGreaterThan(BigDecimal value) {
            addCriterion("suggested_replenishment >", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment >=", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentLessThan(BigDecimal value) {
            addCriterion("suggested_replenishment <", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentLessThanOrEqualTo(BigDecimal value) {
            addCriterion("suggested_replenishment <=", value, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentIn(List<BigDecimal> values) {
            addCriterion("suggested_replenishment in", values, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentNotIn(List<BigDecimal> values) {
            addCriterion("suggested_replenishment not in", values, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("suggested_replenishment between", value1, value2, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andSuggestedReplenishmentNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("suggested_replenishment not between", value1, value2, "suggestedReplenishment");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceIsNull() {
            addCriterion("purchase_price is null");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceIsNotNull() {
            addCriterion("purchase_price is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceEqualTo(BigDecimal value) {
            addCriterion("purchase_price =", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceNotEqualTo(BigDecimal value) {
            addCriterion("purchase_price <>", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceGreaterThan(BigDecimal value) {
            addCriterion("purchase_price >", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("purchase_price >=", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceLessThan(BigDecimal value) {
            addCriterion("purchase_price <", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("purchase_price <=", value, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceIn(List<BigDecimal> values) {
            addCriterion("purchase_price in", values, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceNotIn(List<BigDecimal> values) {
            addCriterion("purchase_price not in", values, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchase_price between", value1, value2, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andPurchasePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("purchase_price not between", value1, value2, "purchasePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceIsNull() {
            addCriterion("sale_price is null");
            return (Criteria) this;
        }

        public Criteria andSalePriceIsNotNull() {
            addCriterion("sale_price is not null");
            return (Criteria) this;
        }

        public Criteria andSalePriceEqualTo(BigDecimal value) {
            addCriterion("sale_price =", value, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceNotEqualTo(BigDecimal value) {
            addCriterion("sale_price <>", value, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceGreaterThan(BigDecimal value) {
            addCriterion("sale_price >", value, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("sale_price >=", value, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceLessThan(BigDecimal value) {
            addCriterion("sale_price <", value, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("sale_price <=", value, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceIn(List<BigDecimal> values) {
            addCriterion("sale_price in", values, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceNotIn(List<BigDecimal> values) {
            addCriterion("sale_price not in", values, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sale_price between", value1, value2, "salePrice");
            return (Criteria) this;
        }

        public Criteria andSalePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sale_price not between", value1, value2, "salePrice");
            return (Criteria) this;
        }

        public Criteria andNoteIsNull() {
            addCriterion("note is null");
            return (Criteria) this;
        }

        public Criteria andNoteIsNotNull() {
            addCriterion("note is not null");
            return (Criteria) this;
        }

        public Criteria andNoteEqualTo(String value) {
            addCriterion("note =", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotEqualTo(String value) {
            addCriterion("note <>", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteGreaterThan(String value) {
            addCriterion("note >", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteGreaterThanOrEqualTo(String value) {
            addCriterion("note >=", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteLessThan(String value) {
            addCriterion("note <", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteLessThanOrEqualTo(String value) {
            addCriterion("note <=", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteLike(String value) {
            addCriterion("note like", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotLike(String value) {
            addCriterion("note not like", value, "note");
            return (Criteria) this;
        }

        public Criteria andNoteIn(List<String> values) {
            addCriterion("note in", values, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotIn(List<String> values) {
            addCriterion("note not in", values, "note");
            return (Criteria) this;
        }

        public Criteria andNoteBetween(String value1, String value2) {
            addCriterion("note between", value1, value2, "note");
            return (Criteria) this;
        }

        public Criteria andNoteNotBetween(String value1, String value2) {
            addCriterion("note not between", value1, value2, "note");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNull() {
            addCriterion("delete_status is null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIsNotNull() {
            addCriterion("delete_status is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusEqualTo(Integer value) {
            addCriterion("delete_status =", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotEqualTo(Integer value) {
            addCriterion("delete_status <>", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThan(Integer value) {
            addCriterion("delete_status >", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("delete_status >=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThan(Integer value) {
            addCriterion("delete_status <", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusLessThanOrEqualTo(Integer value) {
            addCriterion("delete_status <=", value, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusIn(List<Integer> values) {
            addCriterion("delete_status in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotIn(List<Integer> values) {
            addCriterion("delete_status not in", values, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusBetween(Integer value1, Integer value2) {
            addCriterion("delete_status between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andDeleteStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("delete_status not between", value1, value2, "deleteStatus");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(LocalDateTime value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(LocalDateTime value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(LocalDateTime value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(LocalDateTime value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<LocalDateTime> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<LocalDateTime> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(LocalDateTime value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(LocalDateTime value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(LocalDateTime value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(LocalDateTime value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(LocalDateTime value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(LocalDateTime value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<LocalDateTime> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<LocalDateTime> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(LocalDateTime value1, LocalDateTime value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}