package com.mybatisdynamic.mapper.sqlserver;

import com.mybatisdynamic.pojo.entity.sqlserver.StoreCommodity;
import com.mybatisdynamic.pojo.entity.sqlserver.StoreCommodityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface StoreCommodityDao {
    long countByExample(StoreCommodityExample example);

    int deleteByExample(StoreCommodityExample example);

    int insert(StoreCommodity record);

    int insertSelective(StoreCommodity record);

    List<StoreCommodity> selectByExample(StoreCommodityExample example);

    int updateByExampleSelective(@Param("record") StoreCommodity record, @Param("example") StoreCommodityExample example);

    int updateByExample(@Param("record") StoreCommodity record, @Param("example") StoreCommodityExample example);

    List<StoreCommodity> selectAll();
}