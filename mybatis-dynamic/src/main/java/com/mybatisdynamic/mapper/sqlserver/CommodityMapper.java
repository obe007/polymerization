package com.mybatisdynamic.mapper.sqlserver;

import com.mybatisdynamic.pojo.entity.sqlserver.Commodity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CommodityMapper {

    List<Commodity> selectAll();

    List<Commodity> selectByName(String name);

    List<String> selectAllName();
}
