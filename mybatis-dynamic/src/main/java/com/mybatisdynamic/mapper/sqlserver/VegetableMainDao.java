package com.mybatisdynamic.mapper.sqlserver;

import com.mybatisdynamic.pojo.entity.sqlserver.VegetableMain;
import com.mybatisdynamic.pojo.entity.sqlserver.VegetableMainExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VegetableMainDao {
    long countByExample(VegetableMainExample example);

    int deleteByExample(VegetableMainExample example);

    int insert(VegetableMain record);

    int insertSelective(VegetableMain record);

    List<VegetableMain> selectByExample(VegetableMainExample example);

    int updateByExampleSelective(@Param("record") VegetableMain record, @Param("example") VegetableMainExample example);

    int updateByExample(@Param("record") VegetableMain record, @Param("example") VegetableMainExample example);

    List<VegetableMain> selectAll();
}
