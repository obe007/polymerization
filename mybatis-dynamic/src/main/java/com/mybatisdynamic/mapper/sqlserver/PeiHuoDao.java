package com.mybatisdynamic.mapper.sqlserver;

import com.mybatisdynamic.pojo.entity.sqlserver.PeiHuo;
import com.mybatisdynamic.pojo.entity.sqlserver.PeiHuoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PeiHuoDao {
    long countByExample(PeiHuoExample example);

    int deleteByExample(PeiHuoExample example);

    int insert(PeiHuo record);

    int insertSelective(PeiHuo record);

    List<PeiHuo> selectByExample(PeiHuoExample example);

    int updateByExampleSelective(@Param("record") PeiHuo record, @Param("example") PeiHuoExample example);

    int updateByExample(@Param("record") PeiHuo record, @Param("example") PeiHuoExample example);

    List<PeiHuo> selectAll();
}