package com.mybatisdynamic.mapper.sqlserver;

import com.mybatisdynamic.pojo.entity.sqlserver.GreenqtySum;
import com.mybatisdynamic.pojo.entity.sqlserver.GreenqtySumExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GreenqtySumDao {
    long countByExample(GreenqtySumExample example);

    int deleteByExample(GreenqtySumExample example);

    int insert(GreenqtySum record);

    int insertSelective(GreenqtySum record);

    List<GreenqtySum> selectByExample(GreenqtySumExample example);

    int updateByExampleSelective(@Param("record") GreenqtySum record, @Param("example") GreenqtySumExample example);

    int updateByExample(@Param("record") GreenqtySum record, @Param("example") GreenqtySumExample example);
}