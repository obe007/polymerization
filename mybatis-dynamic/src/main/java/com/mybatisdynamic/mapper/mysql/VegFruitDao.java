package com.mybatisdynamic.mapper.mysql;

import com.mybatisdynamic.pojo.entity.mysql.VegFruit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 蔬果要货产品表(蔬果要货功能中的 蔬果产品表) Mapper 接口
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
public interface VegFruitDao extends BaseMapper<VegFruit> {

}
