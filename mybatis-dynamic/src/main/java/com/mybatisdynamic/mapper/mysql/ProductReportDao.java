package com.mybatisdynamic.mapper.mysql;

import com.mybatisdynamic.pojo.entity.mysql.ProductReport;
import com.mybatisdynamic.pojo.entity.mysql.ProductReportExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductReportDao {
    long countByExample(ProductReportExample example);

    int deleteByExample(ProductReportExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ProductReport record);

    int insertSelective(ProductReport record);

    List<ProductReport> selectByExample(ProductReportExample example);

    ProductReport selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ProductReport record, @Param("example") ProductReportExample example);

    int updateByExample(@Param("record") ProductReport record, @Param("example") ProductReportExample example);

    int updateByPrimaryKeySelective(ProductReport record);

    int updateByPrimaryKey(ProductReport record);
}