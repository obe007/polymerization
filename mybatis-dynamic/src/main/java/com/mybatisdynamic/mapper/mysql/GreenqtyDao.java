package com.mybatisdynamic.mapper.mysql;

import com.mybatisdynamic.pojo.entity.mysql.Greenqty;
import com.mybatisdynamic.pojo.entity.mysql.GreenqtyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GreenqtyDao {
    long countByExample(GreenqtyExample example);

    int deleteByExample(GreenqtyExample example);

    int insert(Greenqty record);

    int insertSelective(Greenqty record);

    List<Greenqty> selectByExample(GreenqtyExample example);

    int updateByExampleSelective(@Param("record") Greenqty record, @Param("example") GreenqtyExample example);

    int updateByExample(@Param("record") Greenqty record, @Param("example") GreenqtyExample example);

    List<Greenqty> selectAll();
}