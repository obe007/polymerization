package com.mybatisdynamic.mapper.mysql;

import com.mybatisdynamic.pojo.entity.mysql.VegFruitReplenishment;
import com.mybatisdynamic.pojo.entity.mysql.VegFruitReplenishmentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VegFruitReplenishmentDao {
    long countByExample(VegFruitReplenishmentExample example);

    int deleteByExample(VegFruitReplenishmentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(VegFruitReplenishment record);

    int insertSelective(VegFruitReplenishment record);

    List<VegFruitReplenishment> selectByExample(VegFruitReplenishmentExample example);

    VegFruitReplenishment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") VegFruitReplenishment record, @Param("example") VegFruitReplenishmentExample example);

    int updateByExample(@Param("record") VegFruitReplenishment record, @Param("example") VegFruitReplenishmentExample example);

    int updateByPrimaryKeySelective(VegFruitReplenishment record);

    int updateByPrimaryKey(VegFruitReplenishment record);
}