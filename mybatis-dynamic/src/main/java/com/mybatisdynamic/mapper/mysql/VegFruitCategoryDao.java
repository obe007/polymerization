package com.mybatisdynamic.mapper.mysql;

import com.mybatisdynamic.pojo.entity.mysql.VegFruitCategory;
import com.mybatisdynamic.pojo.entity.mysql.VegFruitCategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VegFruitCategoryDao {
    long countByExample(VegFruitCategoryExample example);

    int deleteByExample(VegFruitCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(VegFruitCategory record);

    int insertSelective(VegFruitCategory record);

    List<VegFruitCategory> selectByExampleWithBLOBs(VegFruitCategoryExample example);

    List<VegFruitCategory> selectByExample(VegFruitCategoryExample example);

    VegFruitCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") VegFruitCategory record, @Param("example") VegFruitCategoryExample example);

    int updateByExampleWithBLOBs(@Param("record") VegFruitCategory record, @Param("example") VegFruitCategoryExample example);

    int updateByExample(@Param("record") VegFruitCategory record, @Param("example") VegFruitCategoryExample example);

    int updateByPrimaryKeySelective(VegFruitCategory record);

    int updateByPrimaryKeyWithBLOBs(VegFruitCategory record);

    int updateByPrimaryKey(VegFruitCategory record);
}