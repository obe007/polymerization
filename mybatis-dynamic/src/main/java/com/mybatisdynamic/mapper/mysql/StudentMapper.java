package com.mybatisdynamic.mapper.mysql;

import com.mybatisdynamic.pojo.entity.mysql.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Mapper
public interface StudentMapper {

    List<Student> selectAll();

    int insertSelective(Student student);

    List<Student> searchByName(@RequestParam("name") String name);

    Student selectByIdOrName(Student student);
}
