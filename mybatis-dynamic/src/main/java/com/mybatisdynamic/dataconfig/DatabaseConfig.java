package com.mybatisdynamic.dataconfig;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 所有数据库，mysql，MongoDB 等都在此扫描包
 * 配置dao层位置，可用, 写多个地址
 * 替代 @MapperScan(basePackages = "com.ngs.management.dao")
 */
@Configuration
//@EnableTransactionManagement
//@MapperScan(basePackages = {"com.mybatisdynamic.mapper.mysql", "com.mybatisdynamic.mapper.sqlserver"})
@ComponentScan(basePackages = {"com.baomidou.mybatisplus.core.mapper"})
public class DatabaseConfig {
}
