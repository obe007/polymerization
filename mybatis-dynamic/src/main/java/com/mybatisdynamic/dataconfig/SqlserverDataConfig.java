package com.mybatisdynamic.dataconfig;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.mybatisdynamic.mapper.sqlserver", sqlSessionFactoryRef = "sqlserverSqlSessionFactory")
public class SqlserverDataConfig {

    final String MAPPER_LOCATIONS = "classpath:mappers/sqlserver/*.xml";
    final String TYPE_ALIASES_PACKAGE = "com.mybatisdynamic.pojo.entity.sqlserver";

    @Bean(name = "sqlserverDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.druid.sqlserver")
    public DataSource setDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sqlserverSqlSessionFactory")
    public SqlSessionFactory setSqlSessionFactory(@Qualifier("sqlserverDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        //指定mapper文件所在位置
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources( MAPPER_LOCATIONS ));
        bean.setTypeAliasesPackage( TYPE_ALIASES_PACKAGE );
        return bean.getObject();
    }

    @Bean(name = "sqlserverSqlSessionTemplate")
    public SqlSessionTemplate setSqlSessionTemplate(@Qualifier("sqlserverSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate( sqlSessionFactory );
    }

//    @Bean(name = "sqlserverTransactionManager")
//    public DataSourceTransactionManager transactionManager(@Qualifier("sqlserverDataSource") DataSource dataSource) {
//        return new DataSourceTransactionManager( dataSource );
//    }
//
//    @Bean(name = "sqlserverSqlSessionFactory")
//    public SqlSessionFactory basicSqlSessionFactory(@Qualifier("sqlserverDataSource") DataSource basicDataSource) throws Exception {
//        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
//        factoryBean.setDataSource( basicDataSource );
//        factoryBean.setMapperLocations(
//                new PathMatchingResourcePatternResolver().getResources("classpath:mappers/sqlserver/*.xml"));
//        return factoryBean.getObject();
//
//    }
//
//    @Bean(name = "sqlserverSqlSessionTemplate")
//    public SqlSessionTemplate testSqlSessionTemplate(
//            @Qualifier("sqlserverSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
//        return new SqlSessionTemplate(sqlSessionFactory);
//    }


}
