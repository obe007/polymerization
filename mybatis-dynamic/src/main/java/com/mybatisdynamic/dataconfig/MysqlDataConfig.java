package com.mybatisdynamic.dataconfig;


import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.sql.DataSource;
import javax.transaction.UserTransaction;

@Configuration
@MapperScan(basePackages = {"com.mybatisdynamic.mapper.mysql", "com.baomidou.mybatisplus.core.mapper"}, sqlSessionFactoryRef = "mysqlSqlSessionFactory")
public class MysqlDataConfig {

    final String MAPPER_LOCATIONS = "classpath:mappers/mysql/*.xml";
    final String TYPE_ALIASES_PACKAGE = "com.mybatisdynamic.pojo.entity.mysql";

    @Bean(name = "mysqlDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.druid.mysql")
    @Primary
    public DataSource setDataSource() {
        return DataSourceBuilder.create().build();
    }

    /** 使之作为总事务管理器 */
    @Bean(name = "transactionManager")
    public JtaTransactionManager regTransactionManager() {
        UserTransactionManager userTransactionManager = new UserTransactionManager();
        UserTransaction userTransaction = new UserTransactionImp();
        return new JtaTransactionManager( userTransaction, userTransactionManager );
    }

    @Bean(name = "mysqlSqlSessionFactory")
    @Primary
    public SqlSessionFactory setSqlSessionFactory(@Qualifier("mysqlDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource( dataSource );
        //指定mapper文件所在位置
        bean.setMapperLocations( new PathMatchingResourcePatternResolver().getResources( MAPPER_LOCATIONS ) );
        bean.setTypeAliasesPackage( TYPE_ALIASES_PACKAGE );
        return bean.getObject();
    }

    @Bean(name = "mysqlSqlSessionTemplate")
    @Primary
    public SqlSessionTemplate setSqlSessionTemplate(@Qualifier("mysqlSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(sqlSessionFactory);
    }



//    @Bean(name = "mysqlTransactionManager")
//    @Primary
//    public DataSourceTransactionManager mysqlTransactionManager() {
//        return new DataSourceTransactionManager( mysqlDataSource() );
//    }
//    public SqlSessionFactory mysqlSqlSessionFactory(
//            @Qualifier("mysqlDataSource") DataSource mysqlDataSource) throws  Exception {
//        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
//        sessionFactory.setDataSource( mysqlDataSource );
//        sessionFactory.setMapperLocations(
//                new PathMatchingResourcePatternResolver().getResource(MysqlDataConfig.MAPPER_CONFIG)
//        );
//        return sessionFactory.getObject();
//    }



}
