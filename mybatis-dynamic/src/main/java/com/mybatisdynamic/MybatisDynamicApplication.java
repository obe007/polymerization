package com.mybatisdynamic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement //启用事务管理器
public class MybatisDynamicApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisDynamicApplication.class, args);
    }

}
