package com.mybatisdynamic.service;

import com.mybatisdynamic.pojo.entity.mysql.Student;

import java.util.List;

public interface StudentService {
    List<Student> selectAll();
}
