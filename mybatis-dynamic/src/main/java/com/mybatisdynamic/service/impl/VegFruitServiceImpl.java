package com.mybatisdynamic.service.impl;

import com.mybatisdynamic.pojo.entity.mysql.VegFruit;
import com.mybatisdynamic.mapper.mysql.VegFruitDao;
import com.mybatisdynamic.service.VegFruitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 蔬果要货产品表(蔬果要货功能中的 蔬果产品表) 服务实现类
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
@Service
public class VegFruitServiceImpl extends ServiceImpl<VegFruitDao, VegFruit> implements VegFruitService {

}
