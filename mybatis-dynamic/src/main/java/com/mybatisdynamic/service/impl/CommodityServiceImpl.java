package com.mybatisdynamic.service.impl;

import com.mybatisdynamic.mapper.sqlserver.CommodityMapper;
import com.mybatisdynamic.pojo.entity.sqlserver.Commodity;
import com.mybatisdynamic.service.CommodityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(transactionManager = "transactionManager")
public class CommodityServiceImpl implements CommodityService {
    @Resource
    CommodityMapper commodityMapper;

    @Override
    public List<Commodity> selectAll() {
        return commodityMapper.selectAll();
    }

    @Override
    public List<Commodity> selectByName(String name) {
        return commodityMapper.selectByName( name );
    }

    @Override
    public List<String> selectAllName() {
        return commodityMapper.selectAllName();
    }
}
