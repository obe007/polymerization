package com.mybatisdynamic.service.impl;

import com.mybatisdynamic.mapper.mysql.StudentMapper;
import com.mybatisdynamic.pojo.entity.mysql.Student;
import com.mybatisdynamic.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional(transactionManager = "transactionManager")
public class StudentServiceImpl implements StudentService {
    @Resource
    StudentMapper studentMapper;

    @Override
    public List<Student> selectAll() {
        return studentMapper.selectAll();
    }
}
