package com.mybatisdynamic.service;

import com.mybatisdynamic.pojo.entity.mysql.VegFruit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 蔬果要货产品表(蔬果要货功能中的 蔬果产品表) 服务类
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
public interface VegFruitService extends IService<VegFruit> {

}
