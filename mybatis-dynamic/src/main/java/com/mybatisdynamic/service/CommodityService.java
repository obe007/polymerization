package com.mybatisdynamic.service;

import com.mybatisdynamic.pojo.entity.sqlserver.Commodity;

import java.util.List;

public interface CommodityService {
    List<Commodity> selectAll();

    List<Commodity> selectByName(String name);

    List<String> selectAllName();
}
