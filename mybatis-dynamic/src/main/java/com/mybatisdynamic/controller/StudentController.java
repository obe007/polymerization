package com.mybatisdynamic.controller;

import com.common.pojo.vo.ResultVO;
import com.common.utils.ResultVOUtil;
import com.mybatisdynamic.pojo.entity.mysql.Student;
import com.mybatisdynamic.pojo.entity.sqlserver.Commodity;
import com.mybatisdynamic.service.CommodityService;
import com.mybatisdynamic.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/list")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    CommodityService commodityService;

    @RequestMapping(value = "/list_student", method = RequestMethod.GET)
    @ResponseBody
    public ResultVO studentList() {
        List<Student> students = studentService.selectAll();
        return ResultVOUtil.success( students );
    }

    @RequestMapping(value = "/list_commodity", method = RequestMethod.GET)
    @ResponseBody
    public ResultVO commodityList() {
        List<Commodity> commodities = commodityService.selectAll();
        return ResultVOUtil.success( commodities );
    }

    @RequestMapping(value = "/all_name_commodity", method = RequestMethod.GET)
    @ResponseBody
    public List<String> allNameByCommodity() {
        List<String> commodities = commodityService.selectAllName();
        return commodities;
    }

    @RequestMapping(value = "/find_commodity_name", method = RequestMethod.GET)
    @ResponseBody
    public ResultVO findNameByCommodity() {
        List<Commodity> commodities = commodityService.selectByName( "舟山小黄鱼4/" );
        return ResultVOUtil.success( commodities );
    }

}
