package com.mybatisdynamic.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 蔬果要货产品表(蔬果要货功能中的 蔬果产品表) 前端控制器
 * </p>
 *
 * @author Jax
 * @since 2019-09-19
 */
@RestController
@RequestMapping("/veg-fruit")
public class VegFruitController {

}
