package com.nettynio.test1.nettytest;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;

import java.io.UnsupportedEncodingException;

/**
 * @author WuTianyu
 * @date 2020/8/4
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {
    // 读取数据
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        simpleRead(ctx, msg);
    }

    /**
     * 最简单的处理
     * @param ctx
     * @param msg
     * @throws UnsupportedEncodingException
     */
    public void simpleRead(ChannelHandlerContext ctx, Object msg) throws UnsupportedEncodingException {
        ByteBuf bb = (ByteBuf)msg;
        byte[] reqByte = new byte[bb.readableBytes()];
        bb.readBytes(reqByte);
        String msgStr = new String(reqByte, CharsetUtil.UTF_8);
        System.out.println("server 接收到客户端的请求：" + msgStr);
        String respStr = new StringBuilder("来自服务器的响应").append(msgStr).append("$_").toString();
        // 返回给客户端响应                                                                                                                                                       和客户端链接中断即短连接，当信息返回给客户端后中断
        ctx.writeAndFlush(Unpooled.copiedBuffer(respStr.getBytes()));//.addListener(ChannelFutureListener.CLOSE);
        return;
    }
}
