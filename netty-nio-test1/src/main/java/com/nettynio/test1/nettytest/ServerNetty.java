package com.nettynio.test1.nettytest;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author WuTianyu
 * @date 2020/8/4
 */
public class ServerNetty {
    private int port;

    public ServerNetty(int port) {
        this.port = port;
    }

    // 服务器启动
    public void action() throws InterruptedException {
        // 接收进来的连接
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        // 处理意见呗接收的连接，一单接收到，就会把连接信息注册到workerGroup上
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            // nio服务启动类
            ServerBootstrap sbs = new ServerBootstrap();
            // 配置nio服务参数
            sbs.group(bossGroup, workerGroup)
                    // 新channel如何接受进来
                    .channel(NioServerSocketChannel.class)
                    // tcp最大缓存连接个数
                    .option(ChannelOption.SO_BACKLOG, 128)
                    // 保持连接
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    // 打印日志级别
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            // 处理收到的请求
                            // 这里相当于过滤器，可以配置多个
                            ch.pipeline().addLast(new ServerHandler());
                        }
                    });

            System.err.println("server 开启--------------");
            // 绑定端口，开始接受链接
            ChannelFuture cf = sbs.bind(port).sync();

            // 开多个端口
//          ChannelFuture cf2 = sbs.bind(3333).sync();
//          cf2.channel().closeFuture().sync();

            // 等待服务端口的关闭；在这个例子中不会发生，但你可以优雅实现；关闭你的服务
            cf.channel().closeFuture().sync();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // 开启netty服务线程
    public static void main(String[] args) throws InterruptedException {
        new ServerNetty(80).action();
    }
}
