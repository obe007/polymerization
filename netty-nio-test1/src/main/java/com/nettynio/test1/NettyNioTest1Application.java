package com.nettynio.test1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NettyNioTest1Application {

    public static void main(String[] args) {
        SpringApplication.run(NettyNioTest1Application.class, args);
    }

}
