package com.nettynio.test1.selector;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author WuTianyu
 * @date 2020/8/4
 */
public class SelectorTest1 {
    public static void main(String[] args) {
        try {

            Selector sel = Selector.open();
            SocketChannel sc = SocketChannel.open();
            //SocketAddress socketAddress = new SocketAddress();
            //sc.bind()
            sc.register(sel, SelectionKey.OP_ACCEPT);
            sel.select();
            Set<SelectionKey> selKeys = sel.selectedKeys();
            List<Selector> collect = selKeys.stream().map(SelectionKey::selector).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bio() {
        /** 一对一 */
    }
    public void nio() {
        /** 通过缓冲器 */
        /** 多路复用器selector不断轮训Channel 如果读或写，会被轮训出来，selectKeys里就是有效的 */

    }


}
