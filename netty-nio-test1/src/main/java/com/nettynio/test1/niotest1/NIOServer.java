package com.nettynio.test1.niotest1;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

/**
 * @author WuTianyu
 * @date 2020/8/5
 */
public class NIOServer {
    public static void main(String[] args) throws IOException {
        // 开启多路I/O复用器
        Selector sel = Selector.open();
        new Thread(() -> {
            try {
                // 开启一个服务器socket，此socket唯一作用接收客户端连接
                ServerSocketChannel ssc = ServerSocketChannel.open();

                // 将接受连接这个socket设置为阻塞模式
                ssc.configureBlocking(true);

                // 绑定端口
                ssc.bind(new InetSocketAddress(9000));

                for (; ; ) {
                    // 阻塞模式，当连接进入时，代码才会往下走
                    SocketChannel sc = ssc.accept();

                    // 这个channel为服务端与客户端的连接，NIO，设置非阻塞
                    sc.configureBlocking(false);

                    // 新连接注册到复用器上，设置关注的事件为读
                    sc.register(sel, SelectionKey.OP_READ);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            // 分配两个内存缓冲区，分别为接收和发送事件使用
            ByteBuffer receiveBuf = ByteBuffer.allocate(1024);
            ByteBuffer sendBuf = ByteBuffer.allocate(1024);

            try {
                for (; ; ) {
                    // 查询复用器是否有事件发生
                    if (sel.select(1000) > 0) {
                        // 获取发生的事件key
                        Set<SelectionKey> keys = sel.selectedKeys();
                        Iterator<SelectionKey> iter = keys.iterator();

                        while (iter.hasNext()) {
                            SelectionKey key = iter.next();
                            // 获取key的channel
                            SocketChannel channel = (SocketChannel)key.channel();
                            try {
                                if (key.isValid()) {
                                    // 如果该key是读事件
                                    if (key.isReadable()) {
                                        // 判断是否有数据刻度，
                                        int readFlag = channel.read(receiveBuf);
                                        if (readFlag == -1) {
                                            channel.close();
                                            continue;
                                        }

                                        // 读取客户端发送的消息
                                        receiveBuf.flip();
                                        System.out.println("【新消息】" + Charset.defaultCharset().newDecoder().decode(receiveBuf).toString());
                                        receiveBuf.clear();
                                    }

                                    //...通常这边还会有写事件或连接事件等等
                                } else {
                                    channel.close();
                                }
                            } finally {
                                // key从迭代器移除
                                iter.remove();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
}
