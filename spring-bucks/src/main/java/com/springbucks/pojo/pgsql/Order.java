package com.springbucks.pojo.pgsql;

import com.common.Const;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "order")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order implements Serializable {
    @Id
    @GeneratedValue(generator = Const.UUID)
    @GenericGenerator(name = Const.UUID, strategy = Const.UUID)
    private String id;

    private String customer;

    @ManyToMany
    @JoinTable(name = "order_coffee")
    @Column(name = "coffee")
    private List<Coffee> coffees;

    private Integer state;

    @CreationTimestamp
    @Column(name = "create_time",updatable = false)
    private LocalDateTime createTime;

    @UpdateTimestamp
    @Column(name = "update_time")
    private LocalDateTime updateTime;
}
