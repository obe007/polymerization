package com.user.api.utils;

import com.user.api.enums.ResponseState;
import com.user.api.vo.ResultVO;

import java.io.Serializable;

@SuppressWarnings({"all", "unchecked"})
public class JsonResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建success 的几种可能
     * @param <T>
     * @return
     */
    public static <T> ResultVO success() {
        return new ResultVO( ResponseState.SUCCESS.getCode() );
    }

    public static <T> ResultVO successMessage() {
        return new ResultVO( ResponseState.SUCCESS.getCode(), ResponseState.SUCCESS.getMsg() );
    }

    public static <T> ResultVO successMessage(String msg) {
        return new ResultVO( ResponseState.SUCCESS.getCode(), msg );
    }

    public static <T> ResultVO success(T data) {
        return new ResultVO( ResponseState.SUCCESS.getCode(), ResponseState.SUCCESS.getMsg(), data );
    }

    public static <T> ResultVO success(String msg, T data) {
        return new ResultVO( ResponseState.SUCCESS.getCode(), msg, data );
    }

    /**
     * 创建ERROR
     * @param <T>
     * @return
     */
    public static <T> ResultVO error() {
        return new ResultVO(ResponseState.ERROR.getCode(), ResponseState.ERROR.getMsg());
    }

    public static <T> ResultVO error(ResponseState responseState) {
        return new ResultVO(responseState.getCode(), responseState.getMsg());
    }

    public static <T> ResultVO error(Integer code, String msg) {
        return new ResultVO(code, msg);
    }

//    public static <T> ResultVO error(ResponseState resultCode) {
//        return new ResultVO(resultCode.ERROR.getCode(), ResponseState.ERROR.getMsg());
//    }

    /**
     * 未登录返回结果
     */
    public static <T> ResultVO unauthorized(T data) {
        return new ResultVO(ResponseState.UNAUTHORIZED.getCode(), ResponseState.UNAUTHORIZED.getMsg(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> ResultVO forbidden(T data) {
        return new ResultVO(ResponseState.FORBIDDEN.getCode(), ResponseState.FORBIDDEN.getMsg(), data);
    }

}
