package com.user.api.exception;

import com.user.api.enums.BaseEnum;
import lombok.Getter;

/**
 * <p>业务异常</p>
 * <p>业务处理时，出现异常，可以抛出该异常</p>
 *
 * @date 2019/5/2
 */
@Getter
public class BusinessException extends BaseException {

    private static final long serialVersionUID = 1L;

    public BusinessException(BaseEnum responseEnum) {
        super(responseEnum);
    }

    public BusinessException(BaseEnum responseEnum, Object[] args, String message) {
        super(responseEnum, args, message);
    }

    public BusinessException(BaseEnum responseEnum, Object[] args, String message, Throwable cause) {
        super(responseEnum, args, message, cause);
    }
}
