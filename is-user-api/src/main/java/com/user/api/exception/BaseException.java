package com.user.api.exception;

import com.user.api.enums.BaseEnum;
import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {
    /**
     * 返回码,返回信息
     */
    private BaseEnum responseEnum;
    /**
     * 异常消息参数
     */
    private Object[] args;

    public BaseException() {
        super();
    }

    public BaseException(BaseEnum responseEnum) {
        super(responseEnum.getMsg());
        this.responseEnum = responseEnum;
    }

    public BaseException(Integer code, String msg) {
        super(msg);
        this.responseEnum = new BaseEnum() {
            @Override
            public Integer getCode() {
                return code;
            }

            @Override
            public String getMsg() {
                return msg;
            }
        };
    }

    public BaseException(BaseEnum responseEnum,
                         Object[] args, String msg) {
        super(msg);
        this.responseEnum = responseEnum;
        this.args = args;
    }

    public BaseException(BaseEnum responseEnum, Object[] args,
                         String msg, Throwable cause) {
        super(msg, cause);
        this.responseEnum = responseEnum;
        this.args = args;
    }
}
