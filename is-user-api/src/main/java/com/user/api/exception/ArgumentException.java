package com.user.api.exception;

import com.user.api.enums.BaseEnum;
import lombok.Getter;

/**
 * <p>参数异常</p>
 * <p>在处理业务过程中校验参数出现错误, 可以抛出该异常</p>
 * <p>编写公共代码（如工具类）时，对传入参数检查不通过时，可以抛出该异常</p>
 * @date 2019/5/2
 */
@Getter
public class ArgumentException extends BaseException {

    private static final long serialVersionUID = 1L;

    public ArgumentException(BaseEnum responseEnum) {
        super(responseEnum);
    }

    public ArgumentException(BaseEnum responseEnum, Object[] args, String msg) {
        super(responseEnum, args, msg);
    }

    public ArgumentException(BaseEnum responseEnum, Object[] args, String msg, Throwable cause) {
        super(responseEnum, args, msg, cause);
    }
}
