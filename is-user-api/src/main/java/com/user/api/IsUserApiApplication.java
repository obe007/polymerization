package com.user.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()    // scanBasePackageClasses = BaseExceptionHandler.class
public class IsUserApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(IsUserApiApplication.class, args);
    }

}
