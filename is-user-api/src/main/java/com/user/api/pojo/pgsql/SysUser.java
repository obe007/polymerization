package com.user.api.pojo.pgsql;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by WuTianyu on 2020/1/13 13:43.
 */
@Data
@Entity
public class SysUser implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    private String username;

    private String password;

    private String email;

    private String nickname;

    private String openid;

    private String role;

    private String status;

    private String create_time;

    private String update_time;
}
