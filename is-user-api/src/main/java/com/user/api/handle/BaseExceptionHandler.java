package com.user.api.handle;

import com.user.api.enums.ResponseState;
import com.user.api.exception.ArgumentException;
import com.user.api.exception.BaseException;
import com.user.api.exception.BusinessException;
import com.user.api.exception.ValidationException;
import com.user.api.utils.JsonResponse;
import com.user.api.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLSyntaxErrorException;

/**
 * 当抛出这个异常时 会被ControllerAdvice拦截
 * <p>
 * @ExceptionHandler (value = 类名.class)  指明要拦截的异常是哪个类
 * <p/>
 */
@RestControllerAdvice
@Slf4j
@SuppressWarnings({"all", "unchecked"})
public class BaseExceptionHandler {

    /** 拦截BaseException */
    @ExceptionHandler(BaseException.class)
    public ResultVO handlerBaseException(BaseException e) {
        return JsonResponse.error(e.getResponseEnum().getCode(), e.getResponseEnum().getMsg());
    }

    @ExceptionHandler(ArgumentException.class)
    public ResultVO handlerArgumentException(ArgumentException e) {
        return JsonResponse.error(e.getResponseEnum().getCode(), e.getResponseEnum().getMsg());
    }

    @ExceptionHandler(ValidationException.class)
    public ResultVO handlerValidationException(ValidationException e) {
        return JsonResponse.error(e.getResponseEnum().getCode(), e.getResponseEnum().getMsg());
    }

    @ExceptionHandler(BusinessException.class)
    public ResultVO handlerBusinessException(BusinessException e) {
        return JsonResponse.error(e.getResponseEnum().getCode(), e.getResponseEnum().getMsg());
    }

    /** TODO Linux时开启 */
    @ExceptionHandler(SQLSyntaxErrorException.class)
    public ResultVO handlerSQLSyntaxErrorException(SQLSyntaxErrorException e) {
        log.error("【sql错误】【output】code={}, msg={}", e.getErrorCode(), e.getMessage());
        return JsonResponse.error(ResponseState.SQL_EXECUTE_ERROR);
    }
}
