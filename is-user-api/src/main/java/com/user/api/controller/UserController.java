package com.user.api.controller;

import com.user.api.enums.ResponseState;
import com.user.api.exception.BaseException;
import com.user.api.pojo.pgsql.SysUser;
import com.user.api.repository.SysUserDao;
import com.user.api.utils.JsonResponse;
import com.user.api.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by WuTianyu on 2019/12/25 13:34.
 */
@RestController
@RequestMapping("/")
@Slf4j
@SuppressWarnings({"all", "unchecked"})
public class UserController {
    @Autowired
    SysUserDao sysUserDao;

    /**
     * 测试
     */
    @GetMapping(value = "/list")
    public ResultVO<SysUser> list() {
//        throw new BaseException(ResponseState.SUCCESS);
        return JsonResponse.success( null );
    }
}
