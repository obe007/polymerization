package com.user.api.enums;

/**
 * <p>
 *  异常返回码枚举接口
 * </p>
 *
 * 基类
 * @author sprainkle
 * @date 2019/5/2
 */
public interface BaseEnum {
    /**
     * 获取返回码
     * @return 返回码
     */
    Integer getCode();

    /**
     * 获取返回信息
     * @return 返回信息
     */
    String getMsg();
}
