package com.user.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 返回前端 相应码
 */
@Getter
@AllArgsConstructor
public enum ResponseState implements BaseEnum {

    SUCCESS(0, "成功"),
    ERROR(1, "失败"),
    NEED_LOGIN(10, "需要登录"),
    ILLEGAL_ARGUMENT(2, "非法参数"),

    /** 400 */
    UNAUTHORIZED(401, "您还没有登录或验证已过期"),

    FORBIDDEN(403, "没有相关权限"),

    /** 500 */
    USER_OR_PASSWORD_FAILED(501, "用户名或密码错误"),

    /** 600 用户信息 */
    REGISTER_ERROR(601, "用户名重复"),

    INFO_SAVE_FAIL(602, "信息保存失败"),

    LOGIN_PARAM_ERROR(603, "登录参数不正确"),

    LOGIN_CAPTCHA_ERROR(604, "验证码不正确"),

    LOGIN_USER_OR_PASS_ERROR(605, "用户名或密码不正确"),

    USER_STATUS_DISABLED(606, "用户已禁用，通知管理员开通权限"),

    LOGIN_PASS_ERROR(607, "密码不正确"),

    LOGIN_USERNAME_UNUSUAL(608, "用户名不存在"),

    USER_NOT_HAS_STORE_AUTHORITY(609, "当前用户未获得此门店的权限"),

    /** 700 注册信息 */
    STORE_ID_NOT_EXIST(701, "有门店id不存在"),

    BINDING_ERROR(701, "验证失败"),

    USER_ROLE_INSERT_FAIL(701, "用户和门店对应关系插入失败"),

    /** 800 更新失败 */
    UPDATE_PRODUCT_REPORT_FAIL(801, "更新产品上报失败"),

    UPDATE_VEG_FRUIT_REPLENISHMENT_FAIL(802, "更新蔬果要货数量失败"),

    /** 900 数据库错误 */
    SQL_EXECUTE_ERROR(901, "SQL语法错误异常"),

    /** 1000 Excel错误 */
    EXCEL_NOT_EXIST_DATA(1001, "获取数据表数据失败"),

    WRITE_RESPONSE_ERROR(1002, "数据表发送失败"),

    NOT_HAVE_IMPORT_EXCEL_ROLE(1003, "您没有导入Excel的权限"),

    EXCEL_IMPORT_TO_PRODUCT_REPORT_NULL_FILE(1004, "上传的Excel内容为空"),

    EXCEL_IMPORT_TO_PRODUCT_REPORT_TYPE_ERROR(1005, "上传的Excel类型错误,请使用office2007以后的版本"),

    EXCEL_SAVE_SERVER_ERROR(1006, "表格保存至服务器错误，保存失败"),

    EXCEL_PATH_NOT_FOUND(1006, "上传的Excel路径，无法找到"),

    EXCEL_TO_PRODUCT_REPORT_ERROR(1007, "表格转Bean实体类失败"),

    EXCEL_TO_PRODUCT_REPORT_DATA_IS_ZERO(1008, "表格数据为空，请检查你的表格"),

    EXCEL_TO_DB_SUCCESS(1008, "成功"),

    /** 1100 账户更新错误 */
    USER_ENABLE_FAIL(1101, "账户启用失败"),

    USER_DISABLE_FAIL(1102, "账户停用失败"),

    USER_ADD_STORE_ERROR(1103, "用户添加门店失败"),

    USER_SELECT_NOT_NULL(1104, "请至少选择一个账户"),

    /** 1200 产品上报错误 */
    REPLENISHMENT_NOT_MIN_REPLENISHMENT_MULTIPLE(1201, "修改要货数为起订量倍数"),

    /** 1300 蔬果要货错误 */
    VEG_FRUIT_UPDATE_TIME_NOT_ALLOWED(1301, "更新时间到16点（下午四点）为止，请明天重试"),

    ;

    private final Integer code;
    private final String msg;
}
