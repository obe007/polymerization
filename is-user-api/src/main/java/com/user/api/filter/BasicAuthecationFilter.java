package com.user.api.filter;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;
import com.user.api.enums.ResponseState;
import com.user.api.exception.ValidationException;
import com.user.api.repository.SysUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by WuTianyu on 2020/1/13 14:06.
 */
@Component
public class BasicAuthecationFilter extends OncePerRequestFilter {
    @Autowired
    private SysUserDao sysUserDao;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // 获取请求头中值
        String authHeader = request.getHeader("Authorization");
        if (StrUtil.isEmpty( authHeader )) {
            throw new ValidationException(ResponseState.ERROR);
        }
        String token64 = StrUtil.subAfter(authHeader, "base ", false);
        String token = Base64.decodeStr(token64);
        String[] itemArr = StrUtil.splitToArray(token, ':');
        System.out.println("itemArr = " + itemArr);
    }
}
