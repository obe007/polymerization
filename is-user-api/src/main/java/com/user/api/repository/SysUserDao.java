package com.user.api.repository;

import com.user.api.pojo.pgsql.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by WuTianyu on 2020/1/13 14:13.
 */
@Repository
public interface SysUserDao extends JpaRepository<SysUser, Long> {
}
