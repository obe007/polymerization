//package com.user.api.config;
//
//import com.ngs.common.enums.impl.ResponseEnum;
//import com.ngs.management.common.component.JwtAuthenticationTokenFilter;
//import com.ngs.management.common.component.RestAuthenticationEntryPoint;
//import com.ngs.management.common.component.RestfulAccessDeniedHandler;
//import com.ngs.management.pojo.bo.UserInfoUserDetails;
//import com.ngs.management.pojo.mysql.Permission;
//import com.ngs.management.pojo.mysql.UserInfo;
//import com.ngs.management.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//import java.util.List;
//
//
///**
// * SpringSecurity的配置
// * 2018/4/26.
// */
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled=true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//    @Autowired
//    private UserService userService;
//    @Autowired
//    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;
//    @Autowired
//    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
//
//    /** 放行的uri 不被过滤 */
//    private final String[] notFilterUri = new String[]{
//            "/user/login",
//            "/user/register",
//            "/store/getStores", //全部门店信息
//            "/store/getStoreHasZone", //同上
//    };
//
//    /** 静态资源放行 */
//    private final String[] staticResourceUri = new String[]{
//            "/",
//            "/*.html",
//            "/favicon.ico",
//            "/**/*.html",
//            "/**/*.css",
//            "/**/*.js",
//            "/swagger-resources/**",
//            "/v2/api-docs/**",
//            "/v2/api-docs-ext/**",
//    };
//
//    @Override
//    protected void configure(HttpSecurity httpSecurity) throws Exception {
//        httpSecurity.csrf()// 由于使用的是JWT，我们这里不需要csrf
//                .disable()
//                .sessionManagement()// 基于token，所以不需要session
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET,    // 允许对于网站静态资源的无授权访问
//                        staticResourceUri
//                )
//                .permitAll()
//                .antMatchers( notFilterUri )// 对登录注册要允许匿名访问
//                .permitAll()
//                .antMatchers( HttpMethod.OPTIONS )//跨域请求会先进行一次options请求
//                .permitAll()
//                .antMatchers("/**")//测试时全部运行访问
//                .permitAll()
//                .anyRequest()// 除上面外的所有请求全部需要鉴权认证
//                .authenticated();
//        // 禁用缓存
//        httpSecurity.headers().cacheControl();
//        // 添加JWT filter
//        httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
//        //添加自定义未授权和未登录结果返回
//        httpSecurity.exceptionHandling()
//                .accessDeniedHandler(restfulAccessDeniedHandler)
//                .authenticationEntryPoint(restAuthenticationEntryPoint);
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService())
//                .passwordEncoder(passwordEncoder());
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Override
//    @Bean
//    public UserDetailsService userDetailsService() {
//        //获取登录用户信息
//        return username -> {
//            UserInfo userInfo = userService.getUserInfoByUsername(username);
//            if (userInfo != null) {
//                List<Permission> permissionList = userService.getPermissionsByUserId(userInfo.getId());
//                return new UserInfoUserDetails(userInfo, permissionList);
//            }
//            // 获取用户名失败，校验密码失败，抛出异常
//            throw new UsernameNotFoundException( ResponseEnum.USER_OR_PASSWORD_FAILED.getMsg() );
//        };
//    }
//
//    @Bean
//    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter(){
//        return new JwtAuthenticationTokenFilter();
//    }
//
//    @Bean
//    @Override
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
//
//}
