//package com.user.api.config;
//
//import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
///**
// * Swagger-ui2.9.0API文档的配置
// */
//@Configuration
//@EnableSwagger2
//@EnableSwaggerBootstrapUI
//public class Swagger2Config {
//    @Bean
//    public Docket createRestApi(){
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("api")
//                .apiInfo(apiInfo())
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.ngs.management.controller")) //要生成文档的controller
//                .paths(PathSelectors.any())
//                .build();
//    }
//
//
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("门店系统API文档")
//                .description("门店系统API文档")
////                .termsOfServiceUrl("http://localhost:8080/")
////                .contact(new Contact("WuTianyu", "https://gitee.com/obe007", "275152725@qq.com"))
//                .version("1.0")
//                .build();
//    }
//}
