package com.user.api.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 所有数据库，mysql，MongoDB 等都在此扫描包
 * 配置dao层位置，可用, 写多个地址
 * 替代 @MapperScan(basePackages = "com.ngs.management.dao")
 */
@Configuration
@EnableTransactionManagement
//@ComponentScan(basePackages = "com.ngs.management.dao") // 被MybatisPlusConfig 中mapperScan替代
@ComponentScan(basePackages = {
        // 全局统一异常拦截器
        "com.user.api.handle"
})
public class ComponentConfig {
}
