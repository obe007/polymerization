package com.jdk14.demo.filesystems;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ArrayUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

/**
 * Created by WuTianyu on 2020/07/21 10:44
 * 新函数测试 FileSystems.newFileSystem
 */
public class NewFileSystem {
    public static final String nodejs_zipUri = "D:\\Program Files\\nodejs.zip";

    public static void main(String[] args) {
        try {
            File nodejsZip = new File(nodejs_zipUri);
        /*AtomicReference<String> parent = new AtomicReference<>();

            JarFile jarFile = new JarFile(nodejsZip);
            jarFile.stream().forEach( jarEntry -> {
                if (jarEntry.getName().lastIndexOf('/') != -1) {
                    System.out.println("parent ->" + jarEntry.getName().substring(0, jarEntry.getName().lastIndexOf('/')));
                    parent.set(jarEntry.getRealName().substring(0, jarEntry.getName().lastIndexOf('/')));
                    if (jarEntry.getName().lastIndexOf('/') != (jarEntry.getName().length() - 1)) {
                        System.out.println("jarEntry = " + jarEntry.getName());
                    }
                }
            });
            jarFile.close();*/
            System.out.println("//================================== FileSystems =============================//");
            FileSystem fileSystem = FileSystems.newFileSystem(nodejsZip.toPath());
            List<Path> filePath = new ArrayList<>();
            Iterable dirsIterable = fileSystem.getRootDirectories();
            Set<Path> pathSet = new HashSet<>();
            List<String> dirs = new ArrayList<>();
            for (Iterator dirsIter = dirsIterable.iterator(); dirsIter.hasNext();) {
                Path nowDir = (Path)dirsIter.next();
                dirs.addAll( Files.list(nowDir).map(path -> path.toString()).collect(Collectors.toList()) );
                pathSet.add(nowDir);
                filePath.add( nowDir );
            }

            Set<Path> resultSet = getDeepDirs(fileSystem, pathSet, dirs.toArray(String[]::new));
            resultSet.forEach( path -> {
                try {
                    Files.list(path).forEach(path1 -> System.out.println(path1));
                } catch (IOException e) {
//                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
//            e.printStackTrace();
        }
    }

    public static Set<Path> getDeepDirs(FileSystem fileSystem /* 文件系统 */, Set<Path> pathSet /* 所有Path */, String... dirPathArr /* 要深入的路径 */) {
//        try {
            List<String> childDirs = new ArrayList<>();
            for (String dirPath: dirPathArr) {
                Path nowDirPath = fileSystem.getPath(dirPath);
                pathSet.add(nowDirPath);
                // 当前文件夹的deep
                try {
                    List<String> paths = Files.list(nowDirPath).map(path -> path.toString()).collect(Collectors.toList());
                    childDirs.addAll( paths );

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (childDirs.isEmpty()) {
                return pathSet;
            }
            dirPathArr = childDirs.toArray(String[]::new);
            return getDeepDirs(fileSystem, pathSet, dirPathArr);
        /*} catch (IOException e) {
            e.printStackTrace();
        }*/
    }

}
