package com.jdk14.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jdk14DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Jdk14DemoApplication.class, args);
    }

}
