package com.feign.service;

import cn.hutool.json.JSONUtil;
import com.feign.pojo.bo.PmsOutstationParam;
import com.feign.pojo.pgsql.PmsOutstation;
import com.feign.pojo.vo.Result;
import com.feign.utils.AppUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by WuTianyu on 2020/4/22 10:30.
 */
class TestFeignTest {
    @Autowired
    TestClient testClient;
    @Autowired
    LoadBalancerClient loadBalancerClient;

    @Test
    void listMapOutstationAreaLevel() {
        PmsOutstationParam outstationParam = new PmsOutstationParam();
        outstationParam.setEntityStatus(List.of("1"));
        outstationParam.setOperatingStatus(List.of("[0]正常"));

        long timestamp = 1587532615;
        String body = JSONUtil.toJsonStr(outstationParam);
        System.out.println("body = " + body);
        String appId = "8cc16a724c8046c1bc3cd8ad75b6c72e";
        String appSecret = AppUtil.getAppSecret(appId);
        String sign = AppUtil.checkSign(appId, timestamp, body);

        Result<Map<String, Map<String, List<PmsOutstation>>>> map = testClient.listMapOutstationAreaLevel(URI.create("https://enterprise.ngs1685.com"), appSecret, timestamp, "4415E88BDA07B08B184AEBABD69A04BD", body);
        System.out.println("map = " + map);
    }
}
