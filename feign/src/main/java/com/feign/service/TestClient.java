package com.feign.service;

import com.feign.pojo.pgsql.PmsOutstation;
import com.feign.pojo.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URI;
import java.util.List;
import java.util.Map;


/**
 * Created by WuTianyu on 2020/4/22 8:56.
 */
@FeignClient(name = "outstationAreaLevel", url = "ngs")   // name 表示哪个应用的接口
public interface TestClient {
    @RequestMapping(value = "/ngs/outstation/mapOutstationAreaLevel", method = RequestMethod.POST)
    Result<Map<String, Map<String, List<PmsOutstation>>>> listMapOutstationAreaLevel(@RequestParam("uri") URI uri,
                                                                                     @RequestParam("appId") String appId,
                                                                                     @RequestParam("timestamp") long timestamp,
                                                                                     @RequestParam("sign") String sign,
                                                                                     @RequestParam("body") String body);
}
