package com.feign.pojo.pgsql;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by WuTianyu on 2020/3/26 12:55.
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
//@Table(indexes = {@Index(columnList = "createTime", name = "idx_pms_sales_price_history_create_time"),
//                  @Index(columnList = "outstationNo", name = "idx_pms_sales_price_history_outstation_no")})
public class PmsOutstation implements Serializable {
    private static final long serialVersionUID = 1L;
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @Column(columnDefinition = "char(36) default gen_random_uuid()")

    private String outstationId;

    private String outstationNo;

    private String name;

    private String channel;

    private String operatingStatus;

    private LocalDateTime openDate;

    private LocalDateTime closeDate;

    private String entityStatus;

    private Double area;

    private Integer workerNum;

    private String manager;

    private String contractExpireStatus;

    private LocalDateTime contractExpireDate;

    private String manageDept;

    private String manageZone;

    private String secondaryDept;

    private String parentZone;

    private String province;

    private String city;

    private String zone;

    private String inventoryCost;

    private String inventoryDate;
}
