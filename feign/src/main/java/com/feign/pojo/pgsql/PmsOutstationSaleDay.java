package com.feign.pojo.pgsql;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by WuTianyu on 2020/3/26 12:55.
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PmsOutstationSaleDay implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    private String outstationId;

    private BigDecimal saleAmount;

    private BigDecimal saleQty;

    private Integer visitorQty;

    private BigDecimal goodsAmount;

    private BigDecimal cigaretAmount;

    private LocalDateTime saleDate;

    private String week;

    private Integer dayOfMonth;

    private Integer dayOfYear;

    private LocalDateTime yoyDate;
}
