package com.feign.pojo.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by WuTianyu on 2020/4/20 13:21.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PmsOutstationParam {
    List<String> operatingStatus;

    List<String> entityStatus;
}
