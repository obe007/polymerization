package com.feign.controller;

import cn.hutool.json.JSONUtil;
import com.feign.pojo.bo.PmsOutstationParam;
import com.feign.pojo.pgsql.PmsOutstation;
import com.feign.pojo.vo.Result;
import com.feign.service.TestClient;
import com.feign.utils.AppUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Created by WuTianyu on 2020/4/22 13:05.
 */
@RestController
//@RequestMapping()
public class ClientController {
    @Autowired
    TestClient testClient;

    @GetMapping(value = "/msg")
    public Result<Map<String, Map<String, List<PmsOutstation>>>> msg(@RequestParam("uri") String uri) {
        PmsOutstationParam outstationParam = new PmsOutstationParam();
        outstationParam.setEntityStatus(List.of("1"));
        outstationParam.setOperatingStatus(List.of("[0]正常"));

        long timestamp = 1587532615;

        String body = JSONUtil.toJsonStr(outstationParam);
        System.out.println("body = " + body);
        String appId = "8cc16a724c8046c1bc3cd8ad75b6c72e";
        String appSecret = AppUtil.getAppSecret(appId);
        String sign = AppUtil.checkSign(appId, timestamp, body);

//        Result<Map<String, Map<String, List<PmsOutstation>>>> map = testClient.listMapOutstationAreaLevel(URI.create("http://127.0.0.1:9702"), appSecret, timestamp, "5A3DFF79429E208FF4FE0ACC38AAB8CC", body);
        Result<Map<String, Map<String, List<PmsOutstation>>>> map = testClient.listMapOutstationAreaLevel(URI.create(uri), appSecret, timestamp, "5A3DFF79429E208FF4FE0ACC38AAB8CC", body);
        System.out.println("map = " + map);
        return map;
    }


}
