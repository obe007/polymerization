package com.feign.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 组件配置注册部分
 *
 * Created by WuTianyu on 2019/12/26 11:12.
 */
@Configuration
@ComponentScan(basePackages = {
        // 公用组件包
        "com.feign.config",
})
public class ComponentConfig {
}
