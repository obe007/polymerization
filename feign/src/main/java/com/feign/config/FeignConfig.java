package com.feign.config;

import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created by WuTianyu on 2020/4/22 13:02.
 *
 * feign配置
 */
@EnableFeignClients
public class FeignConfig {
}
