package com.leetcode.algorithm.algorithm;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Created by WuTianyu on 2020/4/21 14:38.
 */
class Solution {

    /**
     * 给你一个整数数组 nums 和一个整数 k。
     *
     * 如果某个 连续 子数组中恰好有 k 个奇数数字，我们就认为这个子数组是「优美子数组」。
     *
     * 请返回这个数组中「优美子数组」的数目。
     *
     *  
     *
     * 示例 1：
     *
     * 输入：nums = [1,1,2,1,1], k = 3
     * 输出：2
     * 解释：包含 3 个奇数的子数组是 [1,1,2,1] 和 [1,2,1,1] 。
     * 示例 2：
     *
     * 输入：nums = [2,4,6], k = 1
     * 输出：0
     * 解释：数列中不包含任何奇数，所以不存在优美子数组。
     * 示例 3：
     *
     * 输入：nums = [2,2,2,1,2,2,1,2,2,2], k = 2
     * 输出：16
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/count-number-of-nice-subarrays
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     * @param nums
     * @param k
     * @return
     */
    public static int numberOfSubarrays(int[] nums, int k) {
        var length = nums.length;
        var isOdd = false;
        var oddLength = 0;
        var gracefulSubarray = 0;
        for (int i = 0; i < length; i++) {
            calc: for (int j = i; j < length; j++) {
                if (oddLength >= k) {

                }
                if (nums[j] % 2 != 0) {
                    ++oddLength;
                }
            }
            if (oddLength >= k) {
                ++gracefulSubarray;
                oddLength = 0;
            }
        }
        return gracefulSubarray;
    }

    public static void main(String[] args) {
//        int[] nums = new int[]{1,1,2,1,1};
        int[] nums = new int[]{2,2,2,1,2,2,1,2,2,2};
        System.out.println(numberOfSubarrays(nums, 2));
    }
}
