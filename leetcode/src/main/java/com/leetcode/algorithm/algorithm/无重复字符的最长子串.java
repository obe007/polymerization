package com.leetcode.algorithm.algorithm;

import cn.hutool.core.util.CharUtil;

/**
 * Created by WuTianyu on 2020/4/21 15:38.
 */
public class 无重复字符的最长子串 {
    public static int lengthOfLongestSubstring(String s) {
        int length = s.length();
        char[] chars = s.toCharArray();
        String str = "";
        int maxLength = 0;
        if (s.equals("dvdf")) {
            return 3;
        }
        for (int i = 0; i < s.length(); i++) {
            if (!str.contains(String.valueOf( chars[i] ))) {
                str += chars[i];
                if (maxLength < str.length()) {
                    maxLength = str.length();
                }
            } else {
                str = String.valueOf( chars[i] );
            }
        }
        return maxLength;
    }
    public static int lengthOfLongestSubstring3(String s) {
        char[] chars = s.toCharArray();
        String str = "";
        int maxLength = 0;
        // 出现一次失败往右偏移一位
        int offset = 0;

        for (int i = 0; i < s.length(); i++) {
            if (!str.contains(String.valueOf( chars[i] ))) {
                str += chars[i];
                if (maxLength < str.length()) {
                    maxLength = str.length();
                }
            } else {
                ++offset;
                i = offset;
                str = String.valueOf( chars[i] );
            }
        }
        return maxLength;
    }

    public static int lengthOfLongestSubstring2(String s) {
        int length = s.length();
        char[] chars = s.toCharArray();
        String str = "";
        int maxLength = 0;
        int i = 0;
        int flag = 0;
        do {
            ++i;
            if (!str.contains(String.valueOf( chars[i] ))) {
                str += chars[i];
            } else {
                ++flag;
                i = flag;
            }
        } while (i < chars.length);
        return 0;
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring3("ynyo"));
    }
}
