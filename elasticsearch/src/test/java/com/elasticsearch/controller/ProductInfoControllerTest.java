package com.elasticsearch.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.elasticsearch.pojo.entity.CommodityES;
import com.elasticsearch.pojo.entity.ProductInfo;
import com.elasticsearch.repository.CommodityESRepository;
import com.elasticsearch.service.CommodityESService;
import com.elasticsearch.service.ProductService;
import com.elasticsearch.utils.ReadFileLog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductInfoControllerTest {

    @Autowired
    ProductService productService;

    @Test
    public void list() {
        List<ProductInfo> productInfos = productService.searchToList("name", "原味面包");
        productInfos.forEach(productInfo -> {
            System.out.println(productInfo);
        });
    }

    @Test
    public void testInsert() {
        ProductInfo productInfo = new ProductInfo();
        productInfo.setSkuId("1501009001");
        productInfo.setName("原味切片面包（10片装）");
        productInfo.setCategory("101");
        productInfo.setPrice(new BigDecimal(880));
        productInfo.setBrand("良品铺子");
        productInfo.setStock(100);
        productService.save(productInfo);

        productInfo = new ProductInfo();
        productInfo.setSkuId("1501009002");
        productInfo.setName("原味切片面包（6片装）");
        productInfo.setCategory("101");
        productInfo.setPrice(new BigDecimal(680));
        productInfo.setBrand("良品铺子");
        productService.save(productInfo);

        productInfo = new ProductInfo();
        productInfo.setSkuId("1501009004");
        productInfo.setName("元气吐司850g");
        productInfo.setCategory("101");
        productInfo.setPrice(new BigDecimal(120));
        productInfo.setBrand("百草味");
        productService.save(productInfo);

    }

    private String[] productName() {
        String[] productName = new String[]{
                
        };
        return productName;
    }

    @Autowired
    CommodityESService commodityESService;

    @Autowired
    CommodityESRepository commodityESRepository;

    @Resource
    ReadFileLog readFileLog;
    @Test
    public void TestInsert1000() {
        readFileLog.read().parallelStream().forEach(commodityMS -> {
            CommodityES commodityES = new CommodityES();
            BeanUtil.copyProperties( commodityMS, commodityES );
            commodityES.setId(IdUtil.fastUUID());
            commodityESRepository.save( commodityES );
        });
    }

    @Test
    public void showCommodityCount() {
        System.out.println("readFileLog.read().size() = " + readFileLog.read().size());
    }




//    private List<CommodityMS> commodityMSListAll = new ArrayList<>();
//    @Test
//    public List<CommodityMS> ReadFileLog() {
//        final String filePath = "E:\\data\\log\\commodityJson.log";
//        BufferedReader bufferedReader = FileUtil.getUtf8Reader( filePath );
//        try {
//            String commodityES = bufferedReader.readLine();
//            List<CommodityMS> commodityMSList = JSONObject.parseObject( commodityES, new TypeReference<List<CommodityMS>>(){} );
//            commodityMSListAll = commodityMSList;
//            return commodityMSList;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

}