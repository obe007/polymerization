<html>
<#include "../common/header.ftl">

<body>
<div id="wrapper" class="toggled">

    <#--边栏sidebar-->
    <#include "../common/nav.ftl">

    <#--主要内容content-->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <table class="table table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>cCode</th>
                            <th>cName</th>
                            <th>ordCycle</th>
                            <th>scale</th>
                            <th>scaleQty</th>
                            <th>isauto</th>
                            <th>unit</th>
                            <th>ip</th>
                            <th>chgTime</th>
                            <th colspan="2">操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        <#list commodityESPage.content as commodity>
                        <tr>
                            <td>${commodity.id!""}</td>
                            <td>${commodity.cCode!""}</td>
                            <td>${commodity.cName!""}</td>
                            <td>${commodity.ordCycle!""}</td>
                            <td>${commodity.scale!""}</td>
                            <td>${commodity.scaleQty!""}</td>
                            <td>${commodity.isauto!""}</td>
                            <td>${commodity.unit!""}</td>
                            <td>${commodity.ip!""}</td>
                            <td>${commodity.chgTime!""}</td>
                            <td><a href="/show_commodity/commodity_page?productId=${commodity.id}">修改</a></td>
<#--                            <td>-->
<#--                                <#if productInfo.getProductStatusEnum().message == "上架">-->
<#--                                    <a href="/sell/seller/product/off_sale?productId=${commodity.id}">下架</a>-->
<#--                                <#else>-->
<#--                                    <a href="/sell/seller/product/on_sale?productId=${commodity.id}">上架</a>-->
<#--                                </#if>-->
<#--                            </td>-->
                        </tr>
                        </#list>
                        </tbody>
                    </table>
                </div>

            <#--分页-->
                <div class="col-md-12 column">
                    <ul class="pagination pull-right">
                    <#if currentPage lte 1>
                        <li class="disabled"><a href="#">上一页</a></li>
                    <#else>
                        <li><a href="/show_commodity/commodity_page?page=${currentPage - 1}&size=${size}">上一页</a></li>
                    </#if>


<#-- 不适用大数据 -->
<#--                    <#list 1..productInfoPage.getTotalPages() as index>-->
<#--                        <#if currentPage == index>-->
<#--                            <li class="disabled"><a href="#">${index}</a></li>-->
<#--                        <#else>-->
<#--                            <li><a href="/sell/seller/product/list?page=${index}&size=${size}">${index}</a></li>-->
<#--                        </#if>-->
<#--                    </#list>-->

                    <#if commodityESPage.totalPages gt 0>
                        <#if currentPage == 1>
                            <li <#if currentPage == 1>class="disabled"></#if><a href="#">1</a></li>
                        </#if>
                    </#if>

                    <#if commodityESPage.totalPages gt 1>
                        <#if (currentPage - 3) gt 2>
                            <li><span class="text">...<span></li>
                        </#if>
                    </#if>

                    <#list (currentPage - 3)..(currentPage + 3) as index>
                        <#if (index gt 1) && (index lt commodityESPage.totalPages)>
                            <li><a href="/show_commodity/commodity_page?page=${index}&size=${size}">${index!"error"}</a></li>
                        </#if>
                    </#list>

                    <#if (currentPage + 3) lt commodityESPage.totalPages>
                        <li><span class="text">...<span></li>
                    </#if>

                    <#--最后页-->
                    <li <#if currentPage == commodityESPage.getTotalPages()>class="disabled"</#if>><a href="/show_commodity/commodity_page?page=${commodityESPage.getTotalPages()}&size=${size}" >${commodityESPage.getTotalPages()}</a></li>

                    <#if commodityESPage.getTotalPages() gt 3>
                        <li><a href="#">上一页</a></li>
                    </#if>

                    <#if currentPage == commodityESPage.getTotalPages()>
                        <li class="disabled"><a href="#">下一页</a></li>
                    <#else>
                        <li><a href="/show_commodity/commodity_page?page=${currentPage + 1}&size=${size}">下一页</a></li>
                    </#if>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>