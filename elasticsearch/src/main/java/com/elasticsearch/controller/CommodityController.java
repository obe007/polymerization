package com.elasticsearch.controller;

import com.common.pojo.vo.ResultVO;
import com.common.utils.ResultVOUtil;
import com.elasticsearch.pojo.entity.CommodityES;
import com.elasticsearch.service.CommodityESService;
import com.elasticsearch.utils.searchquery.SearchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/show_commodity")
public class CommodityController {

    @Autowired
    CommodityESService commodityESService;

    @GetMapping(value = "/commodity_page")
    public ModelAndView commodityToPage(@RequestParam(value = "page", defaultValue = "1") int pageIndex,
                                        @RequestParam(value = "size", defaultValue = "100") int size,
                                        Map<String, Object> map) {
        Pageable pageable = PageRequest.of( pageIndex - 1, size );
        Page<CommodityES> commodityESPage = commodityESService.findAll( pageable );

        map.put( "commodityESPage", commodityESPage );
        map.put( "currentPage", pageIndex );
        map.put( "size", size);
        return new ModelAndView( "commodity/list" );
    }

    @GetMapping(value = "/commodity_list")
    @ResponseBody
    public ResultVO commodityList() {
        List<CommodityES> commodityESList = commodityESService.findAll();
        return ResultVOUtil.success( commodityESList );
    }

    @GetMapping(value = "/search")
    public ModelAndView commoditySearchToPage(@RequestParam(value = "page", defaultValue = "1000") int pageIndex,
                                        @RequestParam(value = "size", defaultValue = "100") int size,
                                        @RequestParam(value = "keyWord") String keyWord,
                                        Map<String, Object> map) {
        Pageable pageable = PageRequest.of( pageIndex -1 , size );
        SearchQuery searchQuery = SearchUtil.getEntitySearchQuery( pageable, "cName", keyWord );
        Page<CommodityES> commodityESPage = commodityESService.search( searchQuery );

        map.put( "commodityESPage", commodityESPage );
        map.put( "currentPage", pageIndex );
        map.put( "size", size);
        return new ModelAndView( "commodity/list" );
    }

    @GetMapping(value = "/search_page")
    public ModelAndView searchToHighlightPage(@RequestParam(value = "page", defaultValue = "1") int pageIndex,
                                              @RequestParam(value = "size", defaultValue = "500") int size,
                                              @RequestParam(value = "keyWord") String keyWord,
                                              Map<String, Object> map) {
        if (pageIndex < 1) {
            pageIndex = 1;
        }
        Pageable pageable = PageRequest.of( pageIndex -1 , size );
        Page<CommodityES> commodityESPage = commodityESService.searchCommodity( pageable, keyWord );

        map.put( "commodityESPage", commodityESPage );
        map.put( "currentPage", pageIndex );
        map.put( "size", size);
        return new ModelAndView( "commodity/list" );
    }
}
