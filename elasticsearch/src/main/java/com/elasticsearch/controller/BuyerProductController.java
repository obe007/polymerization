package com.elasticsearch.controller;

import com.common.pojo.vo.ResultVO;
import com.common.utils.ResultVOUtil;
import com.elasticsearch.pojo.entity.CommodityES;
import com.elasticsearch.pojo.entity.ProductInfo;
import com.elasticsearch.service.CommodityESService;
import com.elasticsearch.service.ProductService;
import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/show")
public class BuyerProductController {

    @Autowired
    ProductService productService;

    @Autowired
    CommodityESService commodityESService;

    @GetMapping(value = "/product_page")
    public ModelAndView list(Model model,
                             @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
                             @RequestParam(value = "size", defaultValue = "10") int size) {
        String searchContent = "商品"; // 查询条件，暂未使用
        SearchQuery searchQuery = getEntitySearchQuery(pageIndex, size, searchContent);
        Page<ProductInfo> productInfoPage = productService.search( searchQuery );
        model.addAttribute( "page", productInfoPage );
        return new ModelAndView();
    }



    @GetMapping(value = "/product_list")
    @ResponseBody
    public ResultVO productList() {
        List<ProductInfo> productInfos = productService.selectAll();
        return ResultVOUtil.success(productInfos );
    }

    private SearchQuery getEntitySearchQuery(int pageIndex, int size, String searchContent) {
        QueryBuilder queryBuilder = new MatchQueryBuilder("name", searchContent);
        FunctionScoreQueryBuilder builder = QueryBuilders
                .functionScoreQuery(
                        QueryBuilders.matchAllQuery(),  //查询全部  也可以用上面的 queryBuilder 查询某个条件
                        ScoreFunctionBuilders.weightFactorFunction(100)
                )
                /** 设置权重分，求和模式 */
                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                /** 设置权重最低分 */
                /** 设置权重分最低分 */
                .setMinScore(10);

        /** 设置分页 */
        Pageable pageable = PageRequest.of(pageIndex, size, Sort.by(Sort.Direction.DESC, "skuId"));
        return new NativeSearchQueryBuilder()
                .withQuery(builder)
                .withPageable(pageable)
                .build();
    }


}
