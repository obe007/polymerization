package com.elasticsearch.repository;

import com.elasticsearch.pojo.entity.ProductInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductInfoRepository extends ElasticsearchRepository<ProductInfo, String> {

    List<ProductInfo> findByName(String name);
}
