package com.elasticsearch.repository;

import com.elasticsearch.pojo.entity.CommodityES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommodityESRepository extends ElasticsearchRepository<CommodityES, String> {
    List<CommodityES> findAll();
}
