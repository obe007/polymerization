package com.elasticsearch.utils;

import cn.hutool.core.util.StrUtil;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.lang.reflect.Method;
import java.util.Map;

public class Highlight {
    @Autowired
    private ElasticsearchTemplate template;

    public static String parSetName(String fieldName) {
        if (null == fieldName || "".equals(fieldName)) {
            return null;
        }
        int startIndex = 0;
        if (fieldName.charAt(0) == '_')
            startIndex = 1;
        return "set" + fieldName.substring(startIndex, startIndex + 1).toUpperCase()
                + fieldName.substring(startIndex + 1);
    }

    /** 创建高亮字段数组 -> searchQuery */
    public static HighlightBuilder.Field[] getHighlightFields(String[] fieldNames) {
        HighlightBuilder.Field[] fields = new HighlightBuilder.Field[fieldNames.length];
        for (int i = 0; i < fieldNames.length; i++) {
            fields[i] = new HighlightBuilder.Field(fieldNames[i]);
        }
        return fields;
    }

    /**
     * 设置高亮
     * @param hit 命中记录
     * @param filed 字段
     * @param object 待赋值对象
     */
    public static void setHighLight(SearchHit hit, String filed, Object object){
        //获取对应的高亮域
        Map<String, HighlightField> highlightFields = hit.getHighlightFields();
        HighlightField highlightField = highlightFields.get(filed);
        if (highlightField != null){
            // 取得定义的高亮标签
            String highLightMessage = highlightField.fragments()[0].toString();
            // 反射调用set方法将高亮内容设置进去
            try {
                String setMethodName = parSetMethodName(filed);
                Class<?> Clazz = object.getClass();
                Method setMethod = Clazz.getMethod(setMethodName, String.class);
                setMethod.invoke(object, highLightMessage);
            } catch (Exception e) {
                System.out.println("反射报错: " + e + " : " + e.getMessage().toUpperCase() + " : " + e.getStackTrace().toString());
            }
        }
    }

    /**
     * 根据字段名，获取Set方法名
     * @param fieldName 字段名
     * @return  Set方法名
     */
    public static String parSetMethodName(String fieldName){
        if (StrUtil.isBlank(fieldName)){
            return null;
        }
        int startIndex = 0;
        if (fieldName.charAt(0) == '_'){
            startIndex = 1;
        }

        /** set方法 -> 字符拼接 */
        String setMethod = new String();
        Character char0 = fieldName.charAt(0);
        Character char1 = fieldName.charAt(1);
        if ( char0 != null && char1 != null ) {
            if (Character.isLowerCase(char0) && Character.isUpperCase(char1)) {
                return "set" + fieldName;
            }
        }
        return "set" + fieldName.substring(startIndex, startIndex + 1).toUpperCase()
                + fieldName.substring(startIndex + 1);
    }


}
