package com.elasticsearch.utils;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.elasticsearch.pojo.dto.CommodityMS;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

@Component
public class ReadFileLog {

    @Bean
    public List<CommodityMS> read() {
        final String filePath = "E:\\data\\log\\commodityJson.log";
        BufferedReader bufferedReader = FileUtil.getUtf8Reader( filePath );
        try {
            String commodityES = bufferedReader.readLine();
            List<CommodityMS> commodityMSList = JSONObject.parseObject( commodityES, new TypeReference<List<CommodityMS>>(){} );
            return commodityMSList;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
