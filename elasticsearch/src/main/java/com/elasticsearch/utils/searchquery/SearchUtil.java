package com.elasticsearch.utils.searchquery;

import org.elasticsearch.common.lucene.search.function.FunctionScoreQuery;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Component;

@Component
public class SearchUtil {

    public static SearchQuery getEntitySearchQuery(int pageIndex,
                                             int size,
                                             String fieldName,
                                             String searchContent) {
        return getEntitySearchQuery(pageIndex, size, Sort.unsorted(), fieldName, searchContent);
    }

    public static SearchQuery getEntitySearchQuery(Pageable pageable,
                                                   String fieldName,
                                                   String searchContent) {
        QueryBuilder queryBuilder = new MatchQueryBuilder(fieldName, searchContent);
        /** 设置查询的详细权重 命中分值 */
        FunctionScoreQueryBuilder scoreQueryBuilder = QueryBuilders
                .functionScoreQuery(
                        queryBuilder,
                        ScoreFunctionBuilders.weightFactorFunction(100)     //应该是权重最高多少分，不知道什么意思

                )
                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                /** 设置最低权重分值 */
                .setMinScore(10);

        return new NativeSearchQueryBuilder()
                .withQuery( scoreQueryBuilder )
                .withPageable( pageable )
                .build();
    }

    public static SearchQuery getEntitySearchQuery(int pageIndex,
                                             int size, Sort sort,
                                             String fieldName,
                                             String searchContent) {
        QueryBuilder queryBuilder = new MatchQueryBuilder(fieldName, searchContent);
        /** 设置查询的详细权重 命中分值 */
        FunctionScoreQueryBuilder scoreQueryBuilder = QueryBuilders
                .functionScoreQuery(
                        queryBuilder,
                        ScoreFunctionBuilders.weightFactorFunction(100)     //应该是权重最高多少分，不知道什么意思

                )
                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                /** 设置最低权重分值 */
                .setMinScore(10);

        Pageable pageable = PageRequest.of(pageIndex - 1, size, sort);

        return new NativeSearchQueryBuilder()
                .withQuery(scoreQueryBuilder)
                .withPageable(pageable)
                .build();
    }

    public static SearchQuery getEntitySearchQuery(String fieldName,
                                             String searchContent) {
        QueryBuilder queryBuilder = new MatchQueryBuilder(fieldName, searchContent);
        /** 设置查询的详细权重 命中分值 */
        FunctionScoreQueryBuilder scoreQueryBuilder = QueryBuilders
                .functionScoreQuery(
                        queryBuilder,
                        ScoreFunctionBuilders.weightFactorFunction(100)     //应该是权重最高多少分，不知道什么意思

                )
                .scoreMode(FunctionScoreQuery.ScoreMode.SUM)
                /** 设置最低权重分值 */
                .setMinScore(10);

        return new NativeSearchQueryBuilder()
                .withQuery(scoreQueryBuilder)
                .build();
    }



}
