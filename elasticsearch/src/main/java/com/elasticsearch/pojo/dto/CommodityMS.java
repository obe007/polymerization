package com.elasticsearch.pojo.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CommodityMS {

    /** 货号 */
    private String cCode;

    /** 货名 */
    private String cName;

    /** 货名 */
    private String ordCycle;

    /**  */
    private String scale;

    private BigDecimal scaleQty;

    private String isauto;

    /** 量词 */
    private String unit;

    /** IP地址 */
    private String ip;

    private Date chgTime;

    public CommodityMS() {
    }

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getOrdCycle() {
        return ordCycle;
    }

    public void setOrdCycle(String ordCycle) {
        this.ordCycle = ordCycle;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public BigDecimal getScaleQty() {
        return scaleQty;
    }

    public void setScaleQty(BigDecimal scaleQty) {
        this.scaleQty = scaleQty;
    }

    public String getIsauto() {
        return isauto;
    }

    public void setIsauto(String isauto) {
        this.isauto = isauto;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getChgTime() {
        return chgTime;
    }

    public void setChgTime(Date chgTime) {
        this.chgTime = chgTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommodityMS)) return false;

        CommodityMS that = (CommodityMS) o;

        if (getcCode() != null ? !getcCode().equals(that.getcCode()) : that.getcCode() != null) return false;
        if (getcName() != null ? !getcName().equals(that.getcName()) : that.getcName() != null) return false;
        if (getOrdCycle() != null ? !getOrdCycle().equals(that.getOrdCycle()) : that.getOrdCycle() != null)
            return false;
        if (getScale() != null ? !getScale().equals(that.getScale()) : that.getScale() != null) return false;
        if (getScaleQty() != null ? !getScaleQty().equals(that.getScaleQty()) : that.getScaleQty() != null)
            return false;
        if (getIsauto() != null ? !getIsauto().equals(that.getIsauto()) : that.getIsauto() != null) return false;
        if (getUnit() != null ? !getUnit().equals(that.getUnit()) : that.getUnit() != null) return false;
        if (getIp() != null ? !getIp().equals(that.getIp()) : that.getIp() != null) return false;
        return getChgTime() != null ? getChgTime().equals(that.getChgTime()) : that.getChgTime() == null;
    }

    @Override
    public int hashCode() {
        int result = getcCode() != null ? getcCode().hashCode() : 0;
        result = 31 * result + (getcName() != null ? getcName().hashCode() : 0);
        result = 31 * result + (getOrdCycle() != null ? getOrdCycle().hashCode() : 0);
        result = 31 * result + (getScale() != null ? getScale().hashCode() : 0);
        result = 31 * result + (getScaleQty() != null ? getScaleQty().hashCode() : 0);
        result = 31 * result + (getIsauto() != null ? getIsauto().hashCode() : 0);
        result = 31 * result + (getUnit() != null ? getUnit().hashCode() : 0);
        result = 31 * result + (getIp() != null ? getIp().hashCode() : 0);
        result = 31 * result + (getChgTime() != null ? getChgTime().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CommodityMS{" +
                "cCode='" + cCode + '\'' +
                ", cName='" + cName + '\'' +
                ", ordCycle='" + ordCycle + '\'' +
                ", scale='" + scale + '\'' +
                ", scaleQty=" + scaleQty +
                ", isauto='" + isauto + '\'' +
                ", unit='" + unit + '\'' +
                ", ip='" + ip + '\'' +
                ", chgTime=" + chgTime +
                '}';
    }
}
