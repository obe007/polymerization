package com.elasticsearch.common.constant;

public interface EsConstant {

    /**
     * 高亮显示 - 开始标签
     */
    String HIGH_LIGHT_START_TAG = "<font color='#c00'>";

    /**
     * 高亮显示 - 结束标签
     */
    String HIGH_LIGHT_END_TAG = "</font>";

    /** 搜索词 最大长度 */
    int KEY_WORD_MAX_LENGTH = 20;

    /**
     * 索引名称
     */
    class INDEX_NAME {
        /**
         * 游记
         */
        public static final String TRAVEL = "commodity_es";
    }
}
