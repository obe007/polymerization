package com.elasticsearch.service.impl;

import cn.hutool.core.date.DateUtil;
import com.elasticsearch.common.constant.EsConstant;
import com.elasticsearch.pojo.entity.CommodityES;
import com.elasticsearch.repository.CommodityESRepository;
import com.elasticsearch.service.CommodityESService;
import com.elasticsearch.utils.Highlight;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder.Field;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
public class CommodityESServiceImpl implements CommodityESService {
    @Autowired
    CommodityESRepository commodityESRepository;
    @Autowired
    ElasticsearchTemplate elasticsearchTemplate;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveList(List<CommodityES> commodityESList) {
        commodityESList.parallelStream().forEach(commodityES -> {
            CommodityES result = commodityESRepository.save( commodityES );
            if (result == null) {
                return;
            }
        });
        return true;
    }

    @Override
    public List<CommodityES> findAll() {
        return commodityESRepository.findAll();
    }

    @Override
    public Page<CommodityES> findAll(Pageable pageable) {
        return commodityESRepository.findAll( pageable );
    }

    @Override
    public Page<CommodityES> search(SearchQuery searchQuery) {
        return commodityESRepository.search( searchQuery );
    }

    @Override
    public Page<CommodityES> searchAndHighlight(Pageable pageable, String searchContent, String... fieldName) {
        // 构建查询
        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();

        // 多索引查询
        searchQuery.withIndices(EsConstant.INDEX_NAME.TRAVEL);

        // 组合查询，boost即为权重，数值越大，权重越大
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .should(QueryBuilders.multiMatchQuery(searchContent, fieldName).boost(3));
        searchQuery.withQuery(queryBuilder);

        // 高亮设置
        List<String> highlightFields = Arrays.asList(fieldName);
        Field[] fields = new Field[highlightFields.size()];
        for (int i = 0; i < highlightFields.size(); i++) {
            fields[i] = new HighlightBuilder.Field(highlightFields.get(i)).preTags(EsConstant.HIGH_LIGHT_START_TAG)
                    .postTags(EsConstant.HIGH_LIGHT_END_TAG);
        }
        searchQuery.withHighlightFields(fields);

        // 分页设置
        searchQuery.withPageable(pageable);

        Page<CommodityES> page = null;
        page = elasticsearchTemplate.queryForPage(searchQuery.build(), CommodityES.class, new SearchResultMapper() {

            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> aClass, Pageable pageable) {
                // 获取高亮搜索数据
                List<CommodityES> list = new ArrayList<>();

                for (SearchHit searchHit : response.getHits()) {
                    if (response.getHits().getHits().length <= 0) {
                        return null;
                    }

                    // 公共字段
                    CommodityES data = new CommodityES();
                    Object id = searchHit.getSourceAsMap().get("id");
                    Object cCode = searchHit.getSourceAsMap().get("cCode");
                    Object cName = searchHit.getSourceAsMap().get("cName");
                    Object ordCycle = searchHit.getSourceAsMap().get("ordCycle");
                    Object scale = searchHit.getSourceAsMap().get("scale");
                    Object scaleQty = searchHit.getSourceAsMap().get("scaleQty");
                    Object isauto = searchHit.getSourceAsMap().get("isauto");
                    Object unit = searchHit.getSourceAsMap().get("unit");
                    Object chgTime = searchHit.getSourceAsMap().get("chgTime");
                    Object ip = searchHit.getSourceAsMap().get("ip");
                    if (id != null) {
                        data.setId( id.toString() );
                    }
                    if (cCode != null) {
                        data.setcCode( cCode.toString() );
                    }
                    if (cName != null) {
                        data.setcName( cName.toString() );
                    }
                    if (ordCycle != null) {
                        data.setOrdCycle( ordCycle.toString() );
                    }
                    if (scale != null) {
                        data.setScale( scale.toString() );
                    }
                    if (scaleQty != null) {
                        data.setScaleQty( new BigDecimal(scaleQty.toString()) );
                    }
                    if (isauto != null) {
                        data.setIsauto( isauto.toString() );
                    }
                    if (unit != null) {
                        data.setUnit( searchHit.getSourceAsMap().get("unit").toString() );
                    }
                    if (chgTime != null) {
                        data.setChgTime( DateUtil.parseDate(chgTime.toString()) );
                    }
                    if (ip != null) {
                        data.setIp( ip.toString() );
                    }

                    try {
                        for (String field : highlightFields) {
                            HighlightField highlightField = searchHit.getHighlightFields().get(field);
                            if (highlightField != null) {
                                String setMethodName = Highlight.parSetName(field);
                                Class<? extends CommodityES> poemClazz = data.getClass();
                                Method setMethod = poemClazz.getMethod(setMethodName, String.class);

                                String highlightStr = highlightField.fragments()[0].toString();
                                // 截取字符串
//                                if ("content".equals(field) && highlightStr.length() > 50) {
//                                    highlightStr = StringUtils.truncated(highlightStr,
//                                            EsConstant.HIGH_LIGHT_START_TAG, EsConstant.HIGH_LIGHT_END_TAG);
//                                }
                                setMethod.invoke(data, highlightStr);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("e = " + e.getMessage());
                    }
                    list.add( data );
                }

                if (list.size() > 0) {
                    AggregatedPage<T> result = new AggregatedPageImpl<T>((List<T>) list, pageable,
                            response.getHits().getTotalHits());
                    return result;
                }
                return null;
            }


        });
        return page;
    }

    @Override
    public Page<CommodityES> searchCommodity(Pageable pageable, String keyWord) {
        if (keyWord.length() > EsConstant.KEY_WORD_MAX_LENGTH) {
            keyWord = keyWord.substring(0 , EsConstant.KEY_WORD_MAX_LENGTH);
        }

        return highLightQuery(keyWord, pageable);
    }

    private static final String C_NAME = "cName";
    private static final String UNIT = "unit";
    private static final String MUTI_BOOST = '^' + "3.0";
    private static final String[] fieldArr = {C_NAME, UNIT};
    /**
     * 高亮查询
     * @param keyWord 查询关键词
     * @param pageable
     * @return
     */
    private Page<CommodityES> highLightQuery(String keyWord, Pageable pageable) {

        /** 1.创建QueryBuilder(即设置查询条件)这儿创建的是组合查询(也叫多条件查询),后面会介绍更多的查询方法
         *  组合查询BoolQueryBuilder
         *  must(QueryBuilders)     :AND
         *  mustNot(QueryBuilders)  :NOT
         *  should:                 :OR
         *
         *  boost 权重值， 大 -> 字段搜索靠前
         */
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.should(QueryBuilders.multiMatchQuery(keyWord, fieldArr));



        //构建查询
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                //设置高亮字段
                .withHighlightFields(Highlight.getHighlightFields( fieldArr ))
                .withPageable(pageable)
                .build();

        //实施查询，注意：这里的泛型最后和 elasticsearch 中的字段对应
        AggregatedPage<CommodityES> commodityPage = elasticsearchTemplate.queryForPage(searchQuery, CommodityES.class, new SearchResultMapper() {
            @Override
            public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
                List<CommodityES> commodityESList = new ArrayList<>();
                //命中记录
                SearchHits hits = response.getHits();
                for (SearchHit hit : hits){
                    if (hits.totalHits <= 0){
                        return null;
                    }
                    CommodityES commodity = new CommodityES();
                    Object id = hit.getSourceAsMap().get("id");
                    Object cCode = hit.getSourceAsMap().get("cCode");
                    Object cName = hit.getSourceAsMap().get("cName");
                    Object ordCycle = hit.getSourceAsMap().get("ordCycle");
                    Object scale = hit.getSourceAsMap().get("scale");
                    Object scaleQty = hit.getSourceAsMap().get("scaleQty");
                    Object isauto = hit.getSourceAsMap().get("isauto");
                    Object unit = hit.getSourceAsMap().get("unit");
                    Object chgTime = hit.getSourceAsMap().get("chgTime");
                    Object ip = hit.getSourceAsMap().get("ip");
                    if (id != null) {
                        commodity.setId( id.toString() );
                    }
                    if (cCode != null) {
                        commodity.setcCode( cCode.toString() );
                    }
                    if (cName != null) {
                        commodity.setcName( cName.toString() );
                    }
                    if (ordCycle != null) {
                        commodity.setOrdCycle( ordCycle.toString() );
                    }
                    if (scale != null) {
                        commodity.setScale( scale.toString() );
                    }
                    if (scaleQty != null) {
                        commodity.setScaleQty( new BigDecimal(scaleQty.toString()) );
                    }
                    if (isauto != null) {
                        commodity.setIsauto( isauto.toString() );
                    }
                    if (unit != null) {
                        commodity.setUnit( unit.toString() );
                    }
                    if (chgTime != null) {
                        commodity.setChgTime( DateUtil.parseDate(chgTime.toString()) );
                    }
                    if (ip != null) {
                        commodity.setIp( ip.toString() );
                    }

                    //设置高亮（若对应字段有高亮的话）
                    for (int i = 0; i < fieldArr.length; i++) {
                        Highlight.setHighLight(hit, fieldArr[i], commodity);
                    }
                    commodityESList.add(commodity);
                }
                return new AggregatedPageImpl<>((List<T>)commodityESList);
            }
        });
        /** 返回Page */
        return commodityPage;
    }
}
