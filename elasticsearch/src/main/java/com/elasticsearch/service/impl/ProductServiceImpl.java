package com.elasticsearch.service.impl;

import com.common.utils.converter.Page2ListConverter;
import com.elasticsearch.pojo.entity.ProductInfo;
import com.elasticsearch.repository.ProductInfoRepository;
import com.elasticsearch.service.ProductService;
import com.elasticsearch.utils.searchquery.SearchUtil;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductInfoRepository productInfoRepository;

    @Override
    public long count() {

        return productInfoRepository.count();
    }

    @Override
    @Transactional
    public ProductInfo save(ProductInfo productInfo) {
        return productInfoRepository.save(productInfo);
    }

    @Override
    public void delete(ProductInfo productInfo) {
        productInfoRepository.delete(productInfo);
    }

    @Override
    public List<ProductInfo> selectAll() {
        List<ProductInfo> productInfoList = new LinkedList<>();
        productInfoRepository.findAll().forEach(productInfoItem -> {
            productInfoList.add(productInfoItem);
        });
        return productInfoList;
    }


    /** 使用MatchQueryBuilder方法 */
    public List<ProductInfo> selectByName(String name) {
        List<ProductInfo> productInfoList = new LinkedList<>();
        /** ES 带的搜索方式，自动转换小写 并且ES将分词搜索 */
        MatchQueryBuilder matchQueryBuilder = new MatchQueryBuilder("name", name);
        Iterable<ProductInfo> commodityIterable = productInfoRepository.search( matchQueryBuilder );
        commodityIterable.forEach(productInfoItem -> {
            productInfoList.add(productInfoItem);
        });
        return productInfoList;
    }

    @Override
    public Page<ProductInfo> pageQuery(Integer pageIndex, Integer pageSize, String kw) {
        Pageable pageable = PageRequest.of(pageIndex - 1, pageSize);
        return productInfoRepository.findAll( pageable );
    }

    @Override
    public Page<ProductInfo> search(SearchQuery searchQuery) {
        return productInfoRepository.search( searchQuery );
    }

    @Override
    public Page<ProductInfo> searchAndPage(int pageIndex, int size, Sort sort, String fieldName, String searchContent) {
        SearchQuery searchQuery = SearchUtil.getEntitySearchQuery(pageIndex, size, sort, fieldName, searchContent);
        return productInfoRepository.search(searchQuery);
    }

    @Override
    public List<ProductInfo> searchToList(String fieldName, String searchContent) {
        SearchQuery searchQuery = SearchUtil.getEntitySearchQuery(fieldName, searchContent);

        Page<ProductInfo> page = productInfoRepository.search(searchQuery);
        return Page2ListConverter.convert(page);
    }


}
