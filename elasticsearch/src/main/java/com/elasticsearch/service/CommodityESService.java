package com.elasticsearch.service;

import com.elasticsearch.pojo.entity.CommodityES;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.util.List;

public interface CommodityESService {
    boolean saveList(List<CommodityES> commodityESList);

    List<CommodityES> findAll();

    Page<CommodityES> findAll(Pageable pageable);

    Page<CommodityES> search(SearchQuery searchQuery);

    Page<CommodityES> searchAndHighlight(Pageable pageable, String searchContent, String... fieldName);

    Page<CommodityES> searchCommodity(Pageable pageable, String keyWord);
}
