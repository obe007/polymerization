package com.elasticsearch.service;

import com.elasticsearch.pojo.entity.ProductInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import java.util.List;

public interface ProductService {

    long count();

    ProductInfo save(ProductInfo productInfo);

    void delete(ProductInfo productInfo);

    List<ProductInfo> selectAll();

    List<ProductInfo> selectByName(String name);

    Page<ProductInfo> pageQuery(Integer pageIndex, Integer pageSize, String kw);

    Page<ProductInfo> search(SearchQuery searchQuery);

    Page<ProductInfo> searchAndPage(int pageIndex, int size, Sort sort, String fieldName, String searchContent);

    List<ProductInfo> searchToList(String fieldName, String searchContent);
}
