//package com.jdbc.utils;
//
//import org.apache.commons.dbcp2.BasicDataSource;
//
//import java.sql.Connection;
//import java.util.Properties;
//
//public class JdbcUtils {
//    private static final ThreadLocal<Connection> CONNECTION_HOLDER;
//
//    private static final BasicDataSource DATA_SOURCE = new BasicDataSource();
//
//    private JdbcUtils() {
//    }
//
//    static {
//        CONNECTION_HOLDER = new ThreadLocal<Connection>();
//        Properties conf = loadProps("dbconfig.properties");
//        String driverClass = conf.getProperty("jdbc.driverClass");
//        String url = conf.getProperty("jdbc.url");
//        String username = conf.getProperty("jdbc.username");
//        String password = conf.getProperty("jdbc.password");
//        DATA_SOURCE.setDriverClassName(driverClass);
//        DATA_SOURCE.setUrl(url);
//        DATA_SOURCE.setUsername(username);
//        DATA_SOURCE.setPassword(password);
//}
