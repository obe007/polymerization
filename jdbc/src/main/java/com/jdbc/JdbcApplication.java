package com.jdbc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@SpringBootApplication
public class JdbcApplication implements CommandLineRunner {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(JdbcApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        showConnection();
        showData();
    }

    public void showData() {

        Integer storeId = 11;
        List<String> storeIds = Collections.synchronizedList(new ArrayList<>());
        for (Integer i = 12; i < 654; i++) {
            storeIds.add(i.toString());
        }

        storeIds.parallelStream().forEach( id -> {
            String sql = "insert into `auto_replenishment`.`user_store`(`user_id`, `store_id`) values (36, '" + id + "')";
            log.info("sql = {}", sql);
            jdbcTemplate.execute( sql );
        });

//        jdbcTemplate.execute( sql );
    }

    private void showConnection() throws SQLException {
        log.info("dataSource = {}", dataSource.toString());
        Connection conn = dataSource.getConnection();
        log.info("connection = {}", conn.toString());
        conn.close();
    }


}
