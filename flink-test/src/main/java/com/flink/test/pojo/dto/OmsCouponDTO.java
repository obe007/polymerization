package com.flink.test.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * Created by WuTianyu on 2020/4/27 12:49.
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OmsCouponDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("核销券码")
    private String couponId;

    @ApiModelProperty("交易流水号或订单号")
    private String tradeNo;

    @ApiModelProperty("订单总金额")
    private String totalAmount;

    @ApiModelProperty("优惠金额")
    private String discountAmount;

    @ApiModelProperty("支付时间；格式: yyyy-MM-dd HH:mm:ss")
    private String transTime;
}
