package com.flink.test.pojo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by WuTianyu on 2020/4/27 12:49.
 *
 * 优惠券核销明细记录, 明细最多只能上传100条
 * couponList
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OmsWriteoffCouponDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("优惠券核销明细-集合")
    List<OmsCouponDTO> couponList;
}
