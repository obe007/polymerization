package com.flink.test.pojo.vo;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by WuTianyu on 2020/3/12 12:55.
 *
 * 订单核销 传出
 */
@Data
@ApiModel(value="订单核销", description="订单核销-传出")
public class OmsOrderWriteoffVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("调用方流水号; 唯一")
    @NotNull
    private String transactionId;

    @ApiModelProperty("渠道订单号")
    private String channelOrderId;

    @ApiModelProperty("商家自由门店编码")
    @NotNull
    private String outStationNo;

    @ApiModelProperty("是否退货;默认false")
    private Boolean refund;

    @ApiModelProperty("会员卡号,用于积分")
    private String vipCode;

    @ApiModelProperty("区分调用渠道，1->京东到家,2->美团，3->饿了么")
    private String channel;

    @ApiModelProperty("订单创建的时间")
    private LocalDateTime createTime;

    @ApiModelProperty("订单中所包含的项目商品详情")
    private List<OmsOrderItemBO> productList;

//    @ApiModelProperty("门店是否已核销: 0->未核销；1->已核销")
//    private String writeoffStatus;
}
