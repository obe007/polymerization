package com.flink.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlinkTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(FlinkTestApplication.class, args);
    }

}
