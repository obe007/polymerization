package com.rocket.mq.controller;

import cn.hutool.core.util.IdUtil;
import com.rocket.mq.pojo.pgsql.UmsPraiseRec;
import com.rocket.mq.service.test.Producer;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * Created by WuTianyu on 2020/5/6 10:52.
 */
@RestController
@RequestMapping("/message")
public class MsgController {
    @Resource
    RocketMQTemplate rocketMQTemplate;

    @GetMapping("/praise")
    public UmsPraiseRec praise(Long uid, Long liveId /*, UmsPraiseRec rec*/) {
        UmsPraiseRec rec = UmsPraiseRec.builder()
                .id(IdUtil.fastSimpleUUID())
                .uid(uid)
                .liveId(System.currentTimeMillis() % 10 / 1000)
                .createTime(LocalDateTime.now()).build();

        rocketMQTemplate.sendOneWay(Producer.RocketConst.topic, MessageBuilder.withPayload( rec ).build());
        return rec;
    }

    /*@PostMapping("/praise")
    public String praise(@RequestBody UmsPraiseRec rec) {
        rocketMQTemplate.sendOneWay(RocketConstant.Topic.PRAISE_TOPIC, MessageBuilder.withPayload(rec).build());
        return "00000";
    }*/
}
