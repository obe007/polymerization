package com.rocket.mq.pojo.pgsql;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by WuTianyu on 2020/5/6 10:45.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
// 添加普通索引
@Table(indexes = {@Index(columnList = "uid", name = "idx_ums_praise_rec_uid")})
public class UmsPraiseRec implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "char(36) default gen_random_uuid()")
    private String id;

    private Long uid;

    private Long liveId;

    @CreationTimestamp
    private LocalDateTime createTime;

    public UmsPraiseRec(Long uid, Long liveId) {
        this.uid = uid;
        this.liveId = liveId;
    }

    public UmsPraiseRec(String id, LocalDateTime createTime) {
        this.id = id;
        this.createTime = createTime;
    }
}
