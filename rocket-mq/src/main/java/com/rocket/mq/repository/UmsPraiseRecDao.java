package com.rocket.mq.repository;

import com.rocket.mq.pojo.pgsql.UmsPraiseRec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by WuTianyu on 2020/5/6 13:07.
 */
@Repository
public interface UmsPraiseRecDao extends JpaRepository<UmsPraiseRec, String> {
}
