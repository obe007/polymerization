package com.rocket.mq.service.test;

import com.rocket.mq.common.RocketConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class TransProducer {
    public static void main(String[] args) {
        try {
            TransactionMQProducer producer = new TransactionMQProducer(RocketConst.ProducerGroup.PRAISE_CONSUMER);
            ExecutorService executorService = new ThreadPoolExecutor(2, 5, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(2000), r -> {
                    Thread thread = new Thread(r);
                    thread.setName(RocketConst.ProducerGroup.PRAISE_CONSUMER + "-check-thread");
                    return thread;
                });
            producer.setNamesrvAddr(RocketConst.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
            producer.setExecutorService(executorService);
            // 1：异步的执行本地事务, 2：做回查
            TransactionListener transactionListener = new TransactionListener() {
                /**
                 * When send transactional prepare(half) message succeed, this method will be invoked to execute local transaction.
                 *
                 * @param msg Half(prepare) message
                 * @param arg Custom business parameter
                 * @return Transaction state
                 */
                @Override
                public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                    try {
                        log.info("【执行本地事务】【异步】 msg = {}", new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET));
                        String callArg = (String)arg;
                        System.out.println("callArg = " + callArg);

                        // 数据库的落库操作
                        //tx.commit
                        return LocalTransactionState.COMMIT_MESSAGE;
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        log.error("【失败】");
                    }
                    return LocalTransactionState.COMMIT_MESSAGE;
                }
                /**
                 * When no response to prepare(half) message. broker will send check message to check the transaction status, and this
                 * method will be invoked to get local transaction status.
                 *
                 * @param msg Check message
                 * @return Transaction state
                 */
                @Override
                public LocalTransactionState checkLocalTransaction(MessageExt msg) {
                    try {
                        log.info("【回调消息检查】msg = ", new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET));
                        return LocalTransactionState.COMMIT_MESSAGE;
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        log.error("【失败】");
                    }
                    return LocalTransactionState.COMMIT_MESSAGE;
                }
            };
            producer.setTransactionListener(transactionListener);
            producer.start();
            Message msg = new Message(RocketConst.Topic.TOPIC_TEST, RocketConst.Tag.TAG_A, "key", ("rocketMQ分布式事务消息").getBytes(RemotingHelper.DEFAULT_CHARSET));
            // arg : 可以存放当前类的一些属性，进行本地事务和回调都能拿到这个参数
            producer.sendMessageInTransaction(msg, "我是回调参数");
            log.info("发送的消息 = {}", msg);
            Thread.sleep(Integer.MAX_VALUE);
            producer.shutdown();
        } catch (MQClientException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

//    /**
//     * 1：异步的执行本地事务, 2：做回查
//     */
//    static class TransactionListenerImpl implements TransactionListener {
//        /**
//         * When send transactional prepare(half) message succeed, this method will be invoked to execute local transaction.
//         *
//         * @param msg Half(prepare) message
//         * @param arg Custom business parameter
//         * @return Transaction state
//         */
//        @Override
//        @Transactional
//        public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
//            log.info("【执行本地事务】【异步】");
//            String callArg = (String)arg;
//            System.out.println("callArg = " + callArg);
//
//            // 数据库的落库操作
//            //tx.commit
//            return LocalTransactionState.COMMIT_MESSAGE;
//        }
//
//        /**
//         * When no response to prepare(half) message. broker will send check message to check the transaction status, and this
//         * method will be invoked to get local transaction status.
//         *
//         * @param msg Check message
//         * @return Transaction state
//         */
//        @Override
//        public LocalTransactionState checkLocalTransaction(MessageExt msg) {
//            log.info("【回调消息检查】");
//            return LocalTransactionState.COMMIT_MESSAGE;
//        }
//    }
}
