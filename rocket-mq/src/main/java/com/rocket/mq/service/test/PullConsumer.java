package com.rocket.mq.service.test;

import cn.hutool.core.collection.CollUtil;
import com.rocket.mq.common.RocketConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultLitePullConsumer;
import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.PullResult;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.*;

@Slf4j
public class PullConsumer {
    // key为指定队列，value为最后位置
    public static final Map<MessageQueue, Long> offsetTable = new HashMap<>();

    public static void main(String[] args) {
        // 消费者组名
        DefaultMQPullConsumer consumer = new DefaultMQPullConsumer(RocketConstant.ConsumerGroup.ConsumerGroupName1);
        // nameserver
        consumer.setNamesrvAddr(RocketConstant.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
        try {
            consumer.start();
            log.info("pull consumer starting...");

            // 从指定Topic主题获取所有队列（默认四个队列）
            Collection<MessageQueue> mqs = consumer.fetchSubscribeMessageQueues(RocketConstant.Topic.TOPIC_TEST);
            // 遍历每一个队列，拉取数据
            for (MessageQueue mq: mqs) {
                System.out.println("第几个队列：" + mq);

                SINGLE_MQ: while (true) {
                    try {
                        // 从queue中获取数据，从什么位置开始拉取数据 单次对多拉取32条记录
                        PullResult pullResult = consumer.pullBlockIfNotFound(mq, null, getMessageQueueOffset(mq), 32);
                        System.out.println(pullResult);
                        System.out.println("pullResult = " + pullResult.getPullStatus());
                        putMessageQueueOffset(mq, pullResult.getNextBeginOffset());
                        switch (pullResult.getPullStatus()) {
                            case FOUND -> {
                                List<MessageExt> list = pullResult.getMsgFoundList();
                                for (MessageExt msg: list) {
                                    System.out.println(msg.getBody());
                                }
                                break;
                            }
                            case NO_MATCHED_MSG -> {
                                break;
                            }
                            case NO_NEW_MSG -> {
                                System.out.println("没有新的数据...");
                                break SINGLE_MQ;
                            }
                            case OFFSET_ILLEGAL -> {
                                break;
                            }
                            default -> {
                                break;
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        } catch (MQClientException e) {
            e.printStackTrace();
        }


    }

    private static long getMessageQueueOffset(MessageQueue mq) {
        Long offset = offsetTable.get(mq);
        if (offset != null) {
            return offset;
        }
        return 0;
    }

    private static void putMessageQueueOffset(MessageQueue mq, long nextBeginOffset) {
        offsetTable.put(mq, nextBeginOffset);
    }
}
