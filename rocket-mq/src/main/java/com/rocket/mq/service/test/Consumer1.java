//package com.rocket.mq.service.test;
//
//import cn.hutool.json.JSONUtil;
//import com.rocket.mq.common.RocketConstant;
//import com.rocket.mq.pojo.pgsql.UmsPraiseRec;
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
//import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
//import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
//import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
//import org.apache.rocketmq.client.exception.MQClientException;
//import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
//import org.apache.rocketmq.common.message.MessageExt;
//import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
//import org.apache.rocketmq.remoting.common.RemotingHelper;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by WuTianyu on 2020/5/8 16:22.
// */
//@Slf4j
//public class Consumer1 {
//    public static void main(String[] args) {
//        new Consumer1();
//    }
//
//    public Consumer1 () {
//        try {
//            // 定义消费者组 TODO 同一个消费者组，多个消费者，只会消费一部分信息，自动实现负载均衡机制
//            DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketConstant.ConsumerGroup.ConsumerGroupName1);
//
//            // 设置命名服务器地址
//            consumer.setNamesrvAddr(Producer.RocketConst.NAME_SRV_ADDR_MULTI_MASTER);
//
//            // 设置消费位置 TODO 未消费过，初次消费的位置，如果消费过会继续消费上一次的之后的内容
//            consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
//
//            // 订阅主题，并消费
//            consumer.subscribe(Producer.RocketConst.topic, "*");
//
//            // 设置集群模式
//            consumer.setMessageModel(MessageModel.CLUSTERING);
//
//            // 回调任务，消息到达时执行
//            consumer.registerMessageListener(new Listener());
//
//            // 启动消费者
//            consumer.start();
//            log.info("【消费者启动】【成功】读取中...");
//        } catch (MQClientException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @SneakyThrows
//    public static void pushConsumer() {
//        // 自定义消费者组 TODO 同一个消费组的消费者，只会消费一部分信息，实现负载均衡机制
//        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketConstant.ConsumerGroup.ConsumerGroupName1);
//
//        // 设置命名服务器地址
//        consumer.setNamesrvAddr(Producer.RocketConst.NAME_SRV_ADDR_MULTI_MASTER);
//
//        // 设置消费位置
//        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
//
//        // 订阅主题，并消费
//        consumer.subscribe(Producer.RocketConst.topic, "*");    // * 代表所有标签Tag，也可以指定Tag
//
//        // 设置集群模式
//        consumer.setMessageModel(MessageModel.CLUSTERING);
//
//        List<UmsPraiseRec> recs = new ArrayList<>();
//        // 回调任务，消息到达时执行
//        consumer.registerMessageListener(new Listener());
//        // 启动消费者
//        consumer.start();
//        System.out.printf("Consumer1 Started.%n");
//    }
//
//}
//
//class Listener implements MessageListenerConcurrently {
//    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, final ConsumeConcurrentlyContext context) {
//        for (MessageExt msg : msgs) {
//            String entity = null;
//            try {
//                entity = new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET);
//                UmsPraiseRec rec = JSONUtil.toBean(entity, UmsPraiseRec.class);
//                System.out.println(r1ec);
//            } catch (Exception e) {
//                e.printStackTrace();
//                if (msg.getReconsumeTimes() >= Producer.RocketConst.MAX_RECONSUME_TIMES) {
//                    System.out.println("【记日志】已经超过最大重试次数 = " + msg.getReconsumeTimes());
//                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
//                }
//            }
//        }
//        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
//    }
//}
