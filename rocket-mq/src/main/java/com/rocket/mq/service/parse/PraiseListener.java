//package com.rocket.mq.service.parse;
//
//import com.rocket.mq.common.RocketConstant;
//import com.rocket.mq.pojo.pgsql.UmsPraiseRec;
//import com.rocket.mq.repository.UmsPraiseRecDao;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.apache.rocketmq.spring.core.RocketMQListener;
//import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.LinkedList;
//import java.util.List;
//
///**
// * Created by WuTianyu on 2020/5/6 12:59.
// */
//@Service
//@RocketMQMessageListener(topic = RocketConstant.Topic.PRAISE_TOPIC, consumerGroup = RocketConstant.ConsumerGroup.PRAISE_CONSUMER)
//@Slf4j
//public class PraiseListener implements RocketMQListener<UmsPraiseRec>, RocketMQPushConsumerLifecycleListener {
//    @Resource
//    UmsPraiseRecDao praiseRecDao;
//
//    static int count = 0;
//
//    List<UmsPraiseRec> recs = Collections.synchronizedList(new ArrayList<>());
//
//    @Override
//    public void onMessage(UmsPraiseRec rec) {
//        test(rec);
//    }
//
//    public void test(UmsPraiseRec rec) {
//        if (recs.add(rec)) {
//            ++count;
//            log.info("【onMessage】【此次不传输】");
//        }
//        if (count % 1000 == 0) {
//            saveAll(recs);
//            log.info("【onMessage】【测试】{}", recs.size());
//        }
//    }
//
//    public void saveAll(List<UmsPraiseRec> recs) {
//        praiseRecDao.saveAll(recs);
//        count = 0;
//        recs.clear();
//    }
//
//    @Override
//    public void prepareStart(DefaultMQPushConsumer consumer) {
//        // 每次拉取的间隔，单位 （毫秒 / 1000 = 秒）
//        consumer.setPullInterval(2000);
//        // 设置每次从队列拉取的消息数为32
//        consumer.setPullBatchSize(32);
//    }
//}
