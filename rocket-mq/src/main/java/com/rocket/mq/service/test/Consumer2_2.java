package com.rocket.mq.service.test;

import com.alibaba.fastjson.JSON;
import com.rocket.mq.common.RocketConstant;
import com.rocket.mq.pojo.pgsql.UmsPraiseRec;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by WuTianyu on 2020/5/8 16:22.
 */
@Slf4j
public class Consumer2_2 {
    public static void main(String[] args) {
//        pushConsumer();
        new Consumer2_2();
    }

    public Consumer2_2() {
        try {
            DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketConstant.ConsumerGroup.ConsumerGroupName2);
            consumer.setNamesrvAddr(RocketConstant.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
            // 消费位置
            consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
            // 订阅主题消费
            consumer.subscribe(RocketConstant.Topic.TOPIC_TEST, RocketConstant.Tag.TAG_B);
            // 回调，监听消息
            consumer.registerMessageListener(new MsgListener());

            // Broadcasting 广播模式-相同的消费者组(ConsumerGroup)每个consumer接收一遍，他不是集群，也不是广播。他是一个单独的consumer
            consumer.setMessageModel(MessageModel.BROADCASTING);

            consumer.start();
            log.info("【消费者启动】持续读取中...");
        } catch (MQClientException e) {
            e.printStackTrace();
        }
    }

    static class MsgListener implements MessageListenerConcurrently {
        public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, final ConsumeConcurrentlyContext context) {
            // 过滤除了TAG_A以外的
            msgs = msgs.stream().filter(msg -> RocketConstant.Tag.TAG_B.equals( msg.getTags() )).collect(Collectors.toList());
            for (MessageExt msg : msgs) {
                String entity = null;
                try {
                    entity = new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET);
                    UmsPraiseRec rec = JSON.parseObject(entity, UmsPraiseRec.class);

                    System.out.println(rec + " " + msg);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (msg.getReconsumeTimes() >= Producer.RocketConst.MAX_RECONSUME_TIMES) {
                        System.out.println("【记日志】已经超过最大重试次数 = " + msg.getReconsumeTimes());
                        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                    }
                }
            }
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        }
    }

    /*@SneakyThrows
    public static void pushConsumer() {
        // 自定义消费者组 TODO 同一个消费组的消费者，只会消费一部分信息，实现负载均衡机制
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketConstant.ConsumerGroup.ConsumerGroupName);

        // 设置命名服务器地址
        consumer.setNamesrvAddr(Producer.RocketConst.NAME_SRV_ADDR_MULTI_MASTER);

        // 设置消费位置
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);

        // 订阅主题，并消费
        consumer.subscribe(Producer.RocketConst.topic, "*");    // * 代表所有标签Tag，也可以指定Tag

        List<UmsPraiseRec> recs = new ArrayList<>();
        // 回调任务，消息到达时执行
        consumer.registerMessageListener((MessageListenerConcurrently) (msgs, ctx) -> {   // 全局context对象
            for (MessageExt msg : msgs) {
                try {
                    String entity = new String(msg.getBody(), RemotingHelper.DEFAULT_CHARSET);
//                    UmsPraiseRec rec = JSONUtil.toBean(entity, UmsPraiseRec.class);

                    System.out.println(entity + " | " + msg);


                } catch (Exception e) {
                    e.printStackTrace();
                    if (msg.getReconsumeTimes() >= Producer.RocketConst.MAX_RECONSUME_TIMES) {
                        System.out.println("【记日志】已经超过最大重试次数 = " + msg.getReconsumeTimes());
                        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                    }
                }
            }
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        });
        // 启动消费者
        consumer.start();
        System.out.printf("Consumer2_1 Started.%n");
    }*/
}
