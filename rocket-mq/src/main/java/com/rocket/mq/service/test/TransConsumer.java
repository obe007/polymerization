package com.rocket.mq.service.test;

import com.rocket.mq.common.RocketConst;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.util.List;

@Slf4j
public class TransConsumer {
    public static void main(String[] args) {
        try {
            DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketConst.ConsumerGroup.ConsumerGroupName1);
//        consumer.setConsumeThreadMin(10);
//        consumer.setConsumeThreadMax(20);
            consumer.setNamesrvAddr(RocketConst.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
            consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
            consumer.subscribe(RocketConst.Topic.TOPIC_TEST, "*");

            // 设置集群模式
            consumer.setMessageModel(MessageModel.CLUSTERING);

            // 回调任务，消息到达时执行
            MessageListenerConcurrently listenerConcurrently = new Listener();
            consumer.registerMessageListener(new Listener());
            // 启动消费者

            consumer.start();
            log.info("【消费者启动】【成功】读取中...");
        } catch (MQClientException e) {
            e.printStackTrace();
        }
    }
}

@Slf4j
class Listener implements MessageListenerConcurrently {
    /**
     * It is not recommend to throw exception,rather than returning ConsumeConcurrentlyStatus.RECONSUME_LATER if
     * consumption failure
     *
     * @param msgs    msgs.size() >= 1<br> DefaultMQPushConsumer.consumeMessageBatchMaxSize=1,you can modify here
     * @param context
     * @return The consume status
     */
    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
        String entity = "";
        for (MessageExt msgExt: msgs) {
            try {
                String topic = msgExt.getTopic();
                int flag = msgExt.getFlag();
                String tags = msgExt.getTags();
                byte[] body = msgExt.getBody();
                if (body.length > 0) {
                    entity = new String(body, RemotingHelper.DEFAULT_CHARSET);
                }
                log.info("【收到消息 entity = {}, topic = {}, tags = {}, flag = {}】", entity, topic, tags, flag);
            } catch (Exception e) {
                if (msgExt.getReconsumeTimes() >= RocketConst.MAX_RECONSUME_TIMES) {
                    System.out.println("【记日志】已经超过最大重试次数 = " + msgExt.getReconsumeTimes());
                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                }
            }
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
}
