package com.rocket.mq.service.test;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.rocket.mq.common.RocketConstant;
import com.rocket.mq.pojo.pgsql.UmsPraiseRec;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;

/**
 * Created by WuTianyu on 2020/5/8 9:23.
 *
 * 生产者测试
 */
@Slf4j
public class Producer {
    public interface RocketConst {
        String NAME_SRV_ADDR_SINGLE = "10.10.22.90:9876";
        String NAME_SRV_ADDR_MULTI_MASTER = "10.10.22.90:9876;172.20.0.223:9876";

        String topic = "TopicTest";

        String[] tag = new String[]{"tagA", "tagB"};

        String brokerName = "broker-a";

        String PRODUCER_GROUP = "PRODUCER_GROUP";

        int MAX_RECONSUME_TIMES = 3;    // 最大重试次数，超过此数不再重试
    }


    @SneakyThrows
    public static void syncProducer() {
        // 生产者组的名字
        DefaultMQProducer producer = new DefaultMQProducer(RocketConstant.Topic.TOPIC_TEST);
        // 命名服务器地址
        producer.setNamesrvAddr(RocketConstant.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
        // 启动
        producer.start();
        // 重试次数
        producer.setRetryTimesWhenSendFailed(3);

        /**
         * 不要在生产端配置，和消息的限制有关的参数，放在配置文件中
         * 当生产消费启动的时候，会去nameSrv去拉取一些元数据信息，配置信息
         * 数据发的时候已经拒绝了，所以不需要在producer配置
         */

        UmsPraiseRec rec = UmsPraiseRec.builder().id("a1").liveId(1L).uid(1L).createTime(LocalDateTime.now()).build();

        for (int i = 0; i < 6; i++) {
            // 随机msgBody
            rec = UmsPraiseRec.builder().id(IdUtil.fastSimpleUUID()).liveId(1L).uid(1L).createTime(LocalDateTime.now()).build();
            // 创建消息,topic, tag标签，和 消息体
            String TAG = (i % 2 == 0) ? RocketConstant.Tag.TAG_A : RocketConstant.Tag.TAG_B;
            Message msg = new Message(RocketConstant.Topic.TOPIC_TEST /* Topic */,
                    TAG /* Tag 用于消息过滤 */,
                    "key" + i,  // 用户自定义的key， 一般用作唯一标识，主键策略
                    (JSON.toJSONString(rec)).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
//            msg.putUserProperty();
            if (i == 0) {
                msg.setDelayTimeLevel(3);
            }
            // 发送消息
            SendResult sendResult = producer.send(msg);
            /*if (sendResult.getSendStatus() != SendStatus.SEND_OK) {
                throw new Exception();
            }*/
            System.out.printf("%s%n", sendResult);
        }
        // 不使用后 关闭生产者实例
        producer.shutdown();
    }


    @SneakyThrows
    public static void syncProducerSelectMsgQueue() {
        // 生产者组的名字
        DefaultMQProducer producer = new DefaultMQProducer(RocketConst.topic);
        // 命名服务器地址
        producer.setNamesrvAddr(RocketConst.NAME_SRV_ADDR_MULTI_MASTER);
        // 启动
        producer.start();
        // 重试次数
        producer.setRetryTimesWhenSendFailed(3);

        /**
         * 不要在生产端配置，和消息的限制有关的参数，放在配置文件中
         * 当生产消费启动的时候，会去nameSrv去拉取一些元数据信息，配置信息
         * 数据发的时候已经拒绝了，所以不需要在producer配置
         */

        UmsPraiseRec rec = UmsPraiseRec.builder().id("a1").liveId(1L).uid(1L).createTime(LocalDateTime.now()).build();

        for (int i = 0; i < 1; i++) {
            // 创建消息,topic, tag标签，和 消息体
            Message msg = new Message("TopicTest" /* Topic */,
                    "TagA" /* Tag 用于消息过滤 */,
                    "keyA" + i,  // 用户自定义的key， 一般用作唯一标识，主键策略
                    (JSON.toJSONString(rec/*, SerializerFeature.IgnoreNonFieldGetter*/)).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
            // 发送消息
            SendResult sr = producer.send(msg, (mqs, msg1, arg) -> {
                // 转换arg
                Integer queueNum = (Integer)arg;
                // topic下所有messageQueueSelect
                return mqs.get(queueNum);
            }, 3);  // 最后的参数是回调，回传给arg, 指定的同一个topic的第几个队列
            /*if (sr.getSen dStatus() != SendStatus.SEND_OK) {
                throw new Exception();
            }*/
            System.out.printf("%s%n", sr);
        }
        // 不使用后 关闭生产者实例
        producer.shutdown();
    }

    /**
     * 异步传输，用于时间敏感的业务
     */
    @SneakyThrows
    public static void asyncProducer() {
        DefaultMQProducer producer = new DefaultMQProducer(RocketConstant.Topic.TOPIC_TEST);
        producer.setNamesrvAddr(RocketConstant.NameSrvAddress.NAME_SRV_ADDR_MULTI_MASTER);
        // 发起实例
        producer.start();
        // 异步发送时，内部重试次数
        producer.setRetryTimesWhenSendAsyncFailed(3);

        int sendNum = 10;
        String msgBody = JSON.toJSONString(UmsPraiseRec.builder().id(IdUtil.fastSimpleUUID())/*.uid(1/0L)*/.build().toString());

        for (int i = 0; i < sendNum; i++) {
            // 创建消息实例，自定义 topic, tag标签 和消息体
            Message msg = new Message(RocketConstant.Topic.TOPIC_TEST /* Topic */,
                    (RocketConstant.Tag.TAG_A) /* Tag */,
                    (msgBody).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
            );
            producer.send(msg, new SendCallback() {
                @Override
                public void onSuccess(SendResult sendResult) {
                    log.info("【成功】status = {}, msg = {}", sendResult.getSendStatus(), msg);
                }

                @Override
                public void onException(Throwable e) {
                    e.printStackTrace();
                    System.out.println("");
                    System.out.printf("【失败】status = {} %n", e.getMessage());
                }
            });
        }
        // TODO 异步发送不可关闭，因为直接会往下执行代码，不会等待反馈。
//        producer.shutdown();
        System.out.println("【测试】是否异步发消息直接执行");
    }

    public static void main(String[] args) throws MQClientException, UnsupportedEncodingException, RemotingException, InterruptedException, MQBrokerException {
//        asyncProducer();
        syncProducer();
//        syncProducerSelectMsgQueue();
    }

}
