package com.rocket.mq.common;

/**
 * Created by WuTianyu on 2020/5/6 10:56.
 */
public interface RocketConst {
    /**
     * topic 主题
     */
    interface Topic {
        String TOPIC_TEST = "TOPIC_TEST";
    }
    /**
     * Tag 标签
     */
    interface Tag {
        String TAG_A = "TAG_A";
        String TAG_B = "TAG_B";
    }

    /**
     * ConsumerGroup 消费者组
     */
    interface ConsumerGroup {
        String PRAISE_CONSUMER = "producer-test-group";

        String ConsumerGroupName1 = "consumer-test-group1";

        String ConsumerGroupName2 = "consumer-test-group2";

        String OUTSTATION_NO_0001 = "0001";

        String OUTSTATION_NO_3001 = "3001";
    }

    /**
     * nameSrv地址池
     */
    interface NameSrvAddress {
        String NAME_SRV_ADDR_MULTI_MASTER = "172.20.0.223:9876;10.10.22.91:9876";    // 10.10.22.90:9876;
    }

    /**
     * ProducerGroup 生产者组
     */
    interface ProducerGroup {
        String PRAISE_CONSUMER = ConsumerGroup.PRAISE_CONSUMER;
    }

    String brokerName = "broker-a";

    int  MAX_RECONSUME_TIMES = 3;
}
