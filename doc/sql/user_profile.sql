/**
 * 安装 uuid_generate_v4() 扩展函数 version1.0
 * 使用default uuid_generate_v4() 自动生成自增UUID
 * 被gen_random_uuid()取代
 */
--create extension "uuid-ossp";
--select uuid_generate_v4();

/**
 * 安装 gen_random_uuid() 扩展函数 version1.0
 * 自动生成自增UUID
 */
create extension "pgcrypto";
select gen_random_uuid();

/**
 * 当更新时更新update_time 触发器
 */
--create or replace function upd_timestamp() returns trigger
--    language plpgsql
--as
--$$
--begin
--    new.update_time = now();
--    return new;
--end;
--$$;
--create trigger update_sql_time
--    before update
--    on product_location
--    for each row
--execute procedure upd_timestamp();
--create trigger barcode_sixbitcode
--    before update
--    on barcode_sixbitcode
--    for each row
--execute procedure upd_timestamp();

/**
 * 用户信息表
 */
create table user_info (
  id char(36) not null default gen_random_uuid(),
  username varchar(32) not null default '',
  password varchar(64) not null default '',
  email varchar(128) not null default '',
  mobile varchar(32) not null default '',
  nick_name varchar(50) not null default '',
  department int2 not null default 0,
  gender int2 not null default 1,
  birthday timestamp not null default now(),
  age int2 not null default 0,
  id_card char(18) not null default '',
  login_type int2 not null default 0,
  status int2 not null default 1,
  create_time timestamp not null default now(),
  update_time timestamp not null default now(),
  primary key(id),
  constraint uq_username unique(username)
);
comment on table user_info is '用户信息表';
comment on column user_info.id is '主键UUID';
comment on column user_info.username is '用户名(英文)';
comment on column user_info.password is '登录密码(加密)';
comment on column user_info.email is '邮箱';
comment on column user_info.mobile is '手机号';
comment on column user_info.nick_name is '昵称';
comment on column user_info.department is '部门';
comment on column user_info.gender is '性别(社会学性别;包括保密等)';
comment on column user_info.birthday is '出生日期';
comment on column user_info.age is '年龄';
comment on column user_info.id_card is '身份证';
comment on column user_info.login_type is '登录类型';
comment on column user_info.status is '帐号启用状态：0->禁用；1->启用';
comment on column user_info.create_time is '创建时间';
comment on column user_info.update_time is '修改时间';


