package com.calcsuan.service;

import cn.hutool.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MobileReferServiceTest {

    @Autowired
    MobileReferService mobileReferServic;
    @Test
    public void getMobileDetail() {
        JSONObject jsonObject = mobileReferServic.getMobileDetail("17521133035");
        System.out.println("jsonObject = " + jsonObject);
    }
}
