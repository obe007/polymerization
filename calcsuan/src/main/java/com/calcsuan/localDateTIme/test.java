package com.calcsuan.localDateTIme;

import cn.hutool.cron.pattern.matcher.DayOfMonthValueMatcher;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class test {

    public static boolean validUpdateTime() {
        long nowTime = LocalDateTime.now().toEpochSecond(ZoneOffset.ofHours(+8));
        long ruleTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(16, 0)).toEpochSecond(ZoneOffset.ofHours(+8));    // 偏移16个小时0分钟，就是四点
        if ( (ruleTime - nowTime) < 0 ) {
            return false;
        } else {
            return true;
        }
    }

    public static void main(String[] args) {
//        System.out.println("validUpdateTime() = " + validUpdateTime());
//        add8();
//        System.out.println("LocalDateTime.now() = " + LocalDateTime.now().format(DateTimeFormatter.BASIC_ISO_DATE));
//        int a = 60+3*(60+1996-2021)/12;
//        System.out.println("a = " + a);
//        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH时mm分ss秒"));
//
//        System.out.println("123 = " + dateTime);
//        consumeTime();
//        System.out.println("Collections.emptyList() = " + null);

//        List<String> strs = lastDateWithMonty2(new Date("2018/01/20"), new Date("2020/04/20"));
//        lastDateWithMonty2(new Date("2020/02/19"), new Date("2020/04/20"));
        System.out.println(LocalDateTime.now());
        System.out.println(System.currentTimeMillis());
    }
    public static void add8() {
        System.out.println("LocalDateTime.now().plusHours(8L) = " + LocalDateTime.now().plusHours(8L));
        LocalTime time = LocalTime.of(8, 0);
//        LocalDateTime dateTime = LocalDateTime.of(date, time);
//        System.out.println("dateTime.format(DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN)) = " + dateTime.format(DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN)));
    }

    public static LocalTime consumeTime() {
        Long startTime = System.currentTimeMillis() / 1000;
        try {
            Thread.sleep(1000);
            System.out.println("consume time = " + LocalTime.now().minusHours( startTime ));
            return LocalTime.now().minusSeconds( startTime ).minusHours(8L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
    public static List<String> lastDateWithMonty(Date start, Date end) {
        Instant instantStart = start.toInstant();
        Instant instantEnd = end.toInstant();
        ZoneId zoneId  = ZoneId.systemDefault();
        LocalDate i = instantStart.atZone(zoneId).toLocalDate();
        LocalDate endLocal = instantEnd.atZone(zoneId).toLocalDate();
        List<String> lastMonths = new ArrayList<>();
        while (i.toEpochDay() < endLocal.toEpochDay()) {
            LocalDate mateI = LocalDate.of(
                    i.getYear(),i.getMonth(),
                    LocalDate.now().getDayOfMonth()
            );
            if (!mateI.equals( LocalDate.now() )) {
                lastMonths.add(i.with(TemporalAdjusters.lastDayOfMonth()).toString());
            } else {
                lastMonths.add(LocalDate.now().toString());
            }
            i = i.plusMonths(1L);
        }
        return lastMonths;
    }

    public static List<String> lastDateWithMonty2(Date start, Date end) {
        List<String> lastMonths = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Calendar from = Calendar.getInstance();
        from.setTime(start);
        Calendar to = Calendar.getInstance();
        to.setTime(end);
        //只要年月
        int fromYear = from.get(Calendar.YEAR);
        int fromMonth = from.get(Calendar.MONTH);
        int toYear = to.get(Calendar.YEAR);
        int toMonth = to.get(Calendar.MONTH);
        // 两个时间相差多少个月
        int hasMonth = toYear *  12  + toMonth  -  (fromYear  *  12  +  fromMonth);
        for (int i = 0; i < hasMonth; i++) {
            Calendar nowDate = Calendar.getInstance();
            int nowYear = nowDate.get(Calendar.YEAR);
            int nowMonth = nowDate.get(Calendar.MONTH) + 1;

            Calendar cale = Calendar.getInstance();
            cale.setTime(start);
            // 加月份
            cale.add(Calendar.MONTH, i);
            int caleYear = cale.get(Calendar.YEAR);
            int caleMonth = cale.get(Calendar.MONTH) + 1;
            Date lastDate = null;
            if (caleYear == nowYear && caleMonth == nowMonth) {
                lastDate = new Date();
            } else {
                cale.set(caleYear, caleMonth - 1, 1);
                cale.add(Calendar.MONTH,1);
                cale.add(Calendar.DAY_OF_MONTH,-1);
                lastDate = cale.getTime();
            }
            lastMonths.add( dateFormat.format(lastDate) );
        }
        return lastMonths;
    }

    /**
     * 计算两个日期间的天数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int daysBetween(Date beginDate,Date endDate){
        int daysCount = 0 ;

        Calendar calendarBegin = Calendar.getInstance(Locale.CHINA);
        Calendar calendarEnd = Calendar.getInstance(Locale.CHINA);
        calendarBegin.setTime(beginDate);
        calendarEnd.setTime(endDate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        try{
            if(calendarBegin.get(Calendar.YEAR) == calendarEnd.get(Calendar.YEAR)){
                daysCount = calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
            }else{
                int yearBegin  = calendarBegin.get(Calendar.YEAR);
                int yearEnd = calendarEnd.get(Calendar.YEAR);
                Date lastDateOfBegin = format.parse("" + yearBegin + "/12/31");
                calendarEnd.setTime(lastDateOfBegin);
                daysCount = calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
                calendarEnd.setTime(endDate);
                for(int i = yearBegin + 1; i < yearEnd ; i ++){
                    Date _dateBegin= format.parse("" + i + "/01/01");
                    Date _dateEnd = format.parse("" + i + "/12/31");
                    calendarBegin.setTime(_dateBegin);
                    calendarEnd.setTime(_dateEnd);
                    daysCount += calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
                }

                Date firstDateOfEnd = format.parse("" + yearEnd + "/01/01");
                calendarBegin.setTime(firstDateOfEnd);
                calendarEnd.setTime(endDate);
                daysCount += calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
            }
        }catch(Exception e){
//            logger.error(e.getMessage(),e);
        }
        return daysCount;
    }

}
