package com.calcsuan.hutool;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.URLUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

import java.util.List;

/**
 * Created by WuTianyu on 2019/12/19 9:32.
 */
public class ExcelTest {


    public static void main(String[] args) {
        String uri = "G:/0117.xlsx";
        boolean hasFile = FileUtil.isFile(uri);
        if (hasFile == false) {
            System.out.println("hasFile = " + hasFile);
            return;
        }
        ExcelReader reader = ExcelUtil.getReader( uri );
        List<ProductReport> productReports = reader.readAll(ProductReport.class);
        System.out.println("productReports = " + productReports);
    }

}
