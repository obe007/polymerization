package com.calcsuan.hutool;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * product_report
 * @author
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProdReportLogVO implements Serializable {

    /**
     * 门店No
     */
    private String storeNo;

    /**
     * 六位码
     */
    private String sixBitCode;

    /**
     * 即时库存
     */
    private BigDecimal stock;
}
