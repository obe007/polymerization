package com.calcsuan.hutool;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * product_report
 * @author
 */
@Data
@EqualsAndHashCode
@ToString
public class ProductReport implements Serializable {
    /**
     * 主键ID;序号
     */
    private Long id;

    /**
     * 档期
     */
    private String filingDate;

    /**
     * 门店No
     */
    private String storeNo;

    /**
     * 六位码
     */
    private String sixBitCode;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 展示图片
     */
    @JsonIgnore
    private String pic;

    /**
     * 规格(商品配送规格)
     */
    private String scale;

    /**
     * 即时库存
     */
    private BigDecimal stock;

    /**
     * 最低库存
     */
    @JsonIgnore
    private BigDecimal minStock;

    /**
     * 最高库存
     */
    @JsonIgnore
    private BigDecimal maxStock;

    /**
     * 上月销量
     */
    private BigDecimal lastMonthSale;

    /**
     * 促销方式(促销类型)
     */
    private String promotionType;

    /**
     * 配送方式
     */
    private String deliveryType;

    /**
     * 结算方式
     */
    private String settlementType;

    /**
     * 是否拆零(是否零售)
     */
    private String retailType;

    /**
     * 原进价(原进货价)
     */
    private BigDecimal originalPurchasePrice;

    /**
     * 促销进货价
     */
    private BigDecimal promotionPurchasePrice;

    /**
     * 促销销售价
     */
    private BigDecimal promotionSalePrice;

    /**
     * 最小起订量(最小补货量)
     */
    private BigDecimal minReplenishment;

    /**
     * 推荐补货量
     */
    private BigDecimal suggestedReplenishment;

    /**
     * 门店要货(实际补货量)
     */
    private BigDecimal realReplenishment;

    /**
     * 删除状态：0->未删除；1->已删除
     */
    private Integer deleteStatus;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    @JsonIgnore
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonIgnore
    // @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private LocalDateTime updateTime;

    private static final long serialVersionUID = 1L;
}
