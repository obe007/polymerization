package com.calcsuan.assignment;

/**
 * 2019/12/12 14:13.
 */
public class B extends A{
    public void showB() {
        // unicode表中 a 开头
        char beginLetter = 'A';

        // char 转 数字
        for (int i = (int)beginLetter; i < beginLetter + 26; i++) {
            // 强制把数字转成char字符
            System.out.print(" "+ (char)i);
        }
    }

    public static void main(String[] args) {
//        showB();
//        showA();
    }
}
