package com.calcsuan.assignment;

public class Main {

    public static void main(String[] args) {
        // 实例化 B
        B b = new B();
        b.showA();
        b.showB();

        // 实例化 A
        A a = new A();
        System.out.println("");
        a.showA();
    }
}
