package com.calcsuan;

/**
 * @author WuTianyu
 * @date 2019/11/22 9:44
 */
public class Getter {
    private String mbUseAs;

    public String getMbUseAs() {
        return mbUseAs;
    }

    public void setMbUseAs(String mbUseAs) {
        this.mbUseAs = mbUseAs;
    }
}
