package com.calcsuan.deleteif;

import java.math.BigDecimal;

/**
 * Created by WuTianyu on 2019/12/19 12:18.
 */
public class SilverStrategy implements Strategy {
    @Override
    public BigDecimal compute(Double money) {
        System.out.println("白银会员 优惠50元");
        return BigDecimal.valueOf(money - 50);
    }

    @Override
    public int getType() {
        return UserType.SILVER_VIP.getCode();
    }
}
