package com.calcsuan.deleteif;

import java.math.BigDecimal;

/**
 * Created by WuTianyu on 2019/12/19 12:18.
 */
public class OrdinaryStrategy implements Strategy {
    @Override
    public BigDecimal compute(Double money) {
        System.out.println("普通会员不打折");
        return BigDecimal.valueOf(money);
    }

    @Override
    public int getType() {
        return UserType.NORMAL_VIP.getCode();
    }
}
