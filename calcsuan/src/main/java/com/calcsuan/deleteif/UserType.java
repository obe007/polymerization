package com.calcsuan.deleteif;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by WuTianyu on 2019/12/19 12:19.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum UserType {
    NORMAL_VIP(0, "普通会员"),
    SILVER_VIP(1, "白银会员"),
    HJ_VIP(2, "黄金会员"),
    ;

    private int code;
    private String msg;
}
