package com.calcsuan.deleteif;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by WuTianyu on 2019/12/19 12:26.
 */
public class StrategyFactory {
    private Map<Integer, Strategy> map;

    public StrategyFactory() {
        List<Strategy> strategies = new ArrayList<>();

        strategies.add(new OrdinaryStrategy());
        strategies.add(new SilverStrategy());

        map = strategies.stream().collect(Collectors.toMap(Strategy::getType, strategy -> strategy));
    }

    public static class Holder {
        public static StrategyFactory instace = new StrategyFactory();
    }

    public static StrategyFactory getInstance() {
        return Holder.instace;
    }

    public Strategy get(Integer type) {
        return map.get(type);
    }


}
