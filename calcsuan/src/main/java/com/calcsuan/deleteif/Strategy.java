package com.calcsuan.deleteif;

import java.math.BigDecimal;

/**
 * Created by WuTianyu on 2019/12/19 11:34.
 */
public interface Strategy {

    // 计费方式
    BigDecimal compute(Double money);

    // 返回type
    int getType();
}
