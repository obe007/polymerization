package com.calcsuan.suanfa;

/**
 * 从一读到一亿需要读多少个汉字?
 *
 * Created by WuTianyu on 2020/1/8 10:22.
 */
public class Hanzi {

    public static void main(String[] args) {
        String con = doConvert("1000000000");
        System.out.println("con = " + con);
    }

    private static String doConvert(String s) {
        s = s.replaceAll("([0-9])(?=[0-9]{4}$)", "$1万"); // 插入“万”
        s = s.replaceAll("([0-9])(?=[0-9]{3}($|万))", "$1千"); // 插入“千”
        s = s.replaceAll("([0-9])(?=[0-9]{2}($|万))", "$1百"); // 插入“百”
        s = s.replaceAll("([0-9])(?=[0-9]($|万))", "$1十"); // 插入“十”
        s = s.replaceAll("(0千|0百|0十|0)+(?=$|万)", ""); // 抹去多余0
        s = s.replaceAll("(0千|0百|0十)+", "零"); // 替换中间0为“零”
        s = s.replace("1", "一").replace("2", "二").replace("3", "三")
                .replace("4", "四").replace("5", "五").replace("6", "六")
                .replace("7", "七").replace("8", "八").replace("9", "九");
        return s;
    }
}
