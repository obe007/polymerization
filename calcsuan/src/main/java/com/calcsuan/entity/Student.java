package com.calcsuan.entity;

import lombok.*;

@Data
@EqualsAndHashCode
@ToString
@Builder
public class Student {
    String name;
    Integer age;
    Integer height;

    public Student(String name, Integer age, Integer height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public Student() {
    }
}
