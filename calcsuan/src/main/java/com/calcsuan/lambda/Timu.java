package com.calcsuan.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by WuTianyu on 2020/2/10 13:49.
 */
public class Timu {
    public static void main(String[] args) {
        String[] strArr = new String[]{"1", "2", "bilibili","of", "codesheep","5","at","BILIBILI","codesheep","23","CHEERS","6"};
        List<String> strs = Arrays.asList(strArr);
        String resultList = strs.stream()
                .filter( s -> isNum(s) )
                .filter( s -> s.length() >= 5)
                .map(s -> s.toLowerCase())
                .distinct()
                .sorted( Comparator.naturalOrder() )
                .collect(Collectors.joining(" ❤ "));

        System.out.println(resultList);

        System.out.println("isNum(\"123\") = " + isNum("123"));
    }

    public static boolean isNum(String s) {
        for (int i = 0; i < s.length(); i++) {
            if ( !Character.isDigit( s.charAt(i) ) ) {
                return false;
            }
        }
        return true;
    }


}
