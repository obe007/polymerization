package com.calcsuan.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 拆分List版本
 * @version 2.0
 * @author WuTianyu
 * @date 2019/12/11 10:21
 * 天宇造轮子！
 */
@SuppressWarnings("unchecked")
public class BatchList2Util<E> {
    /** default 每X个 为一组 */
    private static Long MAX_NUMBER = 8000L;

    /**
     * @param list
     * @param <E>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <E> List<List<E>> batchList(List<E> list) {
        return batchList(list, MAX_NUMBER);
    }

    /**
     * skip(), 和limit() 分别是跳过n条和获取n条
     * 流操作是先执行的作为后执行的数据
     * 先skip操作后就先截断了前面的几个元素，后再获取limit
     * 如果顺序反了就需要做类似于mysql的实现，获取第100条，要limit(100).skip(99)
     * @param list
     * @param MAX_NUMBER
     * @param <E>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <E> List<List<E>> batchList(List<E> list, Long MAX_NUMBER) {
        if (list.isEmpty()) { return Collections.emptyList(); }
        setMaxNumber( MAX_NUMBER );
        List<List<E>> matrixList = Collections.synchronizedList(new ArrayList<>());
        Stream.iterate(0, i -> i + 1 ).limit( countStep(list.size()) ).forEach(i -> {
            matrixList.add( list.parallelStream()
                    .skip( i * MAX_NUMBER )
                    .limit( MAX_NUMBER )
                    .collect(Collectors.toList())
            );
        });
        return matrixList;
    }

    /**
     * 计算切割次数
     *
     * @param size
     * @return
     */
    public static long countStep(int size) {
        return (size + MAX_NUMBER - 1) / MAX_NUMBER;
    }

    public static void setMaxNumber(Long maxNumber) {
        MAX_NUMBER = maxNumber;
    }
}
