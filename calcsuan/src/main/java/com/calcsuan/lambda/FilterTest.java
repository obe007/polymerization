package com.calcsuan.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by WuTianyu on 2020/3/13 10:24.
 */
public class FilterTest {
    public static void main(String[] args) {
        trueOrFalse();
    }

    public static void trueOrFalse() {
        String a = "123";
        String b = "456";
        List<String> strs = new ArrayList<>();
        strs.addAll(Arrays.asList(a, b));
        List<String> result = strs.stream().filter( str -> str.equals("123") ).collect(Collectors.toList());
        System.out.println("result = " + result);
    }
}
