package com.calcsuan.lambda;

import com.calcsuan.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Configuration
public class Test {

    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(Student.builder().name("第一名").build());
        students.add(Student.builder().name("第二名").build());
        students.add(Student.builder().name("第三名").build());
//        List<List<Student>> studentMatrix = BatchListUtil.batchList(students, 3L);
//        for (List<Student> studentList: studentMatrix) {
//            System.out.println("studentList = " + studentList);
//        }
//        students.stream().skip(1L).limit(1L).forEach(System.out::print);

//        System.out.println("showMax() = " + showMax());

//        String::valueOf("1");
    }

    /**
     * 无参运行
     */
    public static void noArgs() {
        Runnable r1 = () -> System.out.println("Hello");
        for (int i = 0; i <= 10; i++) {
            r1.run();
        }
    }
    public static void collectorsJoining() {
        String str = Stream.of(getStudent()).map(Student::getName).collect(Collectors.joining( ",","|","|" ));

        System.out.println("str = " + str);

//        List<Student> students = new ArrayList<>();
//        students.add(new Student("路飞", 22, 175));
//        students.add(new Student("红发",40,180));
//        students.add(new Student("白胡子", 50, 185));
//        String names = students.stream().map(Student::getName).collect(Collectors.joining(",","|","|"));
//        System.out.println(names);

//        System.out.println("log = " +

    }

        public static void testReduce() {
        Integer reduce = Stream.of( 1, 2, 3, 4 )
                .reduce( 1, (acc, x) -> acc * x );
        System.out.println("reduce = " + reduce);
    }

    public static void testCase() {
        List<Student> studentList = Stream.of( new Student("1",1,1) ).collect(Collectors.toList());
    }



    public static void consumer() {
        Consumer<Student> consumer = System.out::println;
        Student student = getStudent();
        consumer.accept(student);
    }

    public static void binaryOperator() {
//        BinaryOperator<Integer> operator = (x, y) -> x*y;
//        Integer integer = operator.apply(2, 3);
//        System.out.println(integer);
    }

    public static void predicate() {
        Predicate<Integer> predicate = x -> x >= 185;
        Student student = getStudent();
        log.info("九龙的身高高于185么 = {}", predicate.test(student.getHeight())?"高于等于":"低于");
    }

    public static Student getStudent() {
        return Student
                .builder()
                .name("九龙")
                .age(23)
                .height(175)
                .build();
    }

    public static void iterateShow() {
        Stream.iterate(1, i -> i+1)
                .limit( 20 )
                .skip( 10 )
                .forEach(System.out::println);
    }

    public static List<List<?>> batchList(List<?> list, Long MAX_NUMBER) {
        List<List<?>> matrixList = new ArrayList<>();
        Stream.iterate(0, i -> i + 1 ).limit( countStep(list.size(), MAX_NUMBER) ).forEach( i -> {
            matrixList.add( list.parallelStream()
                    .limit( MAX_NUMBER )
                    .skip( i * MAX_NUMBER )
                    .collect(Collectors.toList())
            );
        });
        return null;
    }
    public static long countStep(int size, long MAX_NUMBER) {
        return (size + MAX_NUMBER - 1) / MAX_NUMBER;
    }

    public static Integer showMax() {
        return Integer.MAX_VALUE;
    }
}
