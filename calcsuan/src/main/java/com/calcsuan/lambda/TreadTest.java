package com.calcsuan.lambda;

import java.util.concurrent.TimeUnit;

/**
 * Created by WuTianyu on 2020/3/23 13:37.
 */
public class TreadTest {
    public volatile static boolean flag = false;

    public static void main(String[] args) {
        new Thread(() -> {
            try {
                System.out.println("1");
                TimeUnit.MICROSECONDS.sleep(100);
                flag = true;
                System.out.println("flag = " + flag);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        System.out.println("2");
        int i = 0;
        while ( !flag ) {
            i++;
        }
        System.out.println("程序结束 i = " + i);
    }
}
