package com.calcsuan.pojo.form;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 用户注册参数
 * @author WuTianyu
 */
@Data
@Builder
public class UserRegParam {

    @ApiModelProperty("用户名")
    @NotEmpty(message = "用户名不为空")
    @Length(min = 3, max = 16, message = "失败！用户名长度为: 3~16位")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    @NotEmpty(message = "密码不为空")
    @Length(min = 3, max = 32, message = "失败！密码长度为: 3~32位")
    private String password;

    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    @Email(message = "邮箱格式不正确")
    private String email;

//    /**
//     * 手机号
//     */
//    @ApiModelProperty("手机号")
//    private String mobile;

    /**
     * 昵称
     */
    @ApiModelProperty("真实姓名")
    @NotEmpty(message = "真实姓名不为空")
    private String nickName;
}
