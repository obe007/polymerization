package com.calcsuan.ThinkJava;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by WuTianyu on 2020/4/9 12:55.
 */
class Tank {
    String c;
    @Override
    public String toString() {
        return "Tank{" +
                "c='" + c + '\'' +
                '}';
    }
}
public class Alias {
    //用来验证对象传递给方法时
    static void check(Tank tank) {
        tank.c = "c";
    }
    public static void main(String[] args) {
        Tank tank = new Tank();
        tank.c = "z";
        System.out.println("tank = " + tank);
        check(tank);
        Tank tank2 = new Tank();
        System.out.println("tank.c = " + tank.c);
        Map<String, String> map1 = new ConcurrentHashMap<>();
//        map1.containsValue()
    }
}
