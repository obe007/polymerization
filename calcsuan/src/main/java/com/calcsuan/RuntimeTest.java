package com.calcsuan;

import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.system.SystemUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;

import static cn.hutool.core.util.RuntimeUtil.getResultLines;

/**
 * <pre>
 * 测试删除除了自己以外的Java进程
 * </pre>
 * Created by WuTianyu on 2020/2/19 8:42.
 */
@Slf4j
public class RuntimeTest {
    /** 模板-筛选出除了本身pid之外之外的java */
    public static final String killOtherPat = "TASKKILL /F /FI \"PID ne %s\" /IM %s";

    public static void main(String[] args) {
        Long currentPid = SystemUtil.getCurrentPID();

        String killOtherJava = String.format(killOtherPat, currentPid, "java.exe");
        killJava();
        System.out.println("currentPid = " + currentPid);
        System.out.println("killOtherJava = " + killOtherJava);
        System.out.println(000);
    }

    public static boolean killJava() {
        Long currentPid = SystemUtil.getCurrentPID();
        String killOtherJava = String.format(killOtherPat, currentPid, "java.exe");
        List<String> result = execute(killOtherJava);
        if (result.isEmpty()) {
            return false;
        } else {
            log.info("【结束进程】【Java.exe】结束所有非本身之外的Java.exe: {}", result);
            return true;
        }
    }

    public static Process exec(String command) {
        try {
            return Runtime.getRuntime().exec( command );
        } catch (IOException e) {
            log.error("【运行cmd命令】【失败】命令为: {} , 错误: {}", command, e.getMessage());
            return null;
        }
    }

    public static List<String> execute(String command) {
        if (StrUtil.isBlank(command)) {return null;}
//        exec(command);
        Process proc = exec(command);
        try {
            // 针对使用较长时间做工作的process，就需要调用waitFor方法。
            // 该方法会引起当前Thread等待，直到process中断。
            proc.waitFor();
        } catch (InterruptedException e) {
            log.info("【运行cmd命令】【失败】【proc.waitFor()】错误: {} ", e.getMessage());
        }
        List<String> resultLines = getResultLines(proc);
        log.info("【运行cmd命令】【成功】命令为: {} , 结果: {} ", command, resultLines);
        return resultLines;
    }
}
