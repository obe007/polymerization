package com.calcsuan.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    /** 每3个 为一组 */
    private static final Integer MAX_NUMBER = 3;

    /** 计算切分次数 */
    private static Integer countStep(Integer size) {
        return ( size + MAX_NUMBER - 1 ) / MAX_NUMBER;
    }

    public static void main(String[] args) {
//        returnShow();
        List<Integer> ints = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        int limit = countStep(ints.size());

        // 使用流遍历操作
        List<List<Integer>> matrixList = new ArrayList<>();
        Stream.iterate(0, n -> n + 1).limit( limit ).forEach( i -> {
            matrixList.add( ints.parallelStream()
                    .skip(i * MAX_NUMBER)
                    .limit(MAX_NUMBER)
                    .collect(Collectors.toList())
            );
        });


        System.out.println("matrixList = " + matrixList);
    }

    private static void filterList() {

    }

    private static void returnShow() {

    }


}
