package com.calcsuan;

import cn.hutool.core.collection.CollUtil;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author WuTianyu
 * @date 2019/11/13 14:16
 */
public class SplitList {
    public static <T>List<Collection> splitList(Collection<T> collection, int maxSize, int splitSize) {
        if( CollUtil.isEmpty(collection) ) return Collections.emptyList();
        return Stream.iterate(0,  i -> i + 1)
                .limit(maxSize)
                .map(a -> collection.parallelStream()
                        .skip(a * splitSize)
                        .limit(splitSize)
                        .collect(Collectors.toList()))
                .filter(b -> !b.isEmpty())
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> strs = new ArrayList<>();
        strs.add("a");
        strs.add("b");
        strs.add("c");
        strs.add("d");
        strs.add("e");
        strs.add("f");
        strs.add("f");
        strs.add("f");
//        List<List<String>> strs2 = splitList(strs, 2, 2);

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
//        List<Integer> list1 = list.stream().collect(Collectors.groupingBy(n -> n > 6, Collectors.toList()));
//        System.out.println("list1 = " + list1);
    }
}
