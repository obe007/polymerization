package com.calcsuan;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by WuTianyu on 2020/4/30 16:57.
 */
@Component
@Service
@Configurable
@EnableRetry
public class RecoverTest {
    @Retryable(value= {Exception.class},maxAttempts = 3)
    public void call() throws Exception {
        System.out.println("do something...");
        throw new Exception("RPC调用异常");
    }
    @Recover
    public void recover(RemoteAccessException e) {
        System.out.println(e.getMessage() + "哟哟哟哟");
    }
}
