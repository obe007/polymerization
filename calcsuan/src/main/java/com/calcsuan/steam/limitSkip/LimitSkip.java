//package com.calcsuan.steam.limitSkip;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.function.Predicate;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//
//public class LimitSkip {
//
//    public static void main(String[] args) {
//        List<String> strList = new ArrayList<>();
//        String[] strArr = new String[]{"a", "b", "c", "d", "e"};
//        strList.addAll(Arrays.asList(strArr));
//        int pageNum = 0;
//        int size = 2;
//        for (int i = 0; i < 10000; i++) {
////            System.out.println("strList = " + strList.parallelStream().skip(pageNum * size).limit(size).collect(Collectors.toList()));
//        }
//
//        List<String> strs = Arrays.asList("012309", "1239123", "1231234", "absad1");
//        strs.removeIf( str -> {
//            String regex = ".*[a-zA-Z]+.*";
//            Matcher m = Pattern.compile(regex).matcher(str);
//            return m.matches();
//        });
//        System.out.println("strs = " + strs);
//
////        strList.forEach(new ArrayList<String>());
//    }
//
//    /**  */
//    public static List<String> filter2() {
//        List<String> strs = List.of("012309", "1239123", "1231234", "absad1");
//        strs.removeIf( str -> {
//            String regex = ".*[a-zA-Z]+.*";
//            Matcher m = Pattern.compile(regex).matcher(str);
//            return m.matches();
//        });
//        return strs;
//    }
//}
