package com.calcsuan.util;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.system.SystemUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by WuTianyu on 2020/1/20 10:18.
 */
@Slf4j
public class ProcessUtil {

    public static void main(String[] args) {
//            Process p = Runtime.getRuntime().exec("taskkill /F /PID " + 9560);
//        Process proc = null;
//        try {
//            proc = Runtime.getRuntime().exec("taskkill /?");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        List<String> strs = RuntimeUtil.getResultLines(proc, Charset.defaultCharset());
//            System.out.println("strs = " + strs);
//        String homeDir = SystemUtil.getUserInfo().getHomeDir();
//        String path = homeDir + File.separatorChar + IdUtil.fastSimpleUUID();
//        System.out.println("path = " + path);
        String uri = "ftp://10.10.1.28/0826.ARJ";
        String fileName = StrUtil.subAfter(uri, "/", true);
        System.out.println("fileName = " + fileName);
    }
}
