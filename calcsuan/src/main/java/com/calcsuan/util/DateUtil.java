package com.calcsuan.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by WuTianyu on 2020/2/25 10:33.
 */
public class DateUtil {

//    private static Logger logger = Logger.getLogger(DateUtil.class);

    public static String getNow() {
        return getNow("yyyy-MM-dd HH:mm:ss");
    }

    public static String getNowStr() {
        return getNow("yyyyMMddHHmmss");
    }

    public static String getToday() {
        String today = "00000000";
        String now = getNow();
        if (now != null && now.length() >= 10) {
            today = now.substring(0, 8);
        }
        return today;
    }

    public static String getNow(String formatstr) {
        String formatedatestr = "";
        try {
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                    formatstr);
            java.util.Date currentTime = new java.util.Date();
            formatedatestr = formatter.format(currentTime);
        } catch (Exception e) {
//            logger.error(e.getMessage(),e);
        }
        return formatedatestr;
    }

    public static String formatDate(java.util.Date d_date, String pattern) {
        String formatedatestr = "";
        try {
            if(null!=d_date){
                java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                        pattern);
                formatedatestr = formatter.format(d_date);
            }
        } catch (Exception e) {
//            logger.error(e.getMessage(),e);
        }
        return formatedatestr;
    }

    public static String dateToString(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }
    public static String date2String(Date date) {
        return formatDate(date, "yyyy-MM-dd");
    }

    public static String dateToString(Date date,String pattern) {
        return formatDate(date, pattern);
    }

    public static boolean before(String sdate1, String sdate2) {
        Date date1 = new Date(Integer.parseInt(sdate1.substring(0, 4)),
                Integer.parseInt(sdate1.substring(5, 7)),
                Integer.parseInt(sdate1.substring(8, 10)));
        Date date2 = new Date(Integer.parseInt(sdate2.substring(0, 4)),
                Integer.parseInt(sdate2.substring(5, 7)),
                Integer.parseInt(sdate2.substring(8, 10)));
        return date1.before(date2);
    }

    public static String getLastDate(long day) {
        long date_3_hm = System.currentTimeMillis() - 3600000 * 24 * day;
        Date date_3_hm_date = new Date(date_3_hm);

        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(
                "yyyy-MM-dd");
        String formatedatestr = formatter.format(date_3_hm_date);

        formatedatestr = formatter.format(date_3_hm_date);
        return formatedatestr;
    }

    public static Date parseDate(String datestr, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = format.parse(datestr);
        } catch (ParseException e) {
//            logger.error(e.getMessage(),e);
        }
        return date;
    }

    /**
     * 计算两个日期间的天数
     * @param beginDate
     * @param endDate
     * @return
     */
    public static int daysBetween(Date beginDate,Date endDate){
        int daysCount = 0 ;

        Calendar calendarBegin = Calendar.getInstance(Locale.CHINA);
        Calendar calendarEnd = Calendar.getInstance(Locale.CHINA);
        calendarBegin.setTime(beginDate);
        calendarEnd.setTime(endDate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        try{
            if(calendarBegin.get(Calendar.YEAR) == calendarEnd.get(Calendar.YEAR)){
                daysCount = calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
            }else{
                int yearBegin  = calendarBegin.get(Calendar.YEAR);
                int yearEnd = calendarEnd.get(Calendar.YEAR);
                Date lastDateOfBegin = format.parse("" + yearBegin + "/12/31");
                calendarEnd.setTime(lastDateOfBegin);
                daysCount = calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
                calendarEnd.setTime(endDate);
                for(int i = yearBegin + 1; i < yearEnd ; i ++){
                    Date _dateBegin= format.parse("" + i + "/01/01");
                    Date _dateEnd = format.parse("" + i + "/12/31");
                    calendarBegin.setTime(_dateBegin);
                    calendarEnd.setTime(_dateEnd);
                    daysCount += calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
                }

                Date firstDateOfEnd = format.parse("" + yearEnd + "/01/01");
                calendarBegin.setTime(firstDateOfEnd);
                calendarEnd.setTime(endDate);
                daysCount += calendarEnd.get(Calendar.DAY_OF_YEAR) - calendarBegin.get(Calendar.DAY_OF_YEAR) + 1;
            }
        }catch(Exception e){
//            logger.error(e.getMessage(),e);
        }
        return daysCount;
    }

    /**
     *
     * @Title: getXMonthLateDate
     * @Description: 获取x月后的日期
     * @param date
     * @param x
     * @return
     */
    public static String getXMonthLateDate(String date,int x) throws Exception {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar gc=new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = sdf.parse(date);
        gc.setTime(d);
        gc.add(GregorianCalendar.MONTH, x);
        return format.format(gc.getTime());
    }
    /**
     * @Title: compareDate
     * @Description: 日期对比   返回布尔值
     * @Version 1.0
     * @Date 2015-8-26
     * @Author xw
     * @param date1
     * @param date2
     * @return
     */
    public static boolean compareDate(String date1,String date2){
        boolean result=true;
        DateFormat formatDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date draftDate2;
        try {
            draftDate2 = formatDate.parse(date1);
            Date nowDate2=formatDate.parse(date2);
            if(nowDate2.before(draftDate2)){
                result=false;
            }
        } catch (ParseException e) {

//            logger.error(e.getMessage(),e);
        }
        return result;
    }
    /**
     *
     * @Title: getFewHoursLastDate
     * @Description: 基于传入时间获得X小时后的时间
     * @Version 1.0
     * @Date 2018年4月24日
     * @Author hupeng
     * @param now
     * @param manyHours
     * @return
     */
    public static Date getFewHoursLastDate(Date now , int manyHours){
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(now);
        rightNow.add(Calendar.HOUR_OF_DAY,manyHours*1);
        return rightNow.getTime();
    }

    public static String Date2SimpleStr(Date date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(
                    "yyyyMMdd");
            return format.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date getFewDaysAgoDate(Date now , int manyDays){
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(now);
        rightNow.add(Calendar.DAY_OF_YEAR,manyDays*-1);
        return rightNow.getTime();
    }

    public static Date getFewDaysLaterDate(Date now , int manyDays){
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(now);
        rightNow.add(Calendar.DAY_OF_YEAR,manyDays*1);
        return rightNow.getTime();
    }

    public static Date Str2Date(String date) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.parse(date);
    }

    public static String Date2yMdStr(Date date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(
                    "yyyy-MM-dd");
            return format.format(date);
        } catch (Exception e) {
            return null;
        }
    }
    public static Date StrYMD2Date(String date) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(date);
    }

    /**
     *
     * @Title: weekOfYear
     * @Description: 根据入参时间 “2013-01-14”获取 当前时间为当年第几周
     * @Version 1.0
     * @Date 2019年4月11日
     * @Author hupeng
     * @param date
     * @return
     * @throws Exception
     */
    public static String weekOfYear(String today) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(today);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR)+"WK"+ calendar.get(Calendar.WEEK_OF_YEAR)+"";
    }


    public static int getWeekOfYear (Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);//设置星期一为一周开始的第一天
        calendar.setTimeInMillis(date.getTime());//获得当前的时间戳
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);//获得当前日期属于今年的第几周
        return weekOfYear;
    }

    /**
     * get first date of given month and year
     * @param year
     * @param month
     * @return
     */
    public static String getFirstDayOfMonth(int year,int month){
        String monthStr = month < 10 ? "0" + month : String.valueOf(month);
        return year + "-"+monthStr+"-" +"01";
    }

    /**
     * get the last date of given month and year
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year,int month){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR , year);
        calendar.set(Calendar.MONTH , month - 1);
        calendar.set(Calendar.DATE , 1);
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_YEAR , -1);
        return calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" +
                calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * get Calendar of given year
     * @param year
     * @return
     */
    private static Calendar getCalendarFormYear(int year){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.YEAR, year);
        return cal;
    }

    /**
     * get start date of given week no of a year
     * @param year
     * @param weekNo
     * @return
     */
    public static String getStartDayOfWeekNo(int year, int weekNo) {
        try {
            Calendar cal = getCalendarFormYear(year);
            cal.set(Calendar.WEEK_OF_YEAR, weekNo);
            String time = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-"
                    + cal.get(Calendar.DAY_OF_MONTH);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(time);

            return formatDate(date, "yyyy-MM-dd");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    /**
     * get the end day of given week no of a year.
     * @param year
     * @param weekNo
     * @return
     */
    public static String getEndDayOfWeekNo(int year,int weekNo){
        try {
            Calendar cal = getCalendarFormYear(year);
            cal.set(Calendar.WEEK_OF_YEAR, weekNo);
            cal.add(Calendar.DAY_OF_WEEK, 6);
            String time = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" +
                    cal.get(Calendar.DAY_OF_MONTH);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(time);

            return formatDate(date, "yyyy-MM-dd");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    // 获取当前时间所在年的最大周数
    public static int getMaxWeekNumOfYear(int year) {
        Calendar c = new GregorianCalendar();
        c.set(year, Calendar.DECEMBER, 31, 23, 59, 59);
        return getHoManyWeekInYear(c.getTime());
    }

    public static int getHoManyWeekInYear(Date date) {
        Calendar c = new GregorianCalendar();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setMinimalDaysInFirstWeek(7);
        c.setTime(date);

        return c.get(Calendar.WEEK_OF_YEAR);
    }

}
