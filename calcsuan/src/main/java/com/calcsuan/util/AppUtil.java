package com.calcsuan.util;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.TreeMap;

/**
 * Created by WuTianyu on 2020/3/31 14:38.
 *
 * 生成 app_id 和 app_secret 用于接口安全验证
 */
@Slf4j
public class AppUtil {
    /**
     * a-z, 0-9, A-Z
     */
    public static final String[] strArr = new String[]{
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
            "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C",
            "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    /** 加密字符 */
    public static final String saltValue = "!qaz@wsx";

    @SneakyThrows
    public static String checkSign(String appId, long timestamp, String body){
        TreeMap<String, Object> map = new TreeMap<>();
        map.put("appId", appId);
        map.put("body", body);
        map.put("timestamp", timestamp);
        return calcSign(map, getAppSecret(appId));
    }

    @SneakyThrows
    public static String calcSign(TreeMap<String, Object> map, String secret) {
        return md5Str(concatParams(map, secret)).toUpperCase();
    }

    @SneakyThrows
    private static String concatParams(TreeMap<String, Object> treeMap, String secret) {
        StringBuilder sb = new StringBuilder(secret);
        int i = treeMap.size();
        for (java.util.Map.Entry<String, Object> entry : treeMap.entrySet()) {
            if ("sign".equals(entry.getKey())) {
                i--;
                //忽略sign
                continue;
            }
            sb.append(entry.getKey()).append("=").append(entry.getValue());
            if (i > 1) {
                sb.append("&");
            }
            i--;
        }
        sb.append(secret);
        return sb.toString();
    }

    /**
     * 生成复杂appId
     * @return app_id
     */
    public static String getComplexAppId() {
        return IdUtil.fastSimpleUUID();
    }

    /**
     * 生成appId
     * 短位生成法
     * uuid拆分，转成十进制数字
     * UUID每一位1~F 是16进制。可转为10进制数字
     * 任何数 对 x 取模， 得到的值永远不会超过 x
     * @return app_id
     */
    public static String getAppId() {
        String uuid = IdUtil.simpleUUID();
        StringBuilder shortBuilder = new StringBuilder();
        // 几位长度为一组，越短生成的appId越长
        int bit = 4;
        java.util.stream.Stream.iterate(0, i -> i + 1).limit(countGroup(bit, uuid.length())).forEach(i -> {
            String str = StrUtil.sub(uuid, i * bit, i * bit + bit);
            // 以16位字符转数字
            int x = Integer.parseInt(str, 16);
            shortBuilder.append(strArr[x % strArr.length]);
        });
        return shortBuilder.toString();
    }

    /**
     * 通过appId，获取app_secret
     * @param appId app_id
     * @return app_secret
     */
//    public static String getAppSecret(String appId) {
//        return null;
//    }

    /**
     * 计算组数量
     * @param num 分为几位 为一组字符
     * @param length 拆分字符的长度
     * @return 组的数量
     */
    public static int countGroup(int num, int length) {
        return (num + length - 1) / num;
    }

    /**
     * <p>
     * 通过appId和内置关键词生成APP Secret
     * </P>
     * @author mazhq
     * @date 2019/8/27 16:32
     */
    public static String getAppSecret(String appId) {
        try {
            String[] array = new String[]{appId, saltValue};
            StringBuffer sb = new StringBuffer();
            // 字符串排序
            Arrays.sort(array);
            for (int i = 0; i < array.length; i++) {
                sb.append(array[i]);
            }
            String str = sb.toString();
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(str.getBytes());
            byte[] digest = md.digest();

            StringBuffer hexstr = new StringBuffer();
            String shaHex = "";
            for (int i = 0; i < digest.length; i++) {
                shaHex = Integer.toHexString(digest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexstr.append(0);
                }
                hexstr.append(shaHex);
            }
            return hexstr.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error("【生成AppSecret】【失败】errorMsg = {}", e.getMessage());
            return null;
        }
    }

    @SneakyThrows
    public static String md5Str(String text) {
        byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(bytes);
        bytes = messageDigest.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            if ((bytes[i] & 0xff) < 0x10) {
                sb.append("0");
            }

            sb.append(Long.toString(bytes[i] & 0xff, 16));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String wechatSecret = "wechat appid:8cc16a724c8046c1bc3cd8ad75b6c72e" +
                "secret:13537a1262b37a03d24cab65baff8b1d6bbd3b0e";
        String appId = "8cc16a724c8046c1bc3cd8ad75b6c72e";
        System.out.println("appId = " + appId);
        System.out.println("appSecret = " + getAppSecret( appId ));

        String body = "{\n" +
                "    \"outstationNo\":\"0016\",\n" +
                "    \"startTime\":\"2020-04-02T00:00:00\"\n" +
                "}";
        String secret = "13537a1262b37a03d24cab65baff8b1d6bbd3b0e";
        long timestamp = 1586393642L;
        String sign = checkSign(appId, timestamp, body);
        System.out.println("sign = " + sign);


        System.out.println("LocalDateTime.now() = " + LocalDateTime.now());
    }
}
