package com.calcsuan.util;

import com.calcsuan.pojo.pgsql.SysUser;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by WuTianyu on 2020/2/24 14:39.
 */
public class ListSort<E> {
    public static final String DESC = "DESC";
    public static final String ASC = "ASC";

    /**
     * 排序某个对象的字段
     * @param list 要排序的集合
     * @param method 要排序实体的属性所对应的 get方法全名 例如：getId
     * @param sort DESC 正序, ASC 倒叙
     */
    public static <E> List<E> sort(List<E> list, final String method, String sort) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, (a, b) -> {
                int ret = 0;
                try {
                    Method m1 = a.getClass().getMethod(method, null);
                    Method m2 = b.getClass().getMethod(method, null);

                    if (ASC.equals( sort )) {
                        ret = m2.invoke(((E)b), null).toString().compareTo(
                                m1.invoke(((E)b), null).toString()
                        );
                    } else {
                        ret = m1.invoke(((E)b), null).toString().compareTo(m2.invoke(((E)b), null).toString());
                    }
                } catch (NoSuchMethodException e) {
                    System.out.println("【Sort排序】【方法异常】 = " + e.getMessage());
                } catch (IllegalAccessException e) {
                    System.out.println("【Sort排序】【非法存取异常】 = " + e.getMessage());
                } catch (InvocationTargetException e) {
                    System.out.println("【Sort排序】【反射异常】 = " + e.getMessage());
                }
                return ret;
            });
        }
        return list;
    }

    static class BaseComparator implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            return 0;
        }
    }

    public static void main(String[] args) {
        List<SysUser> users = new ArrayList<>();
        users.add( SysUser.builder().id("2").build() );
        users.add( SysUser.builder().id("1").build() );
        users.add( SysUser.builder().id("a").build() );
        users.add( SysUser.builder().id("3").build() );
        users.add( SysUser.builder().id("微服务").build() );
        List<SysUser> id = ListSort.sort(users, "getId", ListSort.DESC);
        System.out.println("id = " + id);
    }
}
