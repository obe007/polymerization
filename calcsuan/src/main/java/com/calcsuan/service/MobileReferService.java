package com.calcsuan.service;

import cn.hutool.json.JSONObject;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author WuTianyu
 * @date 2019/11/11 13:17
 */
@FeignClient(url = "https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=")
@RequestMapping("/remote")
@Service
public interface MobileReferService {

    @PostMapping(value = "mobile")
    JSONObject getMobileDetail(@RequestParam("tel") String tel);
}
