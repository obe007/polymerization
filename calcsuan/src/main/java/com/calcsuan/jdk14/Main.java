package com.calcsuan.jdk14;

import com.calcsuan.hutool.ProdReportLogVO;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class Main {
    public static void main(String[] args) {
        Integer a = 1; Integer b = a;
//        List list = List.of(a,b);
//        System.out.println("list = " + list);as
//        System.out.println("ii = " + (char)97);
        Map<String, String> map = new HashMap<>();
        List<Map<String, String>> maps = new ArrayList<>();
//        maps.add(new HashMap<String, String>().);

        List<ProdReportLogVO> reportLogVOs = Collections.synchronizedList(new ArrayList<>());
        reportLogVOs.add(ProdReportLogVO.builder().sixBitCode("123189").storeNo("1111").build());
        reportLogVOs.add(ProdReportLogVO.builder().sixBitCode("asda").storeNo("asda").build());
        reportLogVOs.add(ProdReportLogVO.builder().sixBitCode("2222").storeNo("2222").build());
        reportLogVOs.add(ProdReportLogVO.builder().sixBitCode("333").storeNo("333").build());
        log.info("{}", reportLogVOs);

    }
}
